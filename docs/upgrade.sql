-- 20200504 - Add comment reply struct
ALTER TABLE `vblog_comment`
    ADD COLUMN `parent_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `cid`,
	ADD COLUMN `reply_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `parent_id`,
    DROP COLUMN `userAgent`;

ALTER TABLE `vblog_comment`
    DROP INDEX `cid`,
    ADD INDEX `cid_parent` (`cid`, `parent_id`);

--Convert old comments to new table.
INSERT INTO vblog_comment (cid,created,author,mail,content,url,ip,status)
    SELECT articleid AS cid,dateline AS created,author,email as mail,content,url,ipaddress as ip, 1 AS `status` FROM `sax_comments`;
UPDATE `vblog_content` SET `commentNum`=(SELECT COUNT(*) FROM `vblog_comment` WHERE `cid`=`vblog_content`.`cid`);

--20200511 - 配置表更新
ALTER TABLE `vblog_option`
    CHANGE `type` `category` ENUM('core','setting','system','user') NOT NULL DEFAULT 'system',
    ADD COLUMN `type` ENUM('string','int','bool','float','serialize') NOT NULL;

ALTER TABLE `vblog_option`
	DROP INDEX `type`,
    ADD INDEX `category` (`category`);

UPDATE `vblog_option` SET `type` = 'bool' WHERE `name` = 'protect_attachment_url';
UPDATE `vblog_option` SET `type` = 'bool' WHERE `name` = 'url_rewrite';
UPDATE `vblog_option` SET `type` = 'serialize' WHERE `name` IN('url_page','url_category','url_post');
UPDATE `vblog_option` SET `type` = 'int' WHERE `name` = 'upload_allow_maxsize';
UPDATE `vblog_option` SET `type` = 'int' WHERE `name` = 'blog_list_num';
UPDATE `vblog_option` SET `type` = 'serialize' WHERE `name` IN('cache_adapter','cache_file','cache_memcache', 'cache_redis', 'cache_db');
UPDATE `vblog_option` SET `type` = 'serialize' WHERE `name` = 'plugin_hooks';

--20200514 - 增加简单的浏览器数统计
ALTER TABLE `vblog_content`
    ADD COLUMN `viewNum` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `password`;

--20200516 - 增加评论需要输入邮箱限制
INSERT INTO `vblog_option` (`name`, `value`, `category`, `type`) VALUES ('comment_email_require', '1', 'setting', 'bool');

--20200519 - base_url 更新为 core 配置 - Call OptionService::updateCoreConfig();
UPDATE `vblog_option` SET `category` = 'core' WHERE `vblog_option`.`name` = 'base_url';
