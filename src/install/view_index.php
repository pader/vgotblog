<?php !defined('BASE_PATH') && exit; ?>

<div class="jumbotron" style="margin-top: 30px;">
	<h1 class="display-4">欢迎使用 VGOT Blog</h1>
	<p class="lead">VGOT Blog 是一款方便搭建、体验简洁且功能完善，并且具有良好的可扩展性的 PHP 开源博客。</p>
</div>

<div class="text-center">
	<a class="btn btn-primary" href="<?=\vgot\Web\Url::site('install/env')?>"><span class="glyphicon glyphicon-glass"></span> 开始安装</a>
</div>
