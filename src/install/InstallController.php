<?php

namespace install;

use app\models\Option;
use app\models\User;
use app\services\EncryptService;
use app\services\OptionService;
use vgot\Core\Controller;
use vgot\Database\DB;
use vgot\Exceptions\DatabaseException;
use vgot\Web\Url;

class InstallController extends Controller
{

	public function __init()
	{
		$p = $this->router->parse();

		//已经安装的系统不允许再次执行安装程序
		if (is_file(BASE_PATH.'/res/config.php') && $p['real'] != 'install/done') {
			header('Location: '.Url::site('install/done'));
			exit;
		}

		define('STATIC_URL', '../static/');
	}

	/**
	 * 欢迎界面
	 */
	public function index()
	{
		$this->render('index', ['step'=>1]);
	}

	/**
	 * 环境检测
	 */
	public function env()
	{
		$requires = [
			[
				'title' => 'PHP 版本',
				'pass' => PHP_VERSION_ID >= 50400,
				'message' => 'PHP 版本不能低于 5.4'
			], [
				'title' => 'MySQL 扩展',
				'pass' => extension_loaded('pdo') || extension_loaded('mysqli'),
				'message' => '需要启用 pdo 或 mysqli 扩展连接数据库。'
			], [
				'title' => 'OpenSSL',
				'pass' => extension_loaded('openssl'),
				'message' => '需要启用 OpenSSL 扩展对用户登录信息进行加密。'
			], [
				'title' => 'res 目录可写',
				'pass' => is_writable(BASE_PATH.'/res'),
				'message' => 'res 目录需要写入权限以生成缓存和配置文件。'
			], [
				'title' => 'PATH_INFO',
				'pass' => isset($_SERVER['PATH_INFO']),
				'message' => 'PATH_INFO 为伪静态和 URL 提供个性化支持。'
			]
		];

		$allPassed = !in_array(false, array_column($requires, 'pass'), true);
		$step = 2;

		//支持必要的扩展
		$this->render('env', compact('requires', 'allPassed', 'step'));
	}

	/**
	 * 配置
	 *
	 * 数据库、站点名称、管理员账号
	 */
	public function config() {
		if ($_SERVER['REQUEST_METHOD'] != 'POST') {
			$this->render('config', ['step'=>3]);
			return;
		}

		$input = $this->input;

		$dbHost = $input->post('dbhost', 'localhost');
		$dbName = $input->post('dbname', 'root');
		$dbPort = $input->post('dbport', 3306);
		$dbPrefix = $input->post('dbprefix', 'vblog_');
		$dbUsername = $input->post('dbusername');
		$dbPassword = $input->post('dbpassword');
		$siteTitle = $input->post('title');
		$adminUsername = $input->post('adminusername');
		$adminPassword = $input->post('adminpassword');
		$adminEmail = $input->post('adminemail', 'admin@admin.com');

		//连接数据库
		$dbConfig = [
			'dsn' => '',
			'host' => $dbHost,
			'port' => $dbPort,
			'username' => $dbUsername,
			'password' => $dbPassword,
			'database' => null,
			'table_prefix' => $dbPrefix,
			'type' => 'mysql',
			'driver' => 'mysqli',
			'pconnect' => false,
			'charset' => 'utf8mb4',
			'timeout' => 5, //connect timeout seconds
			'collate' => 'utf8mb4_general_ci',
			'query_builder' => true,
			'debug' => true
		];

		$this->config->setAll([
			'default_connection' => 'main',
			'main' => $dbConfig
		], 'databases', false);

		try {
			$db = DB::connection();

			//创建数据库
			$existsDb = $db->query("SHOW DATABASES LIKE '$dbName'")->scalar();
			if (!$existsDb) {
				$db->query("CREATE DATABASE IF NOT EXISTS `$dbName` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci");
			}

			$db->query("USE `$dbName`");
			$dbConfig['database'] = $dbName;
		} catch (DatabaseException $e) {
			ret(false, $e->getMessage().': '.$e->getCode().':'.$e->getError());
		}

		//初始化数据表
		$sqlContent = file_get_contents(__DIR__ . '/vgotblog.sql');

		if ($dbPrefix != 'vblog_') {
			$sqlContent = str_replace('`vblog_', '`'.$dbPrefix, $sqlContent);
		}

		try {
			foreach (preg_split('/;[\r\n]/', $sqlContent) as $sql) {
				$sql = trim($sql);
				if ($sql) {
					$db->query($sql);
					//echo substr($sql, 0, 50) . "..\n";
				}
			}
		} catch (DatabaseException $e) {
			ret(false, $e->getMessage().': '.$e->getError());
		}

		//生成安全密钥
		$secretKey = base64_encode(openssl_random_pseudo_bytes(20));
		$this->config->set('auth_key', $secretKey);

		Option::set('title', $siteTitle, 'setting');

		//生成核心配置
		$this->config->set('main', $dbConfig, 'databases');
		OptionService::updateCoreConfig();

		//清空已有缓存
		if (is_dir(RES_PATH.'/cache')) {
			$this->cleanDir(RES_PATH.'/cache');
		}

		//生成管理员用户
		User::insert([
			'username' => $adminUsername,
			'password' => EncryptService::passwordHash($adminPassword),
			'mail' => $adminEmail,
			'nickname' => $adminUsername,
			'regtime' => time(),
			'group' => 'admin'
		]);

		ret(true, '安装成功');
	}

	/**
	 * 生成配置文件
	 *
	 * 锁定安装，显示完成界面
	 * 提示删除 install 目录
	 */
	public function done()
	{
		$this->render('done', ['step'=>4]);
	}

	protected function render($name, $vars=null, $return=false) {
		$this->view->vars('subView', 'view_'.$name);
		return parent::render('view_layout', $vars, $return);
	}

	protected function cleanDir($dir) {
		if (($handle = opendir($dir)) !== false) {
			while (($path = readdir($handle)) !== false) {
				if ($path == '.' || $path == '..') {
					continue;
				}

				$fullPath = $dir . DIRECTORY_SEPARATOR . $path;

				if (is_dir($fullPath)) {
					$this->cleanDir($fullPath);
					rmdir($fullPath);
				} else {
					unlink($fullPath);
				}
			}
			closedir($handle);
		}
	}

}