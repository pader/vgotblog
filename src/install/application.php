<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2017/4/23
 * Time: 02:26
 */

return [
	'id' => 'vgotblog_installer',
	'base_url' => '',
	'entry_file' => '',

	'providers' => [
		'schema' => [
			'class' => 'install\DBSchema'
		],
		'passwordHash' => [
			'class' => 'app\libs\PasswordHash',
			'args' => [
				'iteration_count_log2' => 8,
				'portable_hashes' => true
			]
		]
	],

	//Output
	'output_charset' => 'utf-8',
	'output_gzip' => true,
	'output_gzip_level' => 8,
	'output_gzip_minlen' => 1024, //1KB
	'output_gzip_force_soft' => false, //是否强制使用框架自带的gzip压缩，否则会检测是否可以启用PHP内置压缩
];