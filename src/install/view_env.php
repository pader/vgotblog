<?php !defined('BASE_PATH') && exit; ?>
<style type="text/css">
.chkico {padding:0 !important;}
.chkico .iconfont {font-size:20px; line-height:45px; font-weight:bold; padding:0 !important;}
</style>

<table class="table table-condensed">
	<thead>
		<tr>
			<th>检查项</th>
			<th class="text-center">检查结果</th>
			<th>介绍</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($requires as $row) { ?>
		<tr>
			<td><?=$row['title']?></td>
			<td class="chkico text-center"><span class="iconfont <?=$row['pass'] ? 'icon-seleted text-success' : 'icon-reeor-fill text-danger'?>"></span></td>
			<td><?=$row['message']?></td>
		</tr>
	<?php } ?>
	</tbody>
</table>

<div class="text-center">
	<?php if ($allPassed) { ?>
		<a class="btn btn-primary" href="<?=\vgot\Web\Url::site('install/config')?>">下一步 <span class="glyphicon glyphicon-arrow-right"></span></a>
	<?php } else { ?>
		<button type="button" class="btn btn-warning" onclick="location.reload();"><span class="glyphicon glyphicon-refresh"></span> 重新检查</button>
	<?php } ?>
<div>