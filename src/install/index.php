<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2020/05/20
 * Time: 01:25
 */
use vgot\Boot;

ini_set('display_errors', 'On');
ini_set('error_reporting', E_ALL);

define('BASE_PATH', realpath(__DIR__.'/..'));
define('RES_PATH', BASE_PATH.'/res');

require BASE_PATH.'/../../vgot_framework/src/framework/Boot.php';
require BASE_PATH.'/app/helpers/common.php';

Boot::addNamespaces([
	'install' => __DIR__,
	'app' => BASE_PATH.'/app'
]);

Boot::systemConfig([
	'controller_namespace' => '\install',
	'config_path' => __DIR__,
	'views_path' => __DIR__,
	'common_config_path' => BASE_PATH.'/res',
	'common_views_path' => BASE_PATH.'/../../vgot_framework/src/views'
]);

Boot::run();
