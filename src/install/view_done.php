<?php !defined('BASE_PATH') && exit; ?>

<h1 class="text-center" style="margin:50px 0;">恭喜您，安装完成！</h1>
<p class="text-info text-center" style="margin:30px 0;">现在可以开始使用您的博客了，安装已被锁定，您也可以删除 install 目录。</p>

<div class="text-center">
	<a class="btn btn-primary" href="../">打开博客</a>
	<a class="btn btn-secondary" target="_blank" href="../index.php/admin">登录后台</a>
</div>