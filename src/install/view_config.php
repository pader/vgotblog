<?php !defined('BASE_PATH') && exit; ?>
<script type="text/javascript">
function install(form) {
	if (form.adminpassword.value != form.passwordrepeat.value) {
		alert("两次输入的管理员密码不一致！");
		return false;
	}

	const inputBtn = $("#inputBtn");
	const inputLoading = $("#inputLoading");
	const progress = $("#progress");

	inputBtn.hide();
	inputLoading.show();
	progress.addClass("progress-bar-animated");

	$.ajax({
		method: "POST",
		url: "?step=install/config",
		data: $(form).serialize(),
		success(res) {
			if (!res.status) {
				inputBtn.show();
				inputLoading.hide();
				progress.removeClass("progress-bar-animated");
				alert(res.message);
			} else {
				location.href = "?step=install/done";
			}
		}
	});

	return false;
}
</script>

<form method="post" onsubmit="return install(this);">
	<div class="form-group row">
		<label for="inputDBHost" class="col-sm-4 col-form-label text-right">数据库地址</label>
		<div class="col-sm-6">
			<input type="text" name="dbhost" class="form-control" id="inputDBHost" placeholder="MySQL 数据库地址" value="localhost" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="inputDBName" class="col-sm-4 col-form-label text-right">数据库</label>
		<div class="col-sm-6">
			<input type="text" name="dbname" class="form-control" id="inputDBName" value="vgotblog" placeholder="安装 vgotblog 的数据库库名" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="inputTablePrefix" class="col-sm-4 col-form-label text-right">数据库表前缀</label>
		<div class="col-sm-6">
			<input type="text" name="dbprefix" class="form-control" id="inputTablePrefix" value="vblog_">
		</div>
	</div>
	<div class="form-group row">
		<label for="inputDBUsername" class="col-sm-4 col-form-label text-right">数据库用户名</label>
		<div class="col-sm-6">
			<input type="text" name="dbusername" class="form-control" id="inputDBUsername" value="root" placeholder="数据库用户名" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="inputDBPassword" class="col-sm-4 col-form-label text-right">数据库密码</label>
		<div class="col-sm-6">
			<input type="password" name="dbpassword" class="form-control" id="inputDBPassword" placeholder="数据库密码">
		</div>
	</div>
	<hr />
	<div class="form-group row">
		<label for="inputBlogName" class="col-sm-4 col-form-label text-right">博客名称</label>
		<div class="col-sm-6">
			<input type="text" name="title" class="form-control" id="inputBlogName" value="VGOT Blog" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="inputAdminUsername" class="col-sm-4 col-form-label text-right">管理员用户名</label>
		<div class="col-sm-6">
			<input type="text" name="adminusername" class="form-control" id="inputAdminUsername" value="admin" placeholder="管理员账号用户名" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="inputAdminEmail" class="col-sm-4 col-form-label text-right">管理员邮箱</label>
		<div class="col-sm-6">
			<input type="email" name="adminemail" class="form-control" id="inputAdminEmail" value="admin@example.com" placeholder="管理员邮箱" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="inputAdminPassword" class="col-sm-4 col-form-label text-right">管理员密码</label>
		<div class="col-sm-6">
			<input type="password" name="adminpassword" class="form-control" id="inputAdminPassword" placeholder="管理员密码" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="inputAdminPassword2" class="col-sm-4 col-form-label text-right">确认管理员密码</label>
		<div class="col-sm-6">
			<input type="password" name="passwordrepeat" class="form-control" id="inputAdminPassword2" placeholder="再次输入管理号密码" required>
		</div>
	</div>

	<div class="form-group">
		<div class="text-center">
			<button type="submit" class="btn btn-primary" id="inputBtn">安装 <span class="glyphicon glyphicon-arrow-right"></span></button>
			<div class="spinner-border text-primary" role="status" id="inputLoading" style="display:none;">
				<span class="sr-only">Loading...</span>
			</div>
		</div>
	</div>
</form>
