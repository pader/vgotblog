<?php !defined('BASE_PATH') && exit; ?>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>VGOT Blog 安装程序</title>
<link rel="stylesheet" href="<?=STATIC_URL?>lib/bootstrap4/css/bootstrap.min.css">
<link rel="stylesheet" href="//at.alicdn.com/t/font_1832098_qaqx165l4z.css">
<style type="text/css">
.step {padding:30px 30px;}
.step .col {text-align:center;}
.step .iconfont {font-size:30px; vertical-align:middle;}
</style>
</head>
<body>
<div class="container" style="padding:30px 0;">

	<h1 class="text-center"><span class="badge badge-primary pull-left">VGOT <span class="badge badge-light">BLOG</span></span></h1>

	<div class="step">
		<div class="align-middle">
			<div class="row">
				<div class="col align-middle text-primary">
					<span class="iconfont icon-zifu-num"></span>
					<p class="mb-1"><strong>开始</strong></p>
				</div>
				<span class="col border <?php if ($step >= 2) { ?>border-primary <?php } ?>mx-3 my-auto"></span>
				<div class="col align-middle <?=($step >= 2 ? 'text-primary' : 'text-muted')?>">
					<span class="iconfont icon-zifu-num2"></span>
					<p class="mb-1"><strong>环境检查</strong></p>
				</div>
				<span class="col border <?php if ($step >= 3) { ?>border-primary <?php } ?>mx-3 my-auto"></span>
				<div class="col align-middle <?=($step >= 3 ? 'text-primary' : 'text-muted')?>">
					<span class="iconfont icon-zifu-num3"></span>
					<p class="mb-1">程序配置</p>
				</div>
				<span class="col border <?php if ($step >= 4) { ?>border-primary <?php } ?>mx-3 my-auto"></span>
				<div class="col align-middle <?=($step >= 4 ? 'text-primary' : 'text-muted')?>">
					<span class="iconfont icon-zifu-num1"></span>
					<p class="mb-1">安装完成</p>
				</div>
			</div>
		</div>
	</div>

	<?php $this->render($subView); ?>
</div>
<script src="<?=STATIC_URL?>lib/jquery-3.4.1.min.js"></script>
<script src="<?=STATIC_URL?>lib/bootstrap4/js/bootstrap.min.js"></script>
</body>
</html>