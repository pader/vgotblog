CREATE TABLE `vblog_attachment` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL,
  `filename` varchar(200) NOT NULL,
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(200) NOT NULL,
  `filetype` varchar(10) NOT NULL,
  `uploaded` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `vblog_comment` (
  `cmtid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cid` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `reply_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created` int(10) UNSIGNED NOT NULL,
  `author` varchar(50) NOT NULL,
  `uid` int(8) UNSIGNED NOT NULL DEFAULT '0',
  `mail` varchar(64) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `url` varchar(128) NOT NULL DEFAULT '',
  `ip` char(15) NOT NULL DEFAULT '',
  `status` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cmtid`),
  KEY `cid_parent` (`cid`,`parent_id`),
  KEY `created` (`created`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `vblog_content` (
  `cid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `alias` varchar(128) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `uid` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `created` int(10) UNSIGNED NOT NULL,
  `modified` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `type` enum('post','page','draft','trash') NOT NULL DEFAULT 'post',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0:草稿,1:正常,2:隐藏,3:私密',
  `password` varchar(32) NOT NULL DEFAULT '',
  `viewNum` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `allowComment` tinyint(1) NOT NULL DEFAULT '1',
  `commentNum` mediumint(8) DEFAULT '0',
  PRIMARY KEY (`cid`),
  UNIQUE KEY `alias` (`alias`),
  KEY `uid` (`uid`),
  KEY `type_created` (`type`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `vblog_content` (`cid`, `alias`, `title`, `created`, `uid`, `content`, `modified`, `type`, `status`, `allowComment`, `commentNum`) VALUES
	(1, 'welcome-vgot-blog', '欢迎使用 VGOT Blog', UNIX_TIMESTAMP(), 1, '这是来自 VGOT Blog 的第一篇文章。', 0, 'post', 1, 1, 0),
	(2, 'playground', 'Playground', UNIX_TIMESTAMP(), 1, 'This is playground', 0, 'page', 1, 0, 0);

CREATE TABLE `vblog_link` (
  `lid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`lid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `vblog_meta` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `type` enum('category','tag') NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`mid`),
  KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `vblog_meta` (`mid`, `name`, `alias`, `description`, `type`, `count`) VALUES
	(1, '默认分类', '1', '', 'category', 1);

CREATE TABLE `vblog_option` (
  `name` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `category` enum('core','setting','system','user') NOT NULL DEFAULT 'system',
  `type` enum('string','int','bool','float','serialize') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `vblog_option` (`name`, `value`, `category`, `type`) VALUES
('title', 'VGOT Blog', 'setting', 'string'),
('protect_attachment_url', '0', 'setting', 'bool'),
('description', '简洁却不简单，VgotBlog PHP 博客系统。', 'setting', 'string'),
('base_url', '', 'core', 'string'),
('keywords', 'vgotblog,phpblog', 'setting', 'string'),
('url_rewrite', '0', 'core', 'string'),
('url_page', '/{alias}/', 'core', 'string'),
('url_category', '/category/{alias}/', 'core', 'string'),
('url_post', '/archives/{alias}.html', 'core', 'string'),
('upload_allow_exts', 'jpg|jpeg|gif|png|zip|rar|gz|bz2|doc|docx|xls|xlsx|ppt|pptx|txt', 'setting', 'string'),
('upload_allow_maxsize', '10240', 'setting', 'string'),
('cookie_prefix', 'vblog_', 'setting', 'string'),
('blog_list_num', '9', 'setting', 'int'),
('nav', 'a:5:{s:6:\"fields\";a:7:{i:0;s:2:\"id\";i:1;s:4:\"name\";i:2;s:4:\"type\";i:3;s:4:\"sort\";i:4;s:3:\"url\";i:5;s:7:\"newpage\";i:6;s:7:\"visible\";}s:11:\"autoIdField\";s:2:\"id\";s:6:\"autoId\";i:17;s:8:\"modified\";i:1590327127;s:4:\"data\";a:2:{i:1;a:7:{i:0;i:1;i:1;s:4:\"Home\";i:2;s:6:\"system\";i:3;s:1:\"0\";i:4;s:0:\"\";i:5;i:0;i:6;i:1;}i:16;a:7:{i:0;i:16;i:1;s:10:\"Playground\";i:2;s:4:\"page\";i:3;i:99;i:4;s:1:\"2\";i:5;i:0;i:6;i:1;}}}', 'setting', 'string'),
('cache_adapter', 'file', 'core', 'string'),
('cache_file', 'a:3:{s:8:\"dirLevel\";s:1:\"1\";s:13:\"gcProbability\";s:2:\"10\";s:13:\"cacheInMemory\";s:1:\"1\";}', 'core', 'serialize'),
('cache_memcache', 'a:3:{s:4:\"host\";s:9:\"127.0.0.1\";s:4:\"port\";s:5:\"11211\";s:9:\"keyPrefix\";s:6:\"vblog_\";}', 'core', 'serialize'),
('cache_redis', 'a:5:{s:4:\"host\";s:9:\"127.0.0.1\";s:4:\"port\";s:4:\"6379\";s:8:\"password\";s:0:\"\";s:8:\"database\";s:1:\"0\";s:9:\"keyPrefix\";s:6:\"vblog_\";}', 'core', 'serialize'),
('cache_db', 'a:1:{s:13:\"gcProbability\";s:2:\"10\";}', 'core', 'serialize'),
('plugin_enables', 'a:0:{}', 'setting', 'serialize'),
('plugin_hooks', 'a:0:{}', 'setting', 'serialize'),
('comment_email_require', '0', 'setting', 'bool');

ALTER TABLE `vblog_option`
    ADD PRIMARY KEY (`name`),
    ADD KEY `category` (`category`);

CREATE TABLE `vblog_relationship` (
  `mid` int(10) unsigned NOT NULL,
  `cid` int(10) unsigned NOT NULL,
  KEY `mid` (`mid`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `vblog_relationship` (`mid`, `cid`) VALUES
	(1, 1);

CREATE TABLE IF NOT EXISTS `vblog_user` (
  `uid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(64) NOT NULL,
  `mail` varchar(64) NOT NULL,
  `url` varchar(128) NOT NULL DEFAULT '',
  `nickname` varchar(128) NOT NULL DEFAULT '',
  `regtime` int(10) unsigned NOT NULL,
  `logged` int(10) unsigned NOT NULL DEFAULT '0',
  `group` enum('admin','writer','visitor') NOT NULL DEFAULT 'visitor',
  `status` enum('normal','ban','disable') NOT NULL DEFAULT 'normal',
  `description` text NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `mail` (`mail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
