<?php
return [
	'name' => 'Hello World',
	'description' => 'VGOT Blog 演示插件，唯一的功能是将标题替换成 "Hello World"。',
	'version' => '1.0.0',
	'hooks' => [
		'system_init' => [
			'post' => '\plugin\hello_world\Hook::changeTitle'
		]
	],
	'author' => 'VGOT Blog',
	'url' => 'https://blog.vgot.net/'
];
