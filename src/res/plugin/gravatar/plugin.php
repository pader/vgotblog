<?php
return [
	'name' => 'Gravatar',
	'description' => '全球公认头像服务 Gravatar。',
	'version' => '1.0.0',
	'entry_class' => '\plugin\gravatar\Gravatar',
	'with_options' => true,
	'author' => 'VGOT Blog',
	'url' => 'https://blog.vgot.net/'
];
