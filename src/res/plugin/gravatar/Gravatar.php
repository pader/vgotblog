<?php

namespace plugin\gravatar;

class Gravatar extends \app\libs\Plugin
{

	private $sizes = [
		's' => '16',
		'm' => '52',
		'b' => '180'
	];

	private $url;
	private $d;
	private $r;

	protected function __init() {
		$this->url = $this->get('url', 'https://www.gravatar.com/avatar/');
		$this->d = $this->get('default', 'mp');
		$this->r = $this->get('rating', 'g');
	}

	public function hooks() {
		return [
			'avatar' => [
				'pre' => 'gravatar'
			]
		];
	}

	public function options() {
		return [
			'url' => [
				'name' => '镜像地址',
				'desc' => '选择 Gravatar 的服务镜像',
				'type' => 'select',
				'default' => 'https://www.gravatar.com/avatar/',
				'options' => [
					'https://www.gravatar.com/avatar/' => 'Gravatar 官网',
					'https://cn.gravatar.com/avatar/' => 'Gravatar CN',
					'https://en.gravatar.com/avatar/' => 'Gravatar EN',
					'https://secure.gravatar.com/avatar/' => 'Gravatar Secure',
					'https://dn-qiniu-avatar.qbox.me/avatar/' => '七牛',
					'https://cdn.v2ex.com/gravatar/' => 'V2EX',
					'https://gravatar.loli.net/avatar/' => 'Loli',
					'https://sdn.geekzu.org/avatar/' => '极客族',
				],
			],
			'default' => [
				'name' => '默认头像',
				'desc' => '选择默认头像风格',
				'type' => 'radio',
				'default' => 'mp',
				'options' => [
					'404' => '404',
					'mp' => 'mp',
					'identicon' => 'identicon',
					'monsterid' => 'monsterid',
					'wavatar' => 'wavatar',
					'retro' => 'retro',
					'robohash' => 'robohash',
					'blank' => 'blank'
				]
			],
			'rating' => [
				'name' => '头像评级',
				'desc' => '选择头像限制评级',
				'type' => 'radio',
				'options' => [
					'g' => '大众级，所有年龄均适合',
					'pg' => '普通级，可能包含粗鲁手势，挑逗性的着装，少许粗话，及轻微暴力等内容。',
					'r' => '限制级R，可能包含亵渎，极度暴力，裸体或吸毒等内容。',
					'x' => '限制级X，可能包含赤裸的性爱或极端暴力内容。'
				],
				'default' => 'g'
			]
		];
	}

	public function gravatar($info, $size) {
		$hash = md5(strtolower(isset($info['mail']) ? $info['mail'] : ''));
		$s = $this->sizes[$size] ?? '80';
		return $this->url.$hash."?s={$s}&d={$this->d}&r={$this->r}";
	}

}