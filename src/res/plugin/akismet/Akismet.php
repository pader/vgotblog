<?php
/**
 * Wordpress Akismet Plugin
 *
 * @author pader
 * @package vgotblog\plugin
 */

namespace plugin\akismet;

use app\libs\Plugin;
use app\models\Comment;
use vgot\Web\Url;

/**
 * Wordpress Akismet Plugin
 * @package plugin\akismet
 */
class Akismet extends Plugin
{

	public function __init() {
		require __DIR__.'/function.php';
	}

	public function hooks() {
		return [
			'comment_post' => [
				'pre' => 'checkComment'
			]
		];
	}

	public function options() {
		return [
			'apiKey' => [
				'type' => 'text',
				'default' => '',
				'name' => 'Akismet API Key',
				'desc' => '由 <a href="https://akismet.com/" target="_blank">Akismet</a> 官网提供的 API Key，需要自己申请。'
			],
			'isTest' => [
				'name' => '测试阶段',
				'desc' => '测试时，提交的内容会进行检查但不会真实标记来源为 SPAM。',
				'type' => 'radio',
				'default' => 'false',
				'options' => [
					1 => '是',
					0 => '否'
				]
			],
			'checkLevel' => [
				'name' => '检查评论等级',
				'desc' => '右侧包含左侧，选择管理员代表检查所有人的评论。',
				'type' => 'radio',
				'default' => 0,
				'options' => [
					0 => '游客',
					1 => '普通注册用户',
					2 => '创作者',
					3 => '管理员'
				]
			],
			'spamRet' => [
				'name' => 'SPAM 响应形式',
				'desc' => '如果检查确认为垃圾评论的反应形式',
				'type' => 'radio',
				'default' => 'spam',
				'options' => [
					'spam' => '进入垃圾评论列表',
					'pass' => '假装评论成功',
					'forbid' => '提示评论失败'
				]
			]
		];
	}

	//Todo: 需实现后台配置合法性检查机制
	public function checkConfig() {
		$apiKey = $this->get('apiKey');

		if ($apiKey) {
			$baseUrl = Url::base(true);
			if (!akismet_verify_key($apiKey, $baseUrl)) {
				ret(false, 'API Key is invalid!');
			}
		}
	}

	public function checkComment($post, $comment) {
		$app = getApp();

		$apiKey = $this->get('apiKey');

		if (!$apiKey) {
			return;
		}

		switch ($this->get('checkLevel', 0)) {
			case 0: $isCheck = !$_ENV['uid']; break;
			case 1: $isCheck = !$_ENV['uid'] || $_ENV['user']['group'] == 'visitor'; break;
			case 2: $isCheck = !$_ENV['uid'] || $_ENV['user']['group'] != 'admin'; break;
			default: $isCheck = true;
		}

		if (!$isCheck) {
			return;
		}

		if ($_ENV['uid']) {
			$comment['author'] = $_ENV['user']['nickname'];
			$comment['email'] = $_ENV['user']['mail'];
			$comment['url'] = $_ENV['user']['url'];
		}

		// Call to comment check
		$data = array(
			'blog' => Url::base(true),
			'user_ip' => $app->input->clientIp(),
			'user_agent' => $app->input->server('HTTP_USER_AGENT'),
			'referrer' => $app->input->server('HTTP_REFERER'),
			'permalink' => $post['url'],
			'comment_type' => 'comment',
			'comment_author' => $comment['author'],
			'comment_author_email' => $comment['email'],
			'comment_author_url' => $comment['url'],
			'comment_content' => $comment['content'],
			'is_test' => $this->get('isTest', false)
		);

		$isSpam = akismet_comment_check($apiKey, $data);

		if ($isSpam) {
			$this->set('spamCount', $this->get('spamCount', 0) + 1);

			switch ($this->get('spamRet', 'forbid')) {
				case 'forbid': ret(false, '很抱歉，评论失败！'); break;
				case 'pass': ret(true, '成功发表评论', ['commentId'=>0]); break;
				default:
					return [
						'status' => Comment::STATUS_SPAM
					];
			}
		}
	}

}