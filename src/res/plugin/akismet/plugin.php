<?php
return [
	'name' => 'Akismet Anti-Spam',
	'description' => '由 Wordpress 提供的 Akismet 反垃圾评论服务。',
	'author' => 'VGOT Blog',
	'url' => 'https://akismet.com/',
	'version' => '1.0.0',
	'entry_class' => '\plugin\akismet\Akismet',
	'with_options' => true
];
