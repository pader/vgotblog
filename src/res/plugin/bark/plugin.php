<?php
return [
	'name' => 'Bark 通知',
	'description' => '可以在有新评论的时候通过 Bark 发送手机通知给博主。',
	'version' => '1.0.0',
	'entry_class' => '\plugin\bark\Bark',
	'with_options' => true,
	'author' => 'VGOT Blog',
	'url' => 'https://blog.vgot.net/'
];
