<?php

namespace plugin\bark;

use app\libs\Plugin;

class Bark extends Plugin
{

	public function hooks()
	{
		return [
			'comment_post' => [
				'post' => 'sendCommentNotify'
			]
		];
	}

	public function options() {
		return [
			'barkId' => [
				'type' => 'text',
				'default' => '',
				'name' => 'BarkId',
				'desc' => '输入 Bark Url 中的 ID 部分'
			]
		];
	}

	public function sendCommentNotify($content, $comment)
	{
		//不用给自己发送
		if ($content['uid'] == $_ENV['uid']) return;

		$title = opt('title');
		$author = $_ENV['uid'] ? $_ENV['user']['nickname'] : $comment['author'];
		$str = "{$author} 在《{$content['title']}》中发表了评论。";

		$this->sendBark($title, $str, $content['url']);
	}

	private function sendBark($title, $content, $url=null)
	{
		$barkId = $this->get('barkId');

		if (!$barkId) {
			return;
		}

		$api = 'https://api.day.app/'.$barkId.'/'.urlencode($title).'/'.urlencode($content);

		if ($url) {
			$api .= '?url='.urlencode($url);
		}

		$ci = curl_init($api);
		curl_setopt($ci, CURLOPT_RETURNTRANSFER, true);
		curl_exec($ci);
		curl_close($ci);
	}

}