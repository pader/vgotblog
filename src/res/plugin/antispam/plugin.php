<?php
return [
	'name' => '简易反垃圾评论',
	'description' => '简单的反垃圾评论服务，检查评论昵称、内容是否有中文字符，没有则认为是可疑垃圾。',
	'author' => 'VGOT Blog',
	'url' => 'https://blog.vgot.net/',
	'version' => '1.0.0',
	'entry_class' => '\plugin\antispam\AntiSpam',
	'with_options' => true
];
