<?php

namespace plugin\antispam;

use app\libs\Plugin;
use app\models\Comment;

class AntiSpam extends Plugin
{

	public function hooks() {
		return [
			'comment_post' => [
				'pre' => 'checkComment'
			]
		];
	}

	public function checkComment($post, $comment) {
		$app = getApp();

		switch ($this->get('checkLevel', 0)) {
			case 0: $isCheck = !$_ENV['uid']; break;
			case 1: $isCheck = !$_ENV['uid'] || $_ENV['user']['group'] == 'visitor'; break;
			case 2: $isCheck = !$_ENV['uid'] || $_ENV['user']['group'] != 'admin'; break;
			default: $isCheck = true;
		}

		if (!$isCheck) {
			return;
		}

		if ($_ENV['uid']) {
			$comment['author'] = $_ENV['user']['nickname'];
			$comment['email'] = $_ENV['user']['mail'];
			$comment['url'] = $_ENV['user']['url'];
		}

		$valid = $this->hasChinese($comment['author'].$comment['content']);

		if (!$valid) {
			switch ($this->get('spamRet', 'forbid')) {
				case 'forbid': ret(false, '很抱歉，评论失败！'); break;
				case 'pass': ret(true, '成功发表评论', ['commentId'=>0]); break;
				case 'check':
					return [
						'status' => Comment::STATUS_PENDING
					];
					break;
				default:
					return [
						'status' => Comment::STATUS_SPAM
					];
			}
		}
	}

	private function hasChinese($text)
	{
		return (bool)preg_match_all('/[一-龥]/u', $text);
	}

	public function options() {
		return [
			'checkLevel' => [
				'name' => '检查评论等级',
				'desc' => '右侧包含左侧，选择管理员代表检查所有人的评论。',
				'type' => 'radio',
				'default' => 0,
				'options' => [
					0 => '游客',
					1 => '普通注册用户',
					2 => '创作者',
					3 => '管理员'
				]
			],
			'spamRet' => [
				'name' => 'SPAM 响应形式',
				'desc' => '如果检查确认为垃圾评论的反应形式',
				'type' => 'radio',
				'default' => 'spam',
				'options' => [
					'spam' => '进入垃圾评论列表',
					'check' => '进入待审核评论列表',
					'pass' => '假装评论成功',
					'forbid' => '提示评论失败'
				]
			]
		];
	}

}