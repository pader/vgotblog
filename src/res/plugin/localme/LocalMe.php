<?php

namespace plugin\localme;

use app\libs\Plugin;

class LocalMe extends Plugin
{

	public function hooks() {
		return [
			'page_foot' => [
				'pre' => 'statCode',
				'post' => 'icp'
			]
		];
	}

	public function options() {
		return [
			'stat_code' => [
				'name' => '统计代码',
				'type' => 'textarea'
			],
			'icp' => [
				'name' => 'ICP备案信息',
				'type' => 'text'
			]
		];
	}

	public function statCode() {
		return $this->get('stat_code');
	}

	public function icp() {
		$icp = $this->get('icp');
		if ($icp) {
			return '<p class="text-center"><a href="http://beian.miit.gov.cn/" target="_blank">'.$icp.'</a></p>';
		}
	}

}