<?php
return [
	'name' => '我的本地化',
	'description' => '一些国内的本地化设置，可以添加统计代码，ICP备案号。',
	'version' => '1.0.0',
	'entry_class' => '\plugin\localme\LocalMe',
	'with_options' => true,
	'author' => 'VGOT Blog',
	'url' => 'https://blog.vgot.net/'
];
