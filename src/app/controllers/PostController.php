<?php

namespace app\controllers;

use app\libs\BlogController;
use app\libs\Pagination;
use app\models\Content;
use app\services\CommentService;
use app\services\ContentService;
use app\services\MetaService;
use vgot\Exceptions\HttpNotFoundException;

/**
 * 微博内容详情页控制器
 * @package app\controllers
 */
class PostController extends BlogController
{

	public function __init() {
		loadHelper('blog');
	}

	/**
	 * 文章详情页
	 *
	 * @throws HttpNotFoundException
	 */
	public function post() {
		$post = $this->getContentByType('post');
		$post['content'] = $this->contentFormat($post['content']);

		//该内容的分类
		$metas = MetaService::getCidMetas($post['cid']);

		$categories = MetaService::getCategories();
		$category = null;

		foreach ($categories as $row) {
			if (isset($metas[$row['mid']])) {
				$category = $row;
				unset($metas[$row['mid']]);
				break;
			}
		}

		if ($category) {
			$post['mid'] = $category['mid'];
			$post['category'] = $category['name'];
			$post['malias'] = $category['alias'];
		} else {
			$post['mid'] = $post['category'] = $post['malias'] = null;
		}

		$post['allowComment'] && $this->loadComments($post['cid']);
		$this->addViews($post['cid']);

		view('post', array(
			'TITLE' => $post['title'],
			'FLAG' => 'POST_'.$post['cid'],
			'post' => $post,
			'passwordChecked' => blogCheckPassword($post),
			'category' => $category,
			'tags' => $metas
		));
	}

	/**
	 * 自定义页面
	 *
	 * @throws HttpNotFoundException
	 */
	public function page()
	{
		$post = $this->getContentByType('page');
		$post['content'] = $this->contentFormat($post['content']);
		$post['allowComment'] && $this->loadComments($post['cid']);
		$this->addViews($post['cid']);

		view('page', array(
			'TITLE' => $post['title'],
			'FLAG' => 'PAGE_'.$post['cid'],
			'post' => $post,
			'passwordChecked' => blogCheckPassword($post)
		));
	}

	/**
	 * 验证输入的密码
	 */
	public function verify() {
		$cid = $this->input->post('cid', 'numval');
		$password = $this->input->post('password');

		$post = ContentService::getContentByUser($cid);

		if ($post['pwd'] && $password === $post['pwd']) {
			vsetcookie('arcpw_'.$cid, base64_encode($password), 86400);
			$url = ContentService::getPostUrl($post);
			header("Location: $url");
		} else {
			showMessage('error: 官码错误！');
		}
	}

	/**
	 * 获取内容详细数据
	 *
	 * @param string $type
	 * @return array
	 * @throws HttpNotFoundException
	 */
	private function getContentByType($type) {
		//匹配地址参数
		$where = ['AND', 'c.type'=>$type];

		foreach (array('cid', 'alias') as $p) {
			if (isset($_ENV['route_params'][$p])) {
				$where['c.'.$p] = $_ENV['route_params'][$p];
			}
		}

		if (count($where) == 0) {
			throw new HttpNotFoundException();
		}

		//获取数据
		Content::privacyWhere($where, $_ENV['uid'], 'c');
		$where['c.type'] = $type;

		$post = Content::find()->alias('c')->join('user u', 'uid', 'left')
			->select('c.cid,c.alias,c.title,c.content,c.created,c.uid,c.password,c.allowComment,c.commentNum,'
				.'u.username,u.nickname,u.status as userStatus,u.group as userGroup')
			->where($where)->get();

		if ($post === null) {
			throw new HttpNotFoundException();
		}

		return $post;
	}

	/**
	 * 内容格式化
	 *
	 * @param string $content
	 * @return string
	 */
	private function contentFormat($content) {
		//移除分页标签
		return str_replace('__page_break_tag__', '<a name="more"></a>', $content);
	}

	/**
	 * 增加浏览数
	 *
	 * @param int $cid
	 */
	private function addViews($cid) {
		Content::update(['^viewNum'=>'viewNum+1'], ['cid'=>$cid]);
	}

	/**
	 * 加载评论数据
	 *
	 * @param int $cid
	 * @throws \vgot\Exceptions\DatabaseException
	 */
	private function loadComments($cid) {
		$listPageNum = 20;
		$page = $this->input->get('page', 'intval');
		$pagination = new Pagination(['curPage'=>$page,'pageUrl' => '?page=*#comment', 'perPage'=>$listPageNum]);
		$comment = CommentService::fetchByCid($cid, $pagination->getStart(), $listPageNum);
		$pagination->initialize(['totalRows' => $comment['count']]);
		$pages = $pagination->makeLinks();
		$commentAuthor = $_ENV['uid'] ? null : CommentService::getAuthorInfo();
		$this->view->vars(compact('pages', 'comment', 'commentAuthor'));
	}

}