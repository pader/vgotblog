<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2020/2/1
 * Time: 14:21
 */

namespace app\controllers;

use app\libs\BlogController;
use app\models\Comment;
use app\models\User;
use app\services\CommentService;
use app\services\ContentService;
use app\services\OptionService;

class CommentController extends BlogController
{

	public function post() {
		$input = $this->input;
		$cid = $input->post('cid', 0, FILTER_VALIDATE_INT);
		$author = $input->post('author');
		$email = $input->post('email', '', FILTER_VALIDATE_EMAIL);
		$url = $input->post('url', '', FILTER_VALIDATE_URL);
		$content = $input->post('content', '', ['htmlspecialchars', 'trim']);
		$replyId = $input->post('reply_id', 0, FILTER_VALIDATE_INT);

		if (!csrfVerify('cid_'.$cid) || !$cid) {
			ret(false, '请求无效！');
		}

		if(!$content) {
			ret(false, '请输入评论内容！');
		}

		if (!$_ENV['uid']) {
			if(!$author) {
				ret(false, '请输入昵称、内容！');
			}

			if (opt('comment_email_require', true) && !$email) {
				ret(false, '请输入邮箱！');
			}
		}

		$post = ContentService::getContentByUser($cid);
		if (!$post) {
			ret(false, '该内容不存在！');
		}

		loadHelper('blog');
		if (!blogCheckPassword($post)) {
			ret(false, '尚未解锁该内容！');
		}

		if (!$post['allowComment']) {
			ret(false, '该内容不接受评论！');
		}

		if (!$_ENV['uid'] && User::find()->select('uid')->where(['username' => $author])->scalar()) {
			ret(false, '该昵称已经被注册，请换个名字再试！');
		}

		//回复评论处理
		$parentId = 0;

		if ($replyId) {
			$reply = Comment::getAvailableComment($cid, $replyId);

			if (!$reply) {
				ret(false, '回复的评论不存在！');
			}

			$parentId = $reply['parent_id'] ?: $replyId;
		}

		$post['url'] = ContentService::getPostUrl($post, true);

		/**
		 * 评论前置钩子
		 *
		 * 支持字段：
		 * - status: 评论的预置状态，若没有预置状态，则默认为 Comment::STATUS_PUBLIC 状态
		 */
		$preset = $this->hook->pre('comment_post', $post, compact('author', 'email', 'url', 'content'));

		$data = [
			'parent_id' => $parentId,
			'reply_id' => $replyId,
			'author' => $author,
			'mail' => $email ?: '',
			'url' => $url ?: '',
			'content' => $content
		];

		if (is_array($preset)) {
			$data = array_merge($data, array_intersect_key($preset, array_flip(['status'])));
		}

		$commentId = CommentService::addComment($cid, $data);

		//未登录用户暂保存输入的称呼等信息方便再次发表评论
		if (!$_ENV['uid']) {
			CommentService::saveAuthorInfo($author, $email, $url);
		}

		$post['url'] .= '#comment'.$commentId;
		$this->hook->post('comment_post', $post, compact('commentId', 'author', 'email', 'url', 'content'));

		ret(true, '成功发表评论', compact('commentId'));
	}

}