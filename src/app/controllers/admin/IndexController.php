<?php

namespace app\controllers\admin;

use app\libs\AdminController;

/**
 * 后台首页
 *
 * @package Controller
 */
class IndexController extends AdminController {

	public function index() {
		loadHelper('time');

		$software = explode(' ',$_SERVER['SERVER_SOFTWARE']);
		
		//Opcache 状态
		$opcache = false;

		if (function_exists('opcache_get_status')) {
			$opcacheStatus = opcache_get_status(false);
			if ($opcacheStatus['opcache_enabled']) {
				$opcache = $opcacheStatus['opcache_statistics']['hits'].' hits, '
					.$opcacheStatus['opcache_statistics']['misses'].' misses, '
					.number_format($opcacheStatus['opcache_statistics']['opcache_hit_rate'], 2, '.', '').'% hit rate';
			}
		}

		//MySQL 状态
		$mysqlVersion = $this->db->select('VERSION()')->scalar();
		$mysqlUptime = $this->db->query('SHOW GLOBAL STATUS LIKE \'Uptime\'')->scalar(1);

		$mysqlUptime = formatTimestamp($mysqlUptime);

		$this->render('admin/index', array(
			'TITLE' => '控制台',
			'software' => $software[0],
			'opcache' => $opcache,
			'mysqlVersion' => $mysqlVersion,
			'mysqlUptime' => $mysqlUptime
		));
	}

}
