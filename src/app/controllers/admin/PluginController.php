<?php

namespace app\controllers\admin;

use app\libs\AdminController;
use app\services\PluginService;

/**
 * 插件管理控制器
 * @package app\controllers\admin
 */
class PluginController extends AdminController
{

	public function index()
	{
		$plugins = PluginService::scanPlugins();
		$TITLE = '插件';
		$this->render('admin/plugin_index', compact('plugins', 'TITLE'));
	}

	/**
	 * 启用/禁用插件
	 */
	public function enable()
	{
		$id = $this->input->get('id');
		$do = $this->input->get('do', 'enable');

		try {
			if ($do == 'disable') {
				PluginService::disable($id);
				showMessage("success: 已停用插件 {$id} 。");
			} else {
				PluginService::enable($id);
				showMessage("success: 已启用插件 {$id}。");
			}
		} catch (\ErrorException $e) {
			showMessage('error: '.$e->getMessage());
		}
	}

	public function config()
	{
		$id = $this->input->get('id');

		try {
			$plugin = PluginService::getPlugin($id);
		} catch (\ErrorException $e) {
			showMessage('error: '.$e->getMessage());
		}

		if (!isset($plugin['entry_class'])) {
			showMessage('alert: 不能为没有 entry_class 的插件建立配置！');
		}

		$ref = new \ReflectionClass($plugin['entry_class']);
		$entry = $ref->newInstance($id);
		$options = $entry->options();

		if (empty($options)) {
			showMessage('alert: 该插件没有可用配置！');
		}

		if (!$this->isSubmit()) {
			return $this->render('admin/plugin_config', compact('plugin', 'entry', 'options'));
		}

		$config = $this->input->post('config', []);

		foreach ($config as $k => $v) {
			if (isset($options[$k])) {
				$entry->set($k, $v);
			}
		}

		showMessage("success: 成功保存插件配置。");
	}

}