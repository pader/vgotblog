<?php

namespace app\controllers\admin;

use app\libs\AdminController;
use app\libs\Pagination;
use app\services\AttachmentService;
use app\services\ContentService;

/**
 * 附件管理控制器
 *
 * @package Controller
 * @property \Model\Attachment $attachment
 * @property \Model\Content $content
 * @property \VF\Library\Pagination pagination
 */
class AttachmentController extends AdminController {

	/**
	 * 附件列表
	 */
	public function index() {
		//分页
		$page = $this->input->get('p', 'intval');
		$page < 1 && $page = 1;

		$pageConfig = array(
			'perPage' => 20,
			'curPage' => $page,
			'pageUrl' => siteUrl('admin/attachment').'?p=*'
		);

		$pagination = new Pagination($pageConfig);
		$start = $pagination->getStart();
		$data = AttachmentService::fetchList($start, $pageConfig['perPage']);

		$pages = '';

		if ($data['count']) {
			$pagination->initialize(array('totalRows'=>$data['count']));
			$pages = $pagination->makeLinks();
			$data['list'] && ContentService::listInjectCategory($data['list']);
		}

		$this->render('admin/attachment_index', array(
			'TITLE' => '附件管理',
			'count' => $data['count'],
			'files' => $data['list'],
			'pages' => $pages,
			'page' => $page
		));
	}

	/**
	 * 查看附件详情
	 */
	public function detail() {
		$aid = $this->input->get('aid', 'numval');
		$page = $this->input->get('p', 'intval');

		$attachment = $this->attachment->getDetail($aid);

		if (!$attachment) {
			showMessage('alert: 该文件不存在！');
		}

		$isImage = in_array($attachment['filetype'], array('jpg', 'jpeg', 'gif', 'png'));
		$fileUrl = baseUrl().$this->attachment->dir.'/'.$attachment['path'];
		
		$TITLE = '附件详情';
		$this->load->view('admin/attachment_detail', compact('TITLE', 'page', 'attachment', 'isImage', 'fileUrl'));
	}

	public function upload() {
		echo 'real upload';
	}

	/**
	 * 删除指定一个附件（通过 AJAX 请求）
	 */
	public function remove() {
		$aid = $this->input->post('aid', 'numval');

		$attach = $this->db->from('attachment')->where(array('aid'=>$aid))->get();

		if (!$attach) {
			ret(false, '附件不存在！');
		}

		if ($attach['cid'] == 0 || $_ENV['user']['group'] == 'admin') {
			$ret = AttachmentService::deleteFromRow($attach);
		} else {
			$uid = $this->db->from('content')->where(array('cid'=>$attach['cid']))->scalar();
			if ($uid == $_ENV['uid']) {
				$ret = AttachmentService::deleteFromRow($attach);
			} else {
				ret(false, '您无权删除该附件！');
			}
		}

		ret($ret, $ret ? '' : '删除失败');
	}

}
