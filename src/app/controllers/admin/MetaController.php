<?php

namespace app\controllers\admin;

use app\libs\AdminController;
use app\services\MetaService;
use vgot\Web\Url;

/**
 * Meta Admin Controller
 *
 * @package Controller
 */
class MetaController extends AdminController {

	public function category() {
		$categories = MetaService::getCategories();

		$this->render('admin/meta_category', array(
			'TITLE' => '管理分类',
			'categories' => $categories
		));
	}

	public function add() {
		if ($this->isSubmit()) {
			$name = $this->input->post('name', 'strip_tags');
			$alias = $this->input->post('alias', 'strip_tags|trim');
			$description = $this->input->post('description');

			if ($alias != '') {
				$aliasMeta = MetaService::getMeta($alias, MMeta::TYPE_CATEGORY, MMeta::BY_ALIAS);
				//Todo: 有相同别名存在时的错误提示
				if ($aliasMeta) {
					$back = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
					Url::redirect($back);
				}
			}

			$mid = MetaService::add($name, $alias, $description, MMeta::TYPE_CATEGORY);

			//未指定别名时，默认使用 ID 作为别名
			if ($alias == '') {
				$this->db->where(array('mid'=>$mid))->update('meta', array('alias'=>$mid));
			}

			//更新分类缓存
			$this->cache->delete('category');

			Url::redirect('admin/meta/category');
		}

		$this->render('admin/meta_category_add', array(
			'TITLE' => '新建分类',
			'isEdit' => false
		));
	}

	/**
	 * 修改分类
	 */
	public function edit() {
		$mid = $this->input->gp('mid', 'intval');
		$mid === null && exit;

		$meta = MetaService::getMeta($mid);

		if (!$meta) {
			showMessage('alert: 不存在的分类！');
		}

		if ($this->isSubmit()) {
			$name = $this->input->post('name', 'strip_tags');
			$alias = $this->input->post('alias', 'strip_tags|trim');
			$description = $this->input->post('description');

			$name == $meta['name'] && $name = null;
			$alias == $meta['alias'] && $alias = null;
			$description == $meta['description'] && $description = null;

			if ($alias === '') {
				$alias = $meta['mid'];
			}

			if (MetaService::update($mid, $name, $alias, $description)) {
				//更新分类缓存
				$this->cache->delete('category');
			}

			Url::redirect('admin/meta/category');
		}

		$this->render('admin/meta_category_add', array(
			'TITLE' => '修改分类',
			'isEdit' => true,
			'meta' => $meta
		));
	}

	public function delete() {

	}

	public function merge() {

	}

	public function tokenize()
	{
		$s = $this->input->get('search');
		$out = [];

		if ($s) {
			$list = MetaService::searchByPrefix($s, MetaService::TYPE_TAG);
			if ($list) {
				foreach ($list as $row) {
					$out[] = [
						'text' => $row['name'],
						'value' => $row['name']
					];
				}
			}
		}

		getApp()->output->json($out);
	}

}

