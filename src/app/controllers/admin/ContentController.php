<?php

namespace app\controllers\admin;

use app\libs\AdminController;
use app\libs\Pagination;
use app\models\Content;
use app\services\AdminContentService;
use app\services\AttachmentService;
use app\services\ContentService;
use app\services\MetaService;
use vgot\Web\Uploader;
use vgot\Web\Url;

/**
 * 后台内容管理
 *
 * @package Controller
 */
class ContentController extends AdminController {

	public $statusTextPost = array(
		0 => '草稿',
		1 => '公开',
		2 => '隐藏',
		3 => '私密'
	);

	public $statusTextPage = array(
		0 => '草稿',
		1 => '公开',
		2 => '隐藏'
	);

	public $super;

	public function __init() {
		parent::__init();
		$this->super = $_ENV['user']['group'] == 'admin';
	}

	/**
	 * 文章列表
	 */
	public function index() {
		$mid = $this->input->get('mid', null, 'numval');
		$uid = $this->input->get('uid', null, 'numval');

		//非管理员只能查看自己的文章
		if (!$this->super) {
			$uid = $_ENV['uid'];
		}

		//分页
		$page = $this->input->get('p', 'intval');
		$page < 1 && $page = 1;

		$qs = '?p=*';
		$mid && $qs .= "&mid=$mid";
		$uid && $qs .= "&uid=$uid";
		$pageConfig = array(
			'perPage' => 20,
			'curPage' => $page,
			'pageUrl' => Url::site('admin/content').$qs
		);

		$pagination = new Pagination($pageConfig);

		$start = $pagination->getStart();
		$data = AdminContentService::fetchList($mid, $uid, $start, $pageConfig['perPage']);

		$pages = '';

		if ($data['count']) {
			ContentService::listInjectCategory($data['list']);

			$pagination->initialize(array('totalRows'=>$data['count']));
			$pages = $pagination->makeLinks();
		}

		unset($this->statusTextPost[1]); //类型“公开”无需显示

		$this->render('admin/content_list_post', array(
			'TITLE' => '管理文章',
			'list' => $data['list'],
			'pages' => $pages,
			'statusText' => $this->statusTextPost
		));
	}

	/**
	 * 页面列表
	 */
	public function page() {
		$uid = $this->super ? null : $_ENV['uid'];

		//分页
		$page = $this->input->get('p', 'intval');
		$page < 1 && $page = 1;

		$pageConfig = array(
			'perPage' => 20,
			'curPage' => $page,
			'pageUrl' => Url::site('admin/content/page').'?p=*'
		);

		$uid && $pageConfig['pageUrl'] .= '&uid='.$uid;

		$pagination = new Pagination($pageConfig);

		$start = $pagination->getStart();
		$data = AdminContentService::fetchPageList($uid, $start, $pageConfig['perPage']);

		$pages = '';

		if ($data['count']) {
			$pagination->initialize(array('totalRows'=>$data['count']));
			$pages = $pagination->makeLinks();
		}

		unset($this->statusTextPage[1]);

		$this->render('admin/content_list_page', array(
			'TITLE' => '管理独立页面',
			'list' => $data['list'],
			'pages' => $pages
		));
	}

	/**
	 * 撰写文章
	 *
	 * @param string $type
	 */
	public function write($type='') {
		$cid = $this->input->get('cid', null, 'numval');

		if ($cid !== null) {
			$this->edit($cid);
			return;
		}

		$type != 'page' && $type = 'post';
		$demoUrl = $this->getDemoUrl($type);

		$categories = MetaService::getCategories();

		$statusText = $type == Content::TYPE_PAGE ? $this->statusTextPage : $this->statusTextPost;
		unset($statusText[0]); //不可在公开度中选择“草稿”类型，由保存草稿按钮决定

		$attachments = AttachmentService::getUnattachments($_ENV['uid']);

		$this->render('admin/content_write', array(
			'TITLE' => $type == Content::TYPE_PAGE ? '创建新页面' : '撰写新文章',
			'demoUrl' => $demoUrl,
			'type' => $type,
			'categories' => $categories,
			'isEdit' => false,
			'status' => 1,
			'statusText' => $statusText,
			'draft' => null,
			'attachments' => $attachments,
			'attachData' => $this->getAttachJson($attachments)
		));
	}

	/**
	 * 修改文章
	 *
	 * @param int $cid
	 */
	private function edit($cid) {
		$content = ContentService::getContent($cid);

		$this->checkEditable($content);

		$isDraftBase = $content['status'] == Content::STATUS_DRAFT;

		//获取草稿，并使用草稿的内容覆盖字段
		$draft = AdminContentService::getDraft($cid);
		if ($draft) {
			$cid = $draft['cid'];
			foreach (array('title', 'alias', 'created', 'content', 'status', 'password', 'allowComment') as $overwrite) {
				$content[$overwrite] = $draft[$overwrite];
			}
		}

		$demoUrl = $this->getDemoUrl($content['type'], $content['alias']);
		$categories = MetaService::getCategories();

		$cateId = 0;
		$tagsJoin = '';

		if ($content['type'] == Content::TYPE_POST) {
			$metas = MetaService::getCidMetas($cid);
			$tags = array();

			foreach ($metas as $row) {
				switch ($row['type']) {
					case MetaService::TYPE_CATEGORY: $cateId = $row['mid']; break;
					case MetaService::TYPE_TAG: $tags[] = $row['name']; break;
				}
			}

			$tagsJoin = join(', ', $tags);
		}

		$statusText = $content['type'] == Content::TYPE_PAGE ? $this->statusTextPage : $this->statusTextPost;
		unset($statusText[0]);

		$attachments = ContentService::getAttachments($content['cid']);

		$this->render('admin/content_write', array(
			'TITLE' => $content['type'] == Content::TYPE_PAGE ? '修改页面' : '修改文章',
			'demoUrl' => $demoUrl,
			'type' => $content['type'],
			'categories' => $categories,
			'content' => $content,
			'isEdit' => true,
			'cateId' => $cateId,
			'status' => $content['status'],
			'statusText' => $statusText,
			'tags' => $tagsJoin,
			//已发布的文章编辑草稿才显示草稿状态，未发布的草稿不显示
			'draft' => $isDraftBase ? null : $draft,
			'attachments' => $attachments,
			'attachData' => $this->getAttachJson($attachments)
		));
	}

	/**
	 * 保存文章
	 */
	public function save() {
		$cid = $this->input->post('cid');

		if ($cid !== null) {
			$this->saveEdit($cid);
			return;
		}

		$title = $this->input->post('title');
		$alias = $this->input->post('alias', 'trim');
		$content = $this->input->post('content', 'stripslashes');
		$mids = $this->input->post('mid'); //Category meta id
		$tags = $this->input->post('tags');
		$created = $this->input->post('created', 'trim');
		$type = $this->input->post('type');
		$action = $this->input->post('action');
		$allowComment = $this->input->post('allowComment') == 1 ? 1 : 0;
		$status = $this->input->post('status');
		$password = $this->input->post('password', '');

		$isPublish = $action == 'publish';
		$tmpAlias = '#tmp_'.uniqid();

		//检查别名是否已经存在，禁止使用系统占用的临时别名
		$alias = $alias ? $this->getUniqueAlias($alias) : $tmpAlias;

		//发布时间
		if ($created == '' || !$created = strtotime($created)) {
			$created = now();
		}

		trim($title) == '' && $title = '无题';

		$row = ['title'=>$title, 'alias'=>$alias, 'content'=>$content, 'uid'=>$_ENV['uid'], 'created'=>$created,
			'type'=>$type, 'status'=>$status, 'allowComment'=>$allowComment];

		$password != '' && $row['password'] = $password;
		$isPublish || $row['status'] = 0;

		$realCid = $cid = AdminContentService::add($row);

		$app = getApp();

		//更正临时别名
		if ($alias == $tmpAlias) {
			$alias = $this->getUniqueAlias($cid);
			Content::update(['alias'=>$alias], ['cid'=>$cid]);
		}

		//保存草稿
		$draftId = 0;
		if (!$isPublish) {
			$draft = $row;
			$draft['alias'] = $alias;
			$draft['status'] = $status;
			$cid = AdminContentService::saveDraft($cid, $draft);
		}

		//保存元数据关系
		if ($type == Content::TYPE_POST) {
			is_array($mids) || $mids = array();
			$this->saveTags($cid, $tags, $mids, $isPublish);
		}

		//更新附件关联
		$unattachIds = AttachmentService::getUnattachIds($_ENV['uid']);
		if ($unattachIds) {
			foreach ($unattachIds as $aid) {
				$app->db->where(array('aid'=>$aid, 'cid'=>0))->update('attachment', array('cid'=>$realCid));
			}
			AttachmentService::delUnattachIds($_ENV['uid']);
		}

		$redirect = siteUrl('admin/content/write').'?cid='.$realCid;
		header("Location: $redirect");
	}

	/**
	 * 保存修改
	 *
	 * @param int $cid
	 */
	private function saveEdit($cid) {
		$title = $this->input->post('title');
		$alias = $this->input->post('alias', 'trim');
		$content = $this->input->post('content', 'stripslashes');
		$mids = $this->input->post('mid'); //Category meta id
		$tags = $this->input->post('tags');
		$created = $this->input->post('created', 'trim');
		$action = $this->input->post('action');
		$allowComment = $this->input->post('allowComment') == 1 ? 1 : 0;
		$status = $this->input->post('status');
		$password = $this->input->post('password', '');

		$row = ContentService::getContent($cid);

		$this->checkEditable($row);
		$app = getApp();

		$isPublish = $action == 'publish';

		$alias == '' && $alias = $cid;
		if ($alias != $row['alias']) {
			$alias = $this->getUniqueAlias($alias);
		}

		//发布时间
		if ($created == '' || !$created = strtotime($created)) {
			$created = now();
		}

		trim($title) == '' && $title = '无题';

		$data = ['title'=>$title, 'alias'=>$alias, 'content'=>$content, 'created'=>$created, 'modified'=>now(),
			'status'=>$status, 'allowComment'=>$allowComment, 'password'=>$password];

		$realCid = $cid;

		//保存到草稿
		$draft = AdminContentService::getDraft($cid);

		if ($isPublish) {
			Content::update($data, ['cid'=>$cid]);
			$draft && AdminContentService::remove($draft['cid']);
		} else {
			if ($draft) {
				$draft = array_merge($draft, $data);
			} else {
				$draft = $data;
				$draft['uid'] = $_ENV['uid'];
			}

			$cid = AdminContentService::saveDraft($cid, $draft);
		}

		//保存元数据关系
		if ($row['type'] == Content::TYPE_POST) {
			is_array($mids) || $mids = array();

			//获取已经拥有的 MIDS
			$hasMids = array();
			$app->db->from('relationship')->select('mid')->where(array('cid'=>$cid));
			while ($meta = $app->db->fetch()) {
				$hasMids[] = $meta['mid'];
			}

			$this->saveTags($cid, $tags, $mids, $isPublish, $hasMids);
		}

		$redirect = siteUrl('admin/content/write').'?cid='.$realCid;
		header("Location: $redirect");
	}

	/**
	 * 上传附件
	 */
	public function upload() {
		$cid = $this->input->get('cid', null, 'numval');

		if ($cid && !$this->super) {
			$uid = $this->db->from('content')->select('uid')->where(array('cid'=>$cid))->scalar();
			if ($uid != $_ENV['uid']) {
				ret(false, '您无权上传到指定的文章！');
			}
		}

		$dir = vdate('Y/m');
		$fullDir = AttachmentService::DIR.'/'.$dir;
		$realDir = BASE_PATH.'/'.$fullDir;

		if (!is_dir($realDir)) {
			mkdir($realDir, 0777, true);
		}

		$uploader = new Uploader([
			'fieldName' => 'file',
			'saveDir' => $realDir,
			'allowExtensions' => str_replace('|', ',', opt('upload_allow_exts')),
			'filesizeLimit' => opt('upload_allow_maxsize').'K',
			'autoName' => true
		]);

		if ($uploader->save()) {
			$info = $uploader->getUploadedInfo();

			$aid = AttachmentService::add($cid, $_ENV['uid'], array(
				'filename' => $info['filename'],
				'filesize' => $info['size'],
				'path' => $dir.'/'.$info['savename'],
				'filetype' => $info['type']
			));

			$filePath = $fullDir.'/'.$info['savename'];
			ret(true, '', array('aid'=>$aid, 'path'=>$fullDir.'/'.$info['savename'], 'type'=>$info['extension'], 'url'=>baseUrl().$filePath));
		} else {
			$err = $uploader->getError();
			$err = iconv('gbk', 'utf-8', $err['message']);
			ret(false, $err);
		}
	}

	/**
	 * 快捷管理
	 */
	public function action() {
		$action = $this->input->post('action');
		$cids = $this->input->post('cids');

		if (!$cids) {
			showMessage('alert: 没有选中任何内容！');
		}

		$checkPermission = $_ENV['user']['group'] != 'admin';

		switch ($action) {
			case 'remove':
				!is_array($cids) && $cids = array($cids);
				foreach ($cids as $cid) {
					$row = Content::find()->select('uid,type,status')->where(array('cid'=>$cid, 'type !'=>'trash'))->get();

					if (!$row || ($checkPermission && $row['uid'] != $_ENV['uid'])) continue;

					//对于非草稿的内容应该真实删除，其 cid 已经发布过不应该再次使用
					$realDelete = ($row['type'] != 'draft' && $row['status'] != Content::STATUS_DRAFT);
					AdminContentService::remove($cid, $realDelete);
				}

				header("Location: {$_SERVER['HTTP_REFERER']}");
				break;
		}
	}

	/**
	 * 从字符串中取出标签数组
	 *
	 * @param string $tagstr
	 * @return array 返回小写化标准键名对应名称的数字
	 */
	private function splitTags($tagstr) {
		$tagstr = str_replace(array('，', '、'), ',', $tagstr); //将符号调整为标准的半角逗号
		$tagstr = trim(preg_replace('/\s*,\s*+/', ',', $tagstr), ','); //清理分割符左右空格
		$tagarr = explode(',', $tagstr); //转换为数组
		$tagarr = array_combine($tagarr, array_map('strtolower', $tagarr)); //使用 [名称=>小写名称] 创建数组
		$tagarr = array_unique($tagarr); //对小写名称排重
		return array_flip($tagarr); //使用小写名称作为键名
	}

	/**
	 * 保存元数据及关系
	 *
	 * @param int $cid
	 * @param string $tags 输入的标签
	 * @param array $mids 所选分类ID
	 * @param bool $isPublish 是否发布状态，草稿为非发布状态
	 * @param array $hasMids 目前文件已经关联的 mid
	 */
	private function saveTags($cid, $tags, $mids, $isPublish=true, $hasMids=array()) {
		//添加标签
		if ($tags != '' && $tagArr = $this->splitTags($tags)) {
			$aliases = array_keys($tagArr);

			if ($existTags = MetaService::getTagByAlias($aliases)) {
				$mids = array_merge($mids, array_column($existTags, 'mid')); //本次输入的数据库中已有元数据关系
				$existsTagAliases = array_column($existTags, 'alias'); //数据库中已经有的标签名称
				$aliases = array_diff($aliases, $existsTagAliases); //本次输入中新的标签索引
			}

			//加入新的标签 MID
			if ($aliases) {
				foreach ($aliases as $alias) {
					$mids[] = MetaService::add($tagArr[$alias], $alias, '', MetaService::TYPE_TAG);
				}
			}
		}

		//去掉已经移除的老 Meta 关系
		$midsUnlinked = array_diff($hasMids, $mids);

		if (count($midsUnlinked) > 0) {
			foreach ($midsUnlinked as $mid) {
				$this->db->where(array('cid'=>$cid, 'mid'=>$mid))->delete('relationship');
			}

			//如果之前内容不是草稿状态，则代表之前增加了计数，则扣除这部分老的计数
			$isPublish && MetaService::updateCount($midsUnlinked, -1);
		}

		//添加新关系数据
		$midsAdded = array_diff($mids, $hasMids);

		if (count($midsAdded) > 0) {
			foreach ($midsAdded as $mid) {
				$this->db->insert('relationship', array('mid' => $mid, 'cid' => $cid));
			}

			//非草稿增加 Meta 计数
			$isPublish && MetaService::updateCount($midsAdded, 1);
		}

		//更新分类缓存
		$isPublish && $this->cache->delete('categories');
	}

	/**
	 * 根据输入的别名获取一个唯一的别名
	 *
	 * 当别名存在时，会自动在别名后加上 -{数字} 直到找到不重复的别名为止
	 *
	 * @param string $alias
	 * @return string
	 */
	private function getUniqueAlias($alias) {
		$alias = str_replace(' ', '-', $alias);
		$aliasCopy = $alias;
		$i = 1;

		while (true) {
			if (AdminContentService::isAliasExists($aliasCopy)) {
				$aliasCopy = $alias.'-'.$i;
				++$i;
			} else {
				return $aliasCopy;
			}
		}
	}

	/**
	 * 获取输入别名的地址框
	 *
	 * @param string $contentType
	 * @param string $aliasValue
	 * @return string
	 */
	private function getDemoUrl($contentType, $aliasValue='') {
		$urlOptName = $contentType == Content::TYPE_PAGE ? 'url_page' : 'url_post';
		$url = substr($this->config->get($urlOptName, 'config'), 1);
		$demoUrl = preg_replace('/^https?\:\/\//', '', Url::site($url, true, true));

		if (preg_match('/\{alias\}/', $demoUrl)) {
			$html = <<<EOD
<div class="form-alias-wrap">
	<input type="text" name="alias" value="$aliasValue" class="form-alias" id="alias" />
	<div class="form-alias-copy">$aliasValue</div>
</div>
EOD;
			$demoUrl = preg_replace('/\{alias\}/', $html, $demoUrl);
		}

		return $demoUrl;
	}

	/**
	 * 检查指定的内容是否是存在和是否可编辑
	 *
	 * @param $row
	 */
	private function checkEditable($row) {
		if (!$row || ($row['type'] != 'post' && $row['type'] != 'page')) {
			showMessage('alert: 指定的内容不存在或无法编辑！');
		}

		if (!$this->super && $row['uid'] != $_ENV['uid']) {
			showMessage('alert: 您无权编辑该文章！');
		}
	}

	/**
	 * 从附件数据中获取供前端使用的数据格式
	 *
	 * @param array $attachments
	 * @return array
	 */
	private function getAttachJson($attachments) {
		$attachArr = array();
		foreach ($attachments as $row) {
			$attachArr[$row['aid']] = array(
				'aid' => $row['aid'],
				'filename' => $row['filename'],
				'url' => baseUrl().AttachmentService::DIR.'/'.$row['path'],
				'type' => $row['filetype']
			);
		}

		return jsonEncode($attachArr);
	}

}
