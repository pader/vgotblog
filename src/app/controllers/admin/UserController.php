<?php

namespace app\controllers\admin;

use app\libs\AdminController;
use app\libs\Pagination;
use app\services\UserManagerService;
use app\services\UserService;

/**
 * Class User
 * @package Controller
 */
class UserController extends AdminController {

	public function index() {
		//分页
		$page = $this->input->get('p', 'intval');
		$page < 1 && $page = 1;

		$pageConfig = array(
			'perPage' => 20,
			'curPage' => $page,
			'pageUrl' => siteUrl('admin/user').'?p=*'
		);

		$pagination = new Pagination($pageConfig);

		$start = $pagination->getStart();
		$data = UserManagerService::fetchList($start, $pageConfig['perPage']);

		if ($data['count']) {
			$pagination->initialize(array('totalRows'=>$data['count']));
			$pages = $pagination->makeLinks();
		}

		$this->render('admin/user_list', array(
			'list' => $data['list'],
			'pages' => $pages,
			'page' => $page
		));
	}

	/**
	 * 编辑用户资料
	 */
	public function profile() {
		$uid = $this->input->get('uid', 'numval');
		$page = $this->input->get('p', 'intval');

		if (!$uid) {
			$this->render('admin/user_profile', array('isEdit'=>false, 'page'=>1, 'TITLE'=>'新增用户'));
			return;
		}

		$user = UserService::getUser($uid);

		$this->render('admin/user_profile', array(
			'isEdit' => true,
			'user' => $user,
			'page' => $page,
			'TITLE' => '修改用户信息'
		));
	}

	public function myaccount() {
		
	}

}
