<?php

namespace app\controllers\admin;

use app\libs\AdminController;
use app\libs\Pagination;
use app\models\Comment;
use app\models\Content;
use app\models\User;
use app\services\CommentService;
use app\services\ContentService;
use vgot\Web\Url;

class CommentController extends AdminController
{

	public function index()
	{
		$cid = $this->input->get('cid');
		$status = $this->input->get('status', Comment::STATUS_PUBLIC);
		$page = $this->input->get('page', 'intval');

		$where = [1];

		if ($cid !== null) {
			$where['c.cid'] = $cid;
		}

		if ($status != -1) {
			$where['c.status'] = $status;
		}

		$perPage = 20;

		//统计与分页
		$count = Comment::find()->alias('c')->where($where)->count();
		$pageUrl = Url::site(['admin/comment', 'status'=>$status, 'cid'=>$cid, 'page'=>'_']);
		$pager = new Pagination(['curPage'=>$page, 'pageUrl' => $pageUrl, 'perPage'=>$perPage, 'pageNumReplace'=>'_']);
		$pager->initialize(['totalRows' => $count]);

		//查询列表
		$comments = Comment::find()->alias('c')
			->select('c.*, p.alias, p.title, p.type, u.nickname, u.mail as userMail, u.url AS userSite')
			->leftJoin(Content::tableName().' p', 'cid')
			->leftJoin(User::tableName().' u', 'u.uid=c.uid')
			->where($where)->orderBy(['c.cmtid'=>SORT_DESC])
			->limit($perPage, $pager->getStart())
			->fetchAll();

		array_walk($comments, function(&$row) {
			if ($row['uid']) {
				$row['author'] = $row['nickname'];
				$row['mail'] = $row['userMail'];
			} else {
				$row['userSite'] = $row['url'];
			}
			unset($row['nickname'], $row['userMail'], $row['userUrl']);
		});

		if ($comments) {
			ContentService::listInjectCategory($comments);
		}

		//统计各状态数量
		unset($where['c.status']);

		$countQuery = Comment::find()->alias('c')->select('COUNT(*) AS count, status')->groupBy('status');

		if ($where) {
			$countQuery->where($where);
		}

		$statusCount = $countQuery->fetchAll();
		$statusCount = array_column($statusCount, 'count', 'status');

		$pages = $pager->makeLinks();

		$TITLE = '评论管理';
		$this->render('admin/comment_index', compact('count', 'comments', 'pages', 'cid', 'status',
			'statusCount', 'TITLE'));
	}

	public function action()
	{
		$action = $this->input->post('action');
		$cmtids = $this->input->post('cmtids');
		$status = $this->input->post('status');

		if (empty($cmtids)) {
			showMessage('没有选中任何评论！');
		}

		$cids = Comment::find()->select('cid')->where(['cmtid'=>$cmtids])->fetchColumn();

		switch ($action) {
			case 'move':
				Comment::update(['status'=>$status], ['cmtid'=>$cmtids, 'status !'=>$status]);
				CommentService::recountComment($cids);
				showMessage('success: 移动评论成功！');
				break;
			case 'delete':
				Comment::delete(['parent_id'=>$cmtids]);
				Comment::delete(['cmtid'=>$cmtids]);
				CommentService::recountComment($cids);
				showMessage('success: 已彻底删除指定评论！');
				break;
		}
	}

}