<?php

namespace app\controllers\admin;

use app\libs\AdminController;
use app\models\Option;
use app\services\ContentService;
use app\services\MetaService;
use app\services\OptionService;
use vgot\Web\Url;

/**
 * Class Option
 *
 * @package Controller
 */
class OptionController extends AdminController {

	public $option;

	public function __init() {
		parent::__init();
		$this->option = OptionService::getOptions();
		$this->view->vars('option', $this->option);
	}

	public function general() {
		$this->render('admin/option_general', [
			'TITLE' => '基本设置',
			'core' => Option::fetchByCategory('core')
		]);
	}

	public function security() {
		$this->render('admin/option_security', array(
			'TITLE' => '安全设置'
		));
	}

	public function url() {
		$preset = array(
			'默认风格' => '/archives/{cid}/',
			'别名风格' => '/archives/{alias}.html',
			'按日期归档' => '/archives/{year}/{month}/{day}/{alias}.html',
			'按分类归档' => '/archives/{category}/{alias}.html'
		);

		$this->render('admin/option_url', array(
			'TITLE' => '链接地址',
			'preset' => $preset,
			'core' => Option::fetchByCategory('core')
		));
	}

	public function save() {
		//保存设置
		if ($this->isSubmit()) {
			$core = $this->input->post('core');
			$coreConfigs = Option::fetchByCategory('core');

			if (is_array($core)) {
				$updated = false;

				foreach ($core as $k => $v) {
					if (isset($coreConfigs[$k]) && $v !== $coreConfigs[$k]) {
						Option::set($k, $v, 'core', false);
						$updated = true;
					}
				}

				$updated && OptionService::updateCoreConfig();
			}

			$setting = $this->input->post('setting');

			foreach (['protect_attachment_url', 'comment_email_require'] as $boolKey) {
				$setting[$boolKey] = !empty($setting[$boolKey]);
			}

			if (is_array($setting)) {
				$updated = false;

				foreach ($setting as $k => $v) {
					if (isset($this->option[$k]) && $this->option[$k] !== $this->input->post($k)) {
						Option::set($k, $v, 'setting', false);
						$updated = true;
					}
				}

				$updated && $this->cache->delete('option');
			}
		}

		showMessage('success: 设置已保存。', ':back', 1);
	}

	/**
	 * 导航设置
	 */
	public function nav() {
		$navs = OptionService::getNavs();

		//显示真实 URL
		foreach ($navs as &$row) {
			switch ($row['type']) {
				case 'page':
					$page = ContentService::getContent($row['url']);
					if (!$page) continue 2;
					$row['url'] = url($page, 'page');
					break;
				case 'category':
					$category = MetaService::getMeta($row['url']);
					if (!$category) continue 2;
					$row['url'] = url($category, 'category');
					break;
			}
		}

		//获取分类

		//获取页面
		$pages = ContentService::fetchPageList();

		$this->render('admin/option_nav', array(
			'TITLE' => '导航设置',
			'navs' => $navs,
			'pages' => $pages
		));
	}

	/**
	 * 修改导航
	 *
	 * @param int $id
	 */
	public function editNav($id=0) {
		$nav = OptionService::getNav($id);

		if (!$nav) {
			showMessage('error: 指定的导航不存在！');
		}

		if ($this->isSubmit()) {
			$name = $this->input->post('name');
			$url = $this->input->post('url');
			$newpage = $this->input->post('newpage') ? 1 : 0;

			$data = array('name'=>$name, 'newpage'=>$newpage);

			if (!in_array($nav['type'], array('page', 'category', 'system'))) {
				$data['url'] = $url;
			}

			OptionService::updateNav($id, $data);
			$this->cache->delete('option');

			showMessage('success: 成功修改指定导航。', 'admin/option/nav', 1);
		}

		$disabled = false;
		$sourceType = $sourceName = '';

		switch ($nav['type']) {
			case 'page':
				$page = ContentService::getContent($nav['url']);
				$nav['url'] = $page ? url($page, 'page') : '';
				$disabled = true;
				$sourceType = '页面';
				$sourceName = $page['title'];
				break;

			case 'category':
				$category = MetaService::getMeta($nav['url']);
				$nav['url'] = $category ? url($category, 'category') : '';
				$disabled = true;
				$sourceType = '分类';
				$sourceName = $category['name'];
				break;

			case 'system':
				$disabled = true;
				$sourceType = '系统';
				break;
		}

		$this->render('admin/option_nav_edit', array(
			'TITLE' => '修改导航',
			'nav' => $nav,
			'disabled' => $disabled,
			'sourceType' => $sourceType,
			'sourceName' => $sourceName
		));
	}

	/**
	 * 导航管理操作
	 */
	public function navdo() {
		$action = $this->input->gp('action');

		if ($action == 'add') {
			$type = $this->input->post('type');
			switch ($type) {
				case 'custom':
					$name = $this->input->post('name');
					$url = $this->input->post('url');
					$sort = $this->input->post('sort');
					$newWindow = $this->input->post('newwindow') == 1 ? 1 : 0;

					OptionService::addNav($name, 'out', $url, $newWindow, $sort);
					break;

				case 'category':
					$mids = $this->input->post('mids');

					if (!is_array($mids) || count($mids) == 0) {
						showMessage('alert: 请至少选择一个分类。');
					}

					$categories = MetaService::getCategories();
					$mids = array_intersect_key(array_flip($mids), $categories);

					OptionService::$navAutoSave = false;
					foreach (array_flip($mids) as $mid) {
						OptionService::addNav($categories[$mid]['name'], $type, $mid);
					}
					OptionService::navSave();
					break;
				case 'page':
					$cids = $this->input->post('cids');

					if (!is_array($cids) || count($cids) == 0) {
						showMessage('alert: 请至少选择一个页面。');
					}

					$pages = ContentService::fetchPageList();
					$pages = array_column($pages, 'title', 'cid');

					OptionService::$navAutoSave = false;
					foreach ($cids as $cid) {
						if (!isset($pages[$cid])) continue;
						OptionService::addNav($pages[$cid], $type, $cid);
					}
					OptionService::navSave();
					break;
			}

			//删除缓存
			$this->cache->delete('option');

			showMessage('success: 添加导航成功。', 'admin/option/nav', 1);

		} elseif ($action == 'remove') {
			$ids = $this->input->post('ids');

			if (!is_array($ids) || count($ids) == 0) {
				showMessage('alert: 请至少选择一个导航。');
			}

			OptionService::$navAutoSave = false;

			foreach ($ids as $id) {
				$nav = OptionService::getNav($id);

				if (!$nav) continue;
				$nav['type'] == 'system' && showMessage('error: 无法删除系统导航!');

				OptionService::removeNav($id);
			}

			OptionService::navSave();

		} elseif ($action == 'sort') {
			$sort = $this->input->post('sort');

			if (!is_array($sort) || count($sort) == 0) {
				showMessage('alert: 没有输入任何设置！');
			}

			OptionService::$navAutoSave = false;

			foreach ($sort as $id => $value) {
				$nav = OptionService::getNav($id);
				if (!$nav) continue;
				OptionService::updateNav($id, array('sort'=>$value));
			}

			OptionService::navSave();

		} elseif ($action == 'visible') {
			$id = $this->input->get('id');
			$visible = $this->input->get('visible', 'intval');

			if (!$id) exit;

			$nav = OptionService::getNav($id);

			if (!$nav) {
				showMessage('error: 指定的导航不存！');
			}

			OptionService::updateNav($id, array('visible'=>$visible));

		} else {
			showMessage('error: 未知动作!');
		}

		$this->cache->delete('option');
		Url::redirect('admin/option/nav');
	}

	public function performance()
	{
		$data = Option::fetchByCategory('core');

		$this->view->vars('TITLE', '性能设置');
		$this->render('admin/option_performance', compact('data'));
	}


}
