<?php

namespace Controller;

class Sax extends \Controller {

	public function __construct() {
		parent::__construct();
		$this->db->close();
		$this->load->database('backup');
	}

	public function index() {
		$r = $this->db->get('sax_articles', '*', null, array('limit'=>2))->result();
		printr($r);
	}

	public function attachment() {
		$dir = './uploads/sax';

		$olds = $this->db->get('sax_attachments', 'attachmentid AS aid, articleid AS cid, filename, filesize, filepath AS path, dateline AS uploaded')->result();

		//INSERT INTO vblog_attachment SELECT attachmentid AS aid,articleid AS cid,filename,filesize,SUBSTRING(filepath, 2) AS path,SUBSTRING(filename, LOCATE('.',filename)) FROM `sax_relationships`
		foreach ($olds as $row) {
			$row['type'] = substr($row['filename'], strrpos($row['filename'], '.') + 1);

			$relativeDir = pathinfo($row['path'], PATHINFO_DIRNAME);
			$pi = pathinfo($dir.$row['path']);

			if ($pi['extension'] == 'file') {
				$name = $pi['filename'].'.'.$row['type'];
				$rename = $pi['dirname'].'/'.$name;
				$path = 'sax'.$relativeDir.'/'.$name;
				rename($pi['dirname'].'/'.$pi['basename'], $rename);
				//printr($pi['dirname'].'/'.$pi['basename'], $rename, 'sax'.$relativeDir.'/'.$name,'=================');
				$row['path'] = $path;
			} else {
				$row['path'] = 'sax'.$row['path'];
			}

			$this->db->insert('vblog_attachment', $row);
		}

		echo 'done';
	}

}
