<?php

namespace app\controllers;

use app\services\EncryptService;
use app\services\UserService;
use vgot\Web\Url;

class LoginController extends \app\libs\BlogController {

	public function index() {
		view('login', [
			'TITLE' => '登录'
		]);
	}

	/**
	 * 用户登录
	 */
	public function postlogin() {
		//登录保护
		$failedKey = 'login_failed_'.$this->input->clientIp();
		$failedCount = $this->cache->get($failedKey, 0);

		if ($failedCount >= 5) {
			ret(false, '您登录失败的次数过多，请稍候再试！');
		}

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$keepLogin = $this->input->post('keeplogin');

		$fetchType = filter_var($username, FILTER_VALIDATE_EMAIL) ? UserService::MAIL : UserService::USERNAME;
		$user = UserService::getUser($username, $fetchType);

		if (!$user) {
			ret(false, '用户不存在！');
		}

		if ($user['status'] == 'disabled') {
			ret(false, '对不起，该用户已被禁用！');
		}

		if (EncryptService::passwordVerify($password, $user['password'])) {
			$authCode = $user['uid']."\t".substr($user['password'], -8)."\t".time();
			$authCode = $this->security->encrypt($authCode, true);

			//保持登录为 15 天有效期
			$expire = $keepLogin ? 1296000 : null;
			vsetcookie('auth', $authCode, $expire);

			$redirectTo = Url::base();

			ret(true, '', array('redirectTo'=>$redirectTo));
		} else {
			$failedCount++;
			$this->cache->set($failedKey, $failedCount, 900);
			ret(false, '密码错误！');
		}
	}

	public function logout() {
		vsetcookie('auth', false, -86400);

		$goto = (isset($_SERVER['HTTP_REFERER']) && preg_match('/\/admin/', $_SERVER['HTTP_REFERER'])) ? Url::base() : ':back';

		showMessage('info: 已成功退出登录', $goto);
	}

}
