<?php
/**
 * Vgot Blog Index
 */
namespace app\controllers;

use app\libs\BlogController;
use app\libs\Pagination;
use app\services\ContentService;
use app\services\MetaService;
use app\services\OptionService;
use app\services\UserService;
use vgot\Exceptions\HttpNotFoundException;
use vgot\Web\Url;

/**
 * 首页及列表页控制器
 *
 * @package Controller
 */
class IndexController extends BlogController {

	private $listPageNum;

	public function __init() {
		loadHelper('blog');
		$this->listPageNum = opt('blog_list_num', 5);
	}

	/**
	 * 博客首页
	 */
	public function index() {
		$page = $this->input->get('page', 'intval');
		$pagination = new Pagination([
			'pageUrl' => Url::site('').'?page=*',
			'perPage' => $this->listPageNum,
			'curPage' => $page
		]);

		$start = $pagination->getStart();
		$data = ContentService::fetchList(null, null, $_ENV['uid'], $start, $this->listPageNum);

		if ($data['list']) {
			ContentService::listInjectCategory($data['list']);
		}

		$pages = '';

		if ($data['count'] > 0) {
			$pagination->initialize(array(
				'totalRows' => $data['count']
			));

			$pages = $pagination->makeLinks();
		}

		view('list', array('list'=>$data['list'], 'FLAG'=>'SYSTEM_1', 'pages'=>$pages));
	}

	/**
	 * 分类列表
	 */
	public function category() {
		//匹配地址参数
		$cond = array();

		foreach (array('mid', 'alias') as $p) {
			if (isset($_ENV['route_params'][$p])) {
				$cond[$p] = $_ENV['route_params'][$p];
			}
		}

		if (count($cond) == 0) {
			throw new HttpNotFoundException();
		}

		//找到分类
		$key = isset($cond['mid']) ? 'mid' : 'alias';

		$categories = MetaService::getCategories();
		$meta = null;

		foreach ($categories as $row) {
			if ($row[$key] == $cond[$key]) {
				$meta = $row;
				break;
			}
		}

		if ($meta === null) {
			throw new HttpNotFoundException();
		}

		//分页
		$pagination = new Pagination([
			'pageUrl' => url($meta, 'category').'?page=*',
			'perPage' => $this->listPageNum,
			'curPage' => $this->input->get('page', 'intval')
		]);

		//获取列表数据
		$start = $pagination->getStart();
		$data = ContentService::fetchList($meta['mid'], null, $_ENV['uid'], $start, $this->listPageNum);

		if ($data['list']) {
			ContentService::listInjectCategory($data['list']);
		}

		$pages = '';

		if ($data['count'] > 0) {
			$pagination->initialize(array('totalRows'=>$data['count']));
			$pages = $pagination->makeLinks();
		}

		view('list', array(
			'TITLE' => $meta['name'],
			'FLAG' => 'CATEGORY_'.$meta['mid'],
			'listTitle' => '分类 <b>'.$meta['name'].'</b> 中的文章',
			'list' => $data['list'],
			'pages' => $pages
		));
	}

	/**
	 * 查看作者文章
	 *
	 * @param int $uid
	 * @throws HttpNotFoundException
	 */
	public function author($uid=0) {
		$uid = intval($uid);
		if ($uid <= 0) {
			throw new HttpNotFoundException();
		}

		$user = UserService::getUser($uid);

		if (!$user) {
			showMessage('alert: 该用户不存在或已被删除！', ':close');
		}

		if ($user['status'] != 'normal') {
			showMessage('alert: 该用户内容无法访问！');
		}

		//获取列表数据
		$data = ContentService::fetchList(null, $uid, $_ENV['uid']);

		if ($data['list']) {
			ContentService::listInjectCategory($data['list']);
		}

		view('list', array(
			'TITLE' => $user['nickname'].' 的文章',
			'listTitle' => "由 <b>{$user['nickname']}</b> 撰写的文章",
			'list' => $data['list'],
			'pages' => ''
		));
	}

	/**
	 * 查看标签文章
	 *
	 * @param string $tagName
	 * @throws HttpNotFoundException
	 */
	public function tag($tagName='') {
		if (!$tagName) {
			throw new HttpNotFoundException();
		}

		$tag = MetaService::getMeta($tagName, MetaService::TYPE_TAG, MetaService::BY_ALIAS);

		if ($tag === null) {
			throw new HttpNotFoundException();
		}

		//获取列表数据
		$data = ContentService::fetchList($tag['mid'], null, $_ENV['uid']);

		if ($data['list']) {
			ContentService::listInjectCategory($data['list']);
		}

		view('list', array(
			'TITLE' => $tag['name'].'标签',
			'listTitle' => "包含 <b>{$tag['name']}</b> 标签的文章",
			'list' => $data['list'],
			'pages' => ''
		));
	}

	public function test()
	{
		OptionService::updateCoreConfig();
	}

}
