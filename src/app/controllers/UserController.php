<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2020/2/1
 * Time: 14:42
 */

namespace app\controllers;

use app\services\EncryptService;
use app\services\UserService;

class UserController extends \app\libs\BlogController
{

	protected $loginRequired = true;

	public function index()
	{
		view('user-index', [
			'TITLE' => '个人中心',
			'user' => $_ENV['user']
		]);
	}

	/**
	 * 修改密码
	 *
	 * @param string $do
	 */
	public function passwd($do='')
	{
		if ($do == 'post') {
			$input = $this->input;
			$password = $input->post('password');
			$newPassword = $input->post('newPassword');

			if (strlen($newPassword) < 6) {
				ret(false, '密码长度不能小于 6 个字符！', 'len_error');
			}

			$user = UserService::getUser($_ENV['uid']);

			if (EncryptService::passwordVerify($password, $user['password'])) {
				UserService::updatePassword($_ENV['uid'], $newPassword);
				ret(true, '恭喜你，密码修改成功！请使用新密码重新登录。');
			} else {
				ret(false, '原密码输入不正确，请重新输入！', 'old_password_error');
			}

		} else {
			view('user-passwd', [
				'TITLE' => '修改密码'
			]);
		}
	}

}