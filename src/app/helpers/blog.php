<?php

function blogDesc($content) {
	$breakPoint = strpos($content, '__page_break_tag__');

	if ($breakPoint) {
		return substr($content, 0, $breakPoint);
	}

	return $content;
}

function blogCheckPassword($row) {
	if ($row['password'] === '') return true;

	//自己的文章则不需要密码
	if ($_ENV['uid'] > 0 && $_ENV['uid'] == $row['uid']) {
		return true;
	}

	$ck = opt('cookie_prefix').'arcpw_'.$row['cid'];
	$pw = isset($_COOKIE[$ck]) ? base64_decode($_COOKIE[$ck]) : '';

	return $pw === $row['password'];
}
