<?php

namespace app\helpers;

/**
 * 协助构建表单的辅助类
 * @package app\helpers
 */
class FormUtil
{

	public static function input($type, $name, $value='', $attrs=[]) {
		$attrs = array_merge($attrs, compact('type', 'name', 'value'));
		return self::tag('input', $attrs, '', true);
	}

	public static function radio($name, $options, $value='', $attrs=[]) {
		$radios = '';
		foreach ($options as $v => $title) {
			$rattrs = $value == $v ? array_merge($attrs, ['checked']) : $attrs;
			$radios.= self::tag('label', [], self::input('radio', $name, $v, $rattrs).' '.$title);
		}
		return $radios;
	}

	public static function select($name, $options, $value='', $attrs=[]) {
		$opts = '';
		foreach ($options as $v => $title) {
			$oattrs = ['value'=>$v];
			if ($v == $value) {
				$oattrs[] = 'selected';
			}
			$opts .= self::tag('option', $oattrs, $title);
		}
		$attrs['name'] = $name;
		return self::tag('select', $attrs, $opts);
	}

	public static function checkbox() {}

	public static function textarea($name, $content, $attrs=[]) {
		$attrs['name'] = $name;
		return self::tag('textarea', $attrs, htmlspecialchars($content));
	}

	protected static function tag($tagName, $attrs=[], $content='', $single=false) {
		$html = "<$tagName";

		if ($attrs) {
			foreach ($attrs as $k => $v) {
				if (is_numeric($k)) {
					$html .= " $v";
				} else {
					$html .= " $k=\"" . addslashes($v) . '"';
				}
			}
		}

		if ($single) {
			$html .= ' />';
		} else {
			$html .= ">$content</$tagName>";
		}

		return $html;
	}

}