<?php

use vgot\Exceptions\ApplicationException;
use vgot\Web\Url;

/**
 * 简单的变量调试
 *
 * @param .. mixed $var1,$var2,$var3....
 * @return void
 */
function printr() {
	$args = func_get_args();

	if (is_array($args) and !empty($args)) {
		foreach ($args as $var) {
			echo '<pre>';
			if (is_bool($var)) {
				echo $var ? '<i>TRUE</i>' : '<i>FALSE</i>';
			} elseif (is_null($var)) {
				echo '<i>NULL</i>';
			} else {
				(is_array($var) or is_object($var)) ? print_r($var) : print($var);
			}
			echo "</pre>\r\n";
		}
	}
}

function loadHelper($name) {
	$file = BASE_PATH.'/app/helpers/'.$name.'.php';

	if (!is_file($file)) {
		throw new ApplicationException('helper '.$name.' not exists!');
	}

	require_once $file;
}

/**
 * 获取后台配置
 *
 * @param string $key
 * @param mixed $defaultValue
 * @return mixed
 */
function opt($key, $defaultValue=null) {
	$value = getApp()->config->get($key);
	return $value !== null ? $value : $defaultValue;
}

/**
 * 加载主题模板
 *
 * @param string $view
 * @param array $vars
 * @param bool $return
 * @return string
 */
function view($view, $vars=null, $return=false) {
	if (THEME != 'default') {
		$path = VIEW_PATH.'/theme/'.THEME.'/'.$view.'.php';
		$theme = (THEME != 'default' && is_file($path)) ? THEME : 'default';
	} else {
		$theme = THEME;
	}

	return getApp()->view->render('theme/'.$theme.'/'.$view, $vars, $return);
}

function jsonEncode($var) {
	if (PHP_VERSION_ID > 50400) {
		return json_encode($var, JSON_UNESCAPED_UNICODE);
	}

	switch (gettype($var)) {
		case 'boolean': return $var ? 'true' : 'false';
		case 'NULL': return 'null';
		case 'integer': return (int) $var;
		case 'double':
		case 'float': return (float) $var;
		case 'string':
			$var = str_replace(array('\\', '"', "\r\n", "\r", "\n", "\t"), array('\\\\', '\\"', '\n', '', '\n', '\t'), $var);
			return '"' . $var . '"';
		case 'array':
			if (count($var) && (array_keys($var) !== range(0, sizeof($var) - 1))) {
				$properties = array();
				foreach ($var as $name => $value) {
					$properties[] = jsonEncode(strval($name)) . ':' . jsonEncode($value);
				}
				return '{' . join(',', $properties) . '}';
			}
			$elements = array_map('jsonEncode', $var);
			return '[' . join(',', $elements) . ']';
	}

	return false;
}

/**
 * Javascript|Ajax|Json 交互响应JSON输出
 *
 * @param bool $status
 * @param string $message
 * @param mixed $data
 */
function ret($status, $message='', $data=null) {
	$output = array('status'=>$status);

	if ($message != '') {
		$output['message'] = $message;
	}

	if ($data !== null) {
		$output['data'] = $data;
	}

	getApp()->output->json($output);
	exit;
}

/**
 * @param string $message 可选前缀 success, info, error, alert
 * @param string $goto 可用 URL，或 :back, :close
 * @param int $seconds
 */
function showMessage($message, $goto = ':back', $seconds = 2) {
	if (!preg_match('/^https?:\/\//', $goto)) {
		switch (substr($goto, 0, 1)) {
			case ':':
				if ($goto == ':back') {
					if (isset($_SERVER['HTTP_REFERER']) and strpos($_SERVER['HTTP_REFERER'], Url::base()) !== FALSE) {
						$goto = $_SERVER['HTTP_REFERER'];
					} else {
						$goto = Url::base();
					}
				}
				break;
			case '/': break;
			default:
				$querySplit = strpos($goto, '?');
				if ($querySplit !== false) {
					$goto = Url::site(substr($goto, 0, $querySplit)).substr($goto, $querySplit);
				} else {
					$goto = Url::site($goto);
				}
		}
	}

	$status = '';
	$a = strpos($message, ':');

	if ($a !== FALSE) {
		$status = ltrim(substr($message, 0, $a));
		if (in_array($status, array('success', 'info', 'error', 'alert'))) {
			$message = substr($message, $a + 1);
		}
	}

	$app = getApp();
	$app->view->vars(array(
		'title' => '提示',
		'message' => $message,
		'goto' => $goto,
		'seconds' => $seconds,
		'status' => $status
	));

	if ($app->controller instanceof \app\libs\AdminController) {
		$buffer = $app->view->render('admin/message');
	} else {
		$buffer = view('message');
	}

	exit($buffer);
}

/**
 * 获取当前时间戳
 *
 * 为不同时区预留，未来用予支持后台设置时区后的时间戳获取
 *
 * @return int
 */
function now() {
	return time();
}

/**
 * 输出日期格式
 *
 * 为不同时区预留，如 now() 函数
 *
 * @param string $format
 * @param int $timestamp
 * @return bool|string
 */
function vdate($format, $timestamp=null) {
	$timestamp === null && $timestamp = now();
	return date($format, $timestamp);
}

/**
 * @param array $row
 * @param string $type
 * @param boolean $absolute
 * @return string
 */
function url($row, $type='post', $absolute=false) {
	static $replaceRules = array(
		'post' => array('cid', 'alias', 'category', 'year', 'month', 'day'),
		'category' => array('mid', 'alias'),
		'page' => array('cid', 'alias')
	);

	//获取链接，并去掉第一个斜杠
	$url = substr(getApp()->config->get('url_'.$type, 'config'), 1);

	//部分特殊转义处理
	switch ($type) {
		case 'post':
			$date = vdate('Y,m,d', $row['created']);
			list($row['year'], $row['month'], $row['day']) = explode(',', $date);
			$row['category'] = isset($row['malias']) ? $row['malias'] : '0';
			break;
		case 'category':
			if (isset($row['malias'])) {
				$row['alias'] = $row['malias'];
			}
			break;
	}

	$find = $replace = array();

	foreach ($replaceRules[$type] as $field) {
		$find[] = '{'.$field.'}';
		$replace[] = $row[$field];
	}

	$url = str_replace($find, $replace, $url);

	return Url::site($url, $absolute);
}

/**
 * 将时间戳转换为更人性化的时间展示
 *
 * @param int
 * @return string
 */
function fmtime($timestamp) {
	$now = now();
	$tl = $now - $timestamp;
	$str = '';

	if ($tl < 60) {
		$str = '刚刚';
	} elseif ($tl < 1800) {
		$str = ceil($tl / 60).'分钟前';
	} elseif ($tl < 2700) {
		$str = '半小时前';
	} elseif ($tl < 3600) {
		$str = ceil($tl / 60).'分钟前';
	} else {
		$today = strtotime('Today', $now);
		if ($timestamp > $today) {
			$str = '今天 '.vdate('H:i', $timestamp);
		} else {
			$yesterday = strtotime('Yesterday', $now);
			if ($timestamp > $yesterday) {
				$str = '昨天 '.vdate('H:i', $timestamp);
			} else {
				$thisYear = mktime(0, 0, 0, 1, 1);
				if ($timestamp > $thisYear) {
					$str = vdate('n月j日 H:i', $timestamp);
				} else {
					$str = vdate('Y-n-j', $timestamp);
				}
			}
		}
	}

	return $str;
}

/**
 * 强制转换为数字
 *
 * 相对于 intval() 可以容纳更大的数字
 *
 * @param mixed $var
 * @return float
 */
function numval($var) {
	if (ctype_digit($var)) {
		return $var;
	}

	return floor(floatval($var));
}

function vgetcookie($name) {
	return getApp()->input->cookie(opt('cookie_prefix').$name);
}

function vsetcookie($name, $value=null, $expire=null) {
	if ($expire && $expire > 0) {
		$expire = now() + $expire;
	}
	return setcookie(opt('cookie_prefix').$name, $value, $expire, Url::base());
}

function siteUrl($uri, $absolute=false) {
	return Url::site($uri, $absolute);
}

function baseUrl($absolute=false) {
	return Url::base($absolute);
}

function csrfToken($key='.') {
	$str = $_ENV['uid']."\t".$key."\t".now();
	return getApp()->security->encrypt($str, true);
}

function csrfVerify($key='.') {
	$token = getApp()->input->post('_csrfToken');
	if ($token) {
		$str = getApp()->security->decrypt($token, true);
		if ($str) {
			list($uid, $skey, $time) = explode("\t", $str);
			if ($uid == $_ENV['uid'] && $skey == $key && now() - $time < 86400) {
				return true;
			}
		}
	}
	return false;
}

/**
 * 获取系统头像
 *
 * @param array $info 包含 uid,email,nickname 的数组
 * @param string $size
 * @return string 头像地址
 */
function getAvatar($info, $size='m') {
	if (is_array($info)) {
		$info = array_intersect_key($info, ['uid'=>0, 'mail'=>0, 'nickname'=>0]);
	}

	$url = getApp()->hook->pre('avatar', $info, $size);

	if ($url) {
		return $url;
	}

	return STATIC_URL.'image/avatar.png';
}

/**
 * 连接指定表进行查询
 *
 * @param string $table 表名
 * @return \vgot\Database\QueryBuilder 已选择表的连接实例
 */
function from($table) {
	return getApp()->db->from($table);
}
