<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2018/5/6
 * Time: 14:12
 */

namespace app\libs;

use app\services\OptionService;
use app\services\UserService;
use vgot\Web\Url;

abstract class BlogController extends \vgot\Core\Controller
{

	protected $loginRequired = false;

	public function __construct() {
		parent::__construct();

		if (!is_file(RES_PATH.'/config.php')) {
			header('Location: '.Url::base().'install/');
			exit;
		}

		define('STATIC_URL', Url::base().'static/');
		define('THEME', 'digitalocean');

		require BASE_PATH.'/app/helpers/common.php';

		$this->initCore();
		$this->initUser();

		$_ENV['version'] = $this->config->get('version');

		header('Content-Type: text/html; charset=utf-8');

		if ($this->loginRequired && $_ENV['uid'] == 0) {
			Url::redirect('login');
		}

		//system_init 勾子仅支持 post
		$this->hook->post('system_init');
	}

	private function initCore()
	{
		$app = getApp();

		//加载核心配置
		$core = $app->config->load('config', true, true);

		if ($core['url_rewrite']) {
			$app->config->set('entry_file', '');
		}

		if ($core['base_url']) {
			$app->config->set('base_url', $core['base_url']);
		}

		$app->config->set('auth_key', $core['secret_key']);
		$app->register('security', [
			'class' => 'vgot\Core\Security',
			'args' => [
				'password' => $core['secret_key']
			]
		]);

		switch ($core['cache_adapter']) {
			case 'file':
				$core['cache_file']['storDir'] = BASE_PATH.'/res/cache';
				$app->register('cache', [
					'class' => 'vgot\Cache\FileCache',
					'args' => $core['cache_file']
				]);
				break;
			case 'db':
				$app->register('cache', [
					'class' => 'vgot\Cache\DbCache',
					'args' => $core['cache_db']
				]);
				break;
			case 'memcache':
				$app->register('cache', [
					'class' => 'vgot\Cache\Memcache',
					'args' => $core['cache_memcache']
				]);
				break;
			case 'redis':
				$app->register('cache', [
					'class' => 'vgot\Cache\Redis',
					'args' => $core['cache_redis']
				]);
				break;
		}

		//加载应用设置
		$options = OptionService::getOptions();
		$app->config->setAll($options);
	}

	private function initUser() {
		$authCode = vgetcookie('auth');

		$_ENV['uid'] = 0;
		$_ENV['user'] = null;

		if ($authCode != '') {
			$app = getApp();
			$authStr = $app->security->decrypt($authCode, true);

			if ($authStr != '') {
				list($uid, $pwdSegment, $loginTime) = explode("\t", $authStr);
				$user = UserService::getUser($uid);

				if ($user && substr($user['password'], -8) == $pwdSegment) {
					if ($user['status'] == 'disabled') {
						showMessage('error: 您的帐号已被禁用。', ':close');
					}

					$user['password'] = '';

					//是否拥有进入后台的权限
					$_ENV['manage_permission'] = ($user['group'] == 'admin' || $user['group'] == 'writer');

					$_ENV['uid'] = $user['uid'];
					$_ENV['user'] = $user;
				}
			}
		}
	}

	/**
	 * 是否提交了POST表单
	 *
	 * @return bool
	 */
	protected function isSubmit() {
		return ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST));
	}

}