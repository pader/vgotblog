<?php

namespace app\libs;

use app\services\OptionService;

/**
 * 插件基类
 * @package app\libs
 */
abstract class Plugin
{

	private $optionPrefix;

	public function __construct($name) {
		$this->optionPrefix = 'p:'.$name.':';
		$this->__init();
	}

	protected function __init() {
	}

	/**
	 * 动态钩子列表
	 *
	 * @return array
	 */
	abstract public function hooks();

	/**
	 * 插件配置列表
	 *
	 * @return array|null
	 */
	public function options() {
		return null;
	}

	public function get($name, $defaultValue=null)
	{
		return opt($this->optionPrefix.$name, $defaultValue);
	}

	/**
	 * 保存插件配置
	 *
	 * @param string $name
	 * @param mixed $value
	 * @return bool 配置是否保存成功
	 * 配置在名称与 option 中注册的不一致时，或类型不能被支持时可能保存失败
	 */
	public function set($name, $value)
	{
		return OptionService::set($this->optionPrefix.$name, $value, 'setting');
	}

}