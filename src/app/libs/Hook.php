<?php

namespace app\libs;

class Hook
{

	private $hooks;
	private $pluginInstances = [];

	public function __construct() {
		$this->hooks = \opt('plugin_hooks', []);
	}

	public function getActionNames() {
		return ['pre', 'post'];
	}

	/**
	 * 前置勾子
	 *
	 * @param string $point
	 * @param mixed ...$arguments
	 * @return mixed 返回钩子处理的结果
	 * @throws
	 */
	public function pre($point, ...$arguments)
	{
		return $this->call($point, 'pre', $arguments);
	}

	/**
	 * 后置勾子
	 *
	 * @param string $point
	 * @param mixed ...$arguments
	 * @return mixed
	 * @throws
	 */
	public function post($point, ...$arguments)
	{
		return $this->call($point, 'post', $arguments);
	}

	private function call($point, $act, $arguments) {
		if (!isset($this->hooks[$point][$act])) {
			return null;
		}
		$result = null;
		foreach ($this->hooks[$point][$act] as $id => $callable) {
			if (is_array($callable)) {
				//动态钩子
				$entry = $this->getPluginInstance($id, $callable[0]);
				$ret = call_user_func_array([$entry, $callable[1]], $arguments);
			} elseif (is_callable($callable)) {
				$ret = call_user_func_array($callable, $arguments);
			} else {
				throw new \ErrorException("插件 $id 的 $callable 无法执行。");
			}

			if ($ret !== null) {
				$result = $ret;
			}
		}
		return $result;
	}

	private function getPluginInstance($id, $className) {
		if (!isset($this->pluginInstances[$id])) {
			$this->pluginInstances[$id] = new $className($id);
		}
		return $this->pluginInstances[$id];
	}

}