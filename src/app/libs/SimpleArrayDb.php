<?php

namespace app\libs;

/**
 * Simple Array Database
 *
 * SimpleArrayDb 是一个简单的基于数组的数据库实现
 * 可以用于一些超简单的结构化数据库需求，而不需要在真正的数据库中建立表
 *
 * @package VgotBlog
 * @subpackage Libraries
 * @author pader
 * @copyright Copyright (c) 2015, VGOT.NET
 * @link http://www.vgot.net/
 * @filesource
 */
class SimpleArrayDb {

	protected $db = array();
	protected $struct = array();

	public function loadFromArray($name, $arr) {
		$this->db[$name] = $arr;
	}

	public function loadFromSerialize($name, $serialize) {
		$this->db[$name] = unserialize($serialize);
	}

	/**
	 * 创建个数据表
	 *
	 * @param string $name
	 * @param array $fields 字段列表
	 * @param string $autoIdField 指定自增数字的字段
	 */
	public function create($name, $fields, $autoIdField='') {
		//检查自增 ID 是否在字段列表中
		if ($autoIdField != '' && !in_array($autoIdField, $fields)) {
			showError('autoIdField must been set int fields!');
		}

		$struct = array(
			'fields' => $fields,
			'autoIdField' => $autoIdField,
			'autoId' => 1,
			'modified' => 0,
			'data' => array()
		);

		$this->loadFromArray($name, $struct);
	}

	/**
	 * 查询数据
	 *
	 * @param string $name
	 * @param array $cond [where, orderby='fieldName:sortType sortFlag', limit, offset]
	 * @return array|null
	 */
	public function get($name, $cond=array()) {
		$this->checkDb($name);

		$db =& $this->db[$name];

		$result = array();

		if (empty($cond['where'])) {
			foreach ($db['data'] as $i => $row) {
				$result[$i] = $this->formatRow($name, $row);
			}
		} else {
			//通过 WHERE 条件取得数据行索引
			$indexes = $this->fetchDataIndexes($name, $cond['where']);
			//通过行索引取得结果集
			foreach ($indexes as $i) {
				$result[$i] = $this->formatRow($name, $db['data'][$i]);
			}
		}

		//数据排序
		if (isset($cond['orderby'])) {
			list($sortField, $sortFlag) = explode(' ', $cond['orderby']);
			$sortType = '';
			if (strpos($sortField, ':')) {
				list($sortField, $sortType) = explode(':', $sortField);
			}

			//用于排序的字段是否存在
			$fi = array_search($sortField, $db['fields']);

			if ($fi === false) {
				showError("Query Error[$name]: Unknow column '$sortField' in orderby list!");
			}

			$sortArray = array();
			foreach ($result as $i => $row) {
				$sortArray[$i] = strtolower($row[$sortField]);
			}

			//未知排序字段类型时猜测一个类型类型
			if ($sortType == '') {
				$sortType = ctype_digit(current($sortArray)) ? 'numeric' : 'string';
			}

			$sortType = $sortType == 'numeric' ? SORT_NUMERIC : SORT_STRING;
			$sortFlag = strtolower($sortFlag) == 'asc' ? SORT_ASC : SORT_DESC;

			array_multisort($sortArray, $sortFlag, $sortType, $result);
		}

		//数据截取，最终数据
		$offset = isset($cond['offset']) ? $cond['offset'] : 0;
		$limit = isset($cond['limit']) ? $cond['limit'] : null;

		if ($offset > 0 || $limit !== null) {
			$result = array_slice($result, $offset, $limit);
		}

		return count($result) > 0 ? $result : null;
	}

	/**
	 * 删除数据
	 *
	 * @param string $name
	 * @param array $where
	 * @return int 影响的数据行数
	 */
	public function delete($name, $where) {
		$this->checkDb($name);

		$db =& $this->db[$name];
		$affected = 0;

		//更新数据
		if ($where) {
			$indexes = $this->fetchDataIndexes($name, $where);
			foreach ($indexes as $i) {
				unset($db['data'][$i]);
				++$affected;
			}
		} else {
			$affected = count($db['data']);
			$db['data'] = array();
		}

		$affected > 0 && $db['modified'] = time();

		return $affected;
	}

	/**
	 * 更新数据
	 *
	 * @param string $name
	 * @param array $value
	 * @param array $where
	 * @return int
	 */
	public function update($name, $value, $where=null) {
		$this->checkDb($name);

		$db =& $this->db[$name];

		//找出所有字段在行中的位置，并且判断字段是否存在
		$fp = array();
		foreach(array_keys($value) as $key) {
			$p = array_search($key, $db['fields']);
			if ($p === false) {
				showError("Query Error[$name]: Unknow column '$key' in update list!");
			}
			$fp[$key] = $p;
		}

		$affected = 0;

		//更新数据
		if ($where) {
			$indexes = $this->fetchDataIndexes($name, $where);
			foreach ($indexes as $i) {
				foreach ($value as $k => $v) {
					$db['data'][$i][$fp[$k]] = $v;
				}
				++$affected;
			}
		} else {
			foreach ($db['data'] as $i => $row) {
				foreach ($value as $k => $v) {
					$db['data'][$i][$fp[$k]] = $v;
				}
				++$affected;
			}
		}

		$affected > 0 && $db['modified'] = time();

		return $affected;
	}

	/**
	 * 插入数据
	 *
	 * @param string $name
	 * @param array $row
	 * @param bool $replace 是否以替换形式插入
	 */
	public function add($name, $row, $replace=false) {
		$this->checkDb($name);

		$db =& $this->db[$name];

		$autoId = false;
		$nextId = 0;

		if ($db['autoIdField'] != '') {
			$autoId = true;

			//输入行中包含自增ID字段的情况
			if (isset($row[$db['autoIdField']])) {
				$rowId = $row[$db['autoIdField']];

				//非替换插入检查自增 ID 是否重复
				if (!$replace) {
					foreach ($db['data'] as $j) {
						if ($j[$db['autoIdField']] == $rowId) {
							showError("Add Error[$name]: Duplicate key '{$db['autoIdField']}' of value'$rowId'!");
						}
					}
				}

				//替换插入的情况下，若 ID 大于表的下一个 ID，则递增表的 ID
				if ($rowId < $db['autoId']) {
					$nextId = $db['autoId'];
				} else {
					$nextId = $rowId + 1;
				}
			} else {
				$row[$db['autoIdField']] = $db['autoId'];
				$nextId = $row[$db['autoIdField']] + 1;
			}
		}

		//过滤不存在的字段值，并给予未指定字段赋 null 值
		$real = array();
		foreach ($db['fields'] as $i => $field) {
			$real[$i] = isset($row[$field]) ? $row[$field] : null;
		}

		//提交数据
		if ($db['autoIdField'] != '') {
			$db['autoId'] = $nextId;
			$i = array_search($db['autoIdField'], $db['fields']);
			$db['data'][$real[$i]] = $real;
		} else {
			$db['data'][] = $real;
		}

		$db['modified'] = time();
	}

	public function getArray($name) {
		if (isset($this->db[$name])) {
			return $this->db[$name];
		}

		return null;
	}

	public function getSerialize($name) {
		if (isset($this->db[$name])) {
			return serialize($this->db[$name]);
		}

		return null;
	}

	/**
	 * 检查数据库是否已加载
	 *
	 * @param string $name
	 */
	protected function checkDb($name) {
		if (!isset($this->db[$name])) {
			showError("Simple Array Db '$name' does not exists!");
		}
	}

	/**
	 * 根据条件找到指定数据所在的行索引
	 *
	 * @param string $name
	 * @param array $where
	 * @return array
	 */
	protected function fetchDataIndexes($name, $where) {
		$db =& $this->db[$name];

		//自增键直接返回记录位置，首先判断该行数据是否真实存在
		if ($db['autoIdField'] != '' && isset($where[$db['autoIdField']])) {
			return isset($db['data'][$where[$db['autoIdField']]]) ? array($where[$db['autoIdField']]) : array();
		}

		//找到各个字段在数组中的位置
		$fp = array(); //Fields Position

		foreach (array_keys($where) as $key) {
			$p = array_search($key, $db['fields']);
			if ($p === false) {
				showError("Query Error[$name]: Unknow column '$key' in where list!");
			}
			$fp[$key] = $p;
		}

		//遍历数据，找出匹配的行索引
		$indexes = array();

		foreach ($db['data'] as $i => $row) {
			foreach ($where as $key => $val) {
				if ($row[$fp[$key]] != $val) continue 2;
			}
			$indexes[] = $i;
		}

		return $indexes;
	}

	/**
	 * 将数据数组的原始形式的原始索引格式化为字段名索引
	 *
	 * @param string $name
	 * @param array $row
	 * @return array
	 */
	protected function formatRow($name, $row) {
		$real = array();
		foreach ($this->db[$name]['fields'] as $i => $field) {
			$real[$field] = $row[$i];
		}
		return $real;
	}

}
