<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2019/11/6
 * Time: 13:24
 */

namespace app\libs;


abstract class Model
{

	public function __construct() {
		$app = getApp();
		$app->db->from(static::tableName());
	}

	public abstract static function tableName();

	/**
	 * @return \vgot\Database\QueryBuilder
	 */
	public static function find()
	{
		$app = getApp();
		return $app->db->from(static::tableName());
	}

	/**
	 * Insert data
	 *
	 * @param array $data
	 * @param bool $repace
	 * @return int
	 */
	public static function insert($data, $repace=false)
	{
		return getApp()->db->insert(static::tableName(), $data, $repace);
	}

	/**
	 * Update data
	 *
	 * @param array $data
	 * @param null|array $where
	 * @return int
	 */
	public static function update($data, $where=null)
	{
		$app = getApp();
		$where && $app->db->where($where);
		return $app->db->update(static::tableName(), $data);
	}

	public static function delete($where)
	{
		$app = getApp();
		$where && $app->db->where($where);
		return $app->db->delete(static::tableName());
	}

}