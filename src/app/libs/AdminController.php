<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2018/5/6
 * Time: 22:37
 */

namespace app\libs;

abstract class AdminController extends BlogController
{

	protected $loginRequired = true;

	public function __init()
	{
		if (!$_ENV['manage_permission']) {
			showMessage('error: 您无权进入管理后台！');
		}

		//加载控制器与动作参数用于判断菜单位置
		$uri = $this->input->uri();
		$controller = strtolower(substr($uri['controller'], strrpos($uri['controller'], '\\')+1, -10));
		$this->view->vars('LOCATE', $controller.'-'.$uri['action']);
	}

}