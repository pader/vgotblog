<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2020/2/5
 * Time: 14:39
 */

namespace app\services;


class EncryptService
{

	public static function passwordHash($password) {
		return getApp()->passwordHash->HashPassword($password);
	}

	public static function passwordVerify($password, $hash) {
		return getApp()->passwordHash->CheckPassword($password, $hash);
	}

}