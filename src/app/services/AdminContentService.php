<?php

namespace app\services;

use app\models\Comment;
use app\models\Content;
use app\models\Option;

/**
 * 内容管理服务
 *
 * @package Services
 */
class AdminContentService {

	/**
	 * 检查别名是否存在
	 *
	 * @param string $alias
	 * @return bool
	 */
	public static function isAliasExists($alias) {
		return getApp()->db->from('content')->select('alias')->where(array('alias'=>$alias))->scalar() === null ? false : true;
	}

	/**
	 * 获取微博内容列表
	 *
	 * @param int $mid
	 * @param int $uid
	 * @param int $offset
	 * @param int $limit
	 * @return array
	 */
	public static function fetchList($mid=null, $uid=null, $offset=0, $limit=20) {
		$app = getApp();
		$where = array();

		if ($mid !== null) {
			$where['r.mid'] = $mid;
			$app->db->join('relationship r', 'cid', 'inner');
		}

		if ($uid !== null) {
			$where['c.uid'] = $uid;
		}

		$where['c.type'] = 'post';

		$result = array(
			'count' => $app->db->from('content c')->where($where)->count(),
			'list' => array()
		);

		if ($result['count'] > 0) {
			$fields = 'c.cid,c.alias,c.title,c.created,c.uid,c.status,c.password,c.allowComment,c.commentNum,c.viewNum';

			//Join is not keep in query, so we need join again
			if ($mid !== null) {
				$app->db->join('relationship r', 'cid', 'inner');
			}

			//包含用户信息
			$fields .= ',u.username,u.nickname';
			$app->db->join('user u', 'u.uid=c.uid', 'left');

			$result['list'] = $app->db->from('content c')->select($fields)->where($where)
				->orderBy(['c.created'=>SORT_DESC])->limit($limit, $offset)->indexBy('cid')->fetchAll();

			//草稿信息
			self::listInjectDraft($result['list']);
		}

		return $result;
	}

	/**
	 * 获取独立页面列表
	 *
	 * @param int $uid
	 * @param int $offset
	 * @param int $limit
	 * @return array
	 */
	public static function fetchPageList($uid=null, $offset=0, $limit=20) {
		$app = getApp();
		$where = array();

		if ($uid !== null) {
			$where['c.uid'] = $uid;
		}

		$where['c.type'] = 'page';

		$result = array(
			'count' => $app->db->from('content c')->where($where)->count(),
			'list' => array()
		);

		$fields = 'c.cid,c.alias,c.title,c.created,c.uid,c.status,c.commentNum,c.viewNum,u.username,u.nickname,u.group as userGroup,u.status as userStatus';

		//包含用户信息
		$app->db->join('user u', 'u.uid=c.uid', 'left');

		$result['list'] = $app->db->from('content c')->select($fields)->where($where)
			->orderBy(['c.created'=>SORT_DESC])->limit($limit, $offset)->indexBy('cid')->fetchAll();

		//草稿信息
		self::listInjectDraft($result['list']);

		return $result;
	}

	/**
	 * 将列表中有草稿的记录更改为草稿数据
	 *
	 * @param array $list
	 */
	public static function listInjectDraft(&$list) {
		if (!$list) return;

		$cids = array_column($list, 'cid');
		$keys = array_map(function($cid) { return 'draft_'.$cid; }, $cids);

		//获取这些内容的草稿 CID
		$draftIds = array();

		$app = getApp();
		$app->db->from('option')->select('name, value')->where(array('name'=>$keys, 'type'=>'system'));
		while ($row = $app->db->fetch()) {
			$draftIds[$row['value']] = substr($row['name'], 6);  //draftId => realCid
		}

		if (count($draftIds) == 0) return;

		//获取草稿的内容，并注入到列表中
		$app->db->from('content')->select('cid,title,created')->where(array('cid'=>array_keys($draftIds)));

		while ($row = $app->db->fetch()) {
			$cid = $draftIds[$row['cid']];
			unset($row['cid']);

			foreach ($row as $k => $v) {
				$list[$cid][$k] = $v;
			}

			$list[$cid]['status'] = Content::STATUS_DRAFT;
		}
	}

	/**
	 * 往内容表中新增一条数据
	 *
	 * 如果内容表中有废弃数据，将回收该数据使用
	 *
	 * @param array $data
	 * @return int
	 */
	public static function add($data) {
		$app = getApp();

		//获取一条废弃内容
		$trashCid = Content::recyleCid();

		//如果有废弃内容，则回收使用废弃的内容
		if ($trashCid) {
			$data['cid'] = $trashCid;
			$app->db->insert('content', $data, true);
			return $trashCid;
		} else {
			$app->db->insert('content', $data);
			return $app->db->insertId();
		}
	}

	/**
	 * 获取指定内容的草稿
	 *
	 * @param int $cid
	 * @return array
	 */
	public static function getDraft($cid) {
		$draftId = self::getDraftId($cid);

		if ($draftId && $draft = ContentService::getContent($draftId)) {
			$draft['alias'] = substr($draft['alias'], 1);
			return $draft;
		} else {
			return null;
		}
	}

	/**
	 * 获取指定内容草稿的 CID
	 *
	 * @param int $cid
	 * @return int
	 */
	public static function getDraftId($cid) {
		return Option::get('draft_'.$cid);
	}

	/**
	 * 保存草稿
	 *
	 * @param int $cid
	 * @param array $row
	 * @return int
	 */
	public static function saveDraft($cid, $row) {
		$row['alias'] = '*'.$row['alias'];
		$row['type'] = 'draft';

		if (isset($row['cid'])) {
			Content::update($row, ['cid'=>$row['cid']]);
		} else {
			$row['cid'] = self::add($row);
			Option::set('draft_'.$cid, $row['cid'], 'system');
		}

		return $row['cid'];
	}

	/**
	 * 删除一条内容库数据
	 *
	 * @param $cid
	 * @param bool $real 是否真实移除，真移除将移除数据行，否则只是移除相关数据，内容数据本身则设为废弃状态待回收
	 */
	public static function remove($cid, $real=false) {
		$app = getApp();
		$row = Content::find()->select('type,status')->where(array('cid'=>$cid))->get();

		if (!$row) {
			return;
		}

		//移除草稿数据
		if ($row['type'] == 'post' || $row['type'] == 'page') {
			$draftId = self::getDraftId($cid);
			$draftId && self::remove($draftId);
		}

		if ($row['type'] == 'post' || $row['type'] == 'draft') {
			//移除元数据关系
			$mids = $app->db->from('relationship')->select('mid')->where(array('cid'=>$cid))->fetchAll();
			if ($mids) {
				$app->db->where(array('cid'=>$cid))->delete('relationship');

				//更新计数，草稿无需更新
				if ($row['status'] != Content::STATUS_DRAFT && $row['type'] != 'draft') {
					$mids = array_column($mids, 'mid');
					MetaService::updateCount($mids, -1);
				}
			}

			//删除附件
			$attachments = ContentService::getAttachments($cid);
			foreach ($attachments as $attach) {
				AttachmentService::deleteFromRow($attach);
			}
		}

		//移除草稿索引
		if ($row['type'] == 'draft') {
			//$this->db->delete('option', array('name'=>'draft_'.$realCid)); //realCid 来源 ?
			Option::delete(array('name like'=>'draft_%', 'value'=>$cid, 'category'=>'system'));
		}

		//删除所有评论
		Comment::delete(['cid'=>$cid]);

		$app->db->where(array('cid'=>$cid));

		if ($real) {
			$app->db->delete('content');
		} else {
			$app->db->update('content', array('alias'=>"#trash_$cid", 'type'=>'trash'));
		}
	}

}
