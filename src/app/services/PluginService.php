<?php

namespace app\services;

use app\libs\Plugin;

class PluginService
{

	const PLUGIN_DIR = BASE_PATH.'/res/plugin';

	/**
	 * 扫描并读取出所有的插件
	 *
	 * @return array
	 */
	public static function scanPlugins()
	{
		$plugins = [];
		$enables = opt('plugin_enables', []);

		foreach (glob(self::PLUGIN_DIR.'/*', GLOB_ONLYDIR) as $path) {
			if (!is_dir($path) || !is_file($path.'/plugin.php')) {
				continue;
			}
			$set = include $path.'/plugin.php';
			if (self::validate($set)) {
				$set['id'] = pathinfo($path, PATHINFO_FILENAME);
				$set['enabled'] = in_array($set['id'], $enables);
				$plugins[] = $set;
			}
		}

		return $plugins;
	}

	/**
	 * 获取指定插件的配置
	 *
	 * @param string $plugin
	 * @return array
	 * @throws \ErrorException
	 */
	public static function getPlugin($plugin)
	{
		if (!preg_match('/[a-z0-9_]/i', $plugin)) {
			throw new \ErrorException('插件名字 '.$plugin.' 不合法！');
		}

		$plugFile = self::PLUGIN_DIR.'/'.$plugin.'/plugin.php';

		if (is_file($plugFile)) {
			$set = include $plugFile;
			if (self::validate($set)) {
				$set['id'] = $plugin;
				return $set;
			}
		}

		throw new \ErrorException('找不到插件 '.$plugFile.'！');
	}

	/**
	 * 启用指定插件
	 *
	 * @param string $plugin
	 * @throws \ErrorException
	 */
	public static function enable($plugin)
	{
		$set = self::getPlugin($plugin);
		$list = opt('plugin_enables', []);
		$hooks = opt('plugin_hooks', []);

		$acts = array_flip(getApp()->hook->getActionNames());

		if (in_array($plugin, $list)) return;

		//注册静态钩子到勾子列表
		if (isset($set['hooks'])) {
			foreach ($set['hooks'] as $point => $actions) {
				foreach (array_intersect_key($actions, $acts) as $act => $callable) {
					if (!is_callable($callable)) {
						throw new \ErrorException("插件 {$plugin} 安装失败，{$callable} 不可调用。");
					}
					//静态勾子调用结构强制转为字符串
					if (is_array($callable)) {
						$callable = $callable[0] . '::' . $callable[1];
					}
					$hooks[$point][$act][$plugin] = $callable;
				}
			}
		}

		//注册动态钩子到勾子列表
		//动态钩子会覆盖同名钩子，所以动态和静态的同名勾子同时存在时，总是只有动态的生效
		if (isset($set['entry_class'])) {
			if (!class_exists($set['entry_class'])) {
				throw new \ErrorException("插件 {$plugin} 安装失败，{$set['entry_class']} 不存在。");
			}

			$ref = new \ReflectionClass($set['entry_class']);
			$entry = $ref->newInstance($plugin);

			if ($entry instanceof Plugin === false) {
				throw new \ErrorException("插件 {$plugin} 不是 \app\libs\Plugin 的继承实例，无法安装。");
			}

			$dynamicHooks = $entry->hooks();
			foreach ($dynamicHooks as $point => $actions) {
				foreach (array_intersect_key($actions, $acts) as $act => $method) {
					if (!is_callable([$entry, $method])) {
						throw new \ErrorException("插件 {$plugin} 安装失败，{$set['entry_class']}::{$method} 不可调用。");
					}
					//动态勾子为数组形式
					$hooks[$point][$act][$plugin] = [$set['entry_class'], $method];
				}
			}
		}

		array_unshift($list, $plugin);

		OptionService::set('plugin_enables', array_values($list), 'setting');
		OptionService::set('plugin_hooks', $hooks, 'setting');
	}

	/**
	 * 停用指定插件
	 *
	 * @param string $plugin
	 */
	public static function disable($plugin)
	{
		$list = opt('plugin_enables', []);
		$hooks = opt('plugin_hooks', []);

		if (!in_array($plugin, $list)) return;

		//将已注册的勾子从启用勾子中移除
		foreach ($hooks as $point => $actions) {
			foreach ($actions as $act => $callables) {
				if (isset($callables[$plugin])) {
					unset($hooks[$point][$act][$plugin]);
				}
				if (!$hooks[$point][$act]) {
					unset($hooks[$point][$act]);
				}
			}
			if (!$hooks[$point]) {
				unset($hooks[$point]);
			}
		}

		$list = array_diff($list, [$plugin]);

		OptionService::set('plugin_enables', array_values($list), 'setting');
		OptionService::set('plugin_hooks', $hooks, 'setting');
	}

	private static function validate($set)
	{
		return is_array($set)
			&& isset($set['name'])
			&& $set['version']
			&& (
				(isset($set['hooks']) && is_array($set['hooks']))
				|| isset($set['entry_class'])
			);
	}

}