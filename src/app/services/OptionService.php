<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2018/5/6
 * Time: 14:58
 */

namespace app\services;

use app\libs\SimpleArrayDb;
use app\models\Option;
use vgot\Utils\ArrayUtil;
use vgot\Web\Url;

class OptionService
{

	/**
	 * 自动保存导航数据
	 *
	 * 设置为 true 则导航发生更改时自动保存，因为导航是 simpleArrayDb 结构，若每次保存都每次都会有一次数据库存储操作
	 * 若一次添加多条，建议先关闭此项，在多次更改后，手动调用 saveNavs() 进行保存
	 *
	 * @var bool
	 */
	public static $navAutoSave = true;

	public static function getOptions()
	{
		$app = getApp();
		$option = $app->cache->get('option');

		if ($option === null) {
			$option = Option::fetchByCategory('setting');

			if (isset($option['nav'])) {
				$adb = new SimpleArrayDb();
				$adb->loadFromSerialize('nav', $option['nav']);

				$navData = $adb->get('nav', array(
					'where' => array('visible'=>1),
					'orderby' => 'sort asc'
				));

				//组织导航缓存
				$navs = [];

				foreach ($navData as $row) {
					switch ($row['type']) {
						case 'system':
							$navs[] = ['name'=>$row['name'], 'url'=>Url::site($row['url']), 'flag'=>"SYSTEM_{$row['id']}", 'newpage'=>$row['newpage']];
							break;
						case 'category':
							$category = MetaService::getMeta($row['url']);
							if (!$category) continue 2;
							$navs[] = ['name'=>$row['name'], 'url'=>url($category, 'category'), 'flag'=>"CATEGORY_{$row['url']}", 'newpage'=>$row['newpage']];
							break;
						case 'page':
							$page = ContentService::getContent($row['url']);
							if (!$page) continue 2;
							$navs[] = ['name' => $row['name'], 'url'=>url($page, 'page'), 'flag'=>"PAGE_{$row['url']}", 'newpage'=>$row['newpage']];
							break;
						case 'out':
							$navs[] = ['name' => $row['name'], 'url'=>$row['url'], 'flag'=>'', 'newpage'=>$row['newpage']];
							break;
					}
				}

				$option['nav'] = $navs;
			}

			$app->cache->set('option', $option);
		}

		return $option;
	}

	public static function set($key, $value, $category)
	{
		if (opt($key) === $value) {
			return true;
		}

		if (Option::set($key, $value, $category)) {
			getApp()->config->set($key, $value);
			getApp()->cache->delete('option');
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 获取导航的简单数组数据库实例
	 *
	 * @return SimpleArrayDb
	 */
	private static function adb()
	{
		static $adb = null;

		if ($adb === null) {
			$app = getApp();

			$adb = new SimpleArrayDb();
			$navOpt = Option::get('nav');

			if ($navOpt) {
				$adb->loadFromSerialize('nav', $navOpt);
			} else {
				$adb->create('nav', array('id', 'name', 'type', 'sort', 'url', 'newpage', 'visible'), 'id');
				$adb->add('nav', array('id'=>1, 'name'=>'首页', 'type'=>'system', 'sort'=>0, 'url'=>'', 'visible'=>1, 'newpage'=>0));
				$app->db->insert('option', array('name'=>'nav', 'value'=>$adb->getSerialize('nav'), 'type'=>'setting'));
			}
		}

		return $adb;
	}

	/**
	 * 获取导航列表
	 *
	 * @return array
	 */
	public static function getNavs() {
		return self::adb()->get('nav', array('orderby'=>'sort asc'));
	}

	/**
	 * 保存导航数据
	 */
	public static function navSave() {
		getApp()->db->where(array('name'=>'nav'))->update('option', array('value'=>self::adb()->getSerialize('nav')));
	}

	public static function getNav($id) {
		$nav = self::adb()->get('nav', array('where'=>array('id'=>$id)));
		return $nav ? current($nav) : null;
	}

	public static function addNav($name, $type, $url, $newPage=0, $sort=99) {
		self::adb()->add('nav', array('name'=>$name, 'type'=>$type, 'sort'=>$sort, 'url'=>$url, 'visible'=>1, 'newpage'=>$newPage));
		self::$navAutoSave && self::navSave();
	}

	public static function removeNav($id) {
		self::adb()->delete('nav', array('id'=>$id));
		self::$navAutoSave && self::navSave();
	}

	/**
	 * 更新指定导航数据
	 *
	 * @param int $id
	 * @param array $data 允许更改的字段 [name,sort,url,newpage,visible]
	 */
	public static function updateNav($id, $data) {
		self::adb()->update('nav', $data, array('id'=>$id));
		self::$navAutoSave && self::navSave();
	}

	/**
	 * 更新系统核心配置
	 */
	public static function updateCoreConfig()
	{
		$app = getApp();
		$config = Option::fetchByCategory('core');

		$databases = $app->config->load('databases', true, true);
		$dbc = $databases[$databases['default_connection']];

		$config['db'] = [
			'host' => $dbc['host'],
			'port' => $dbc['port'],
			'username' => $dbc['username'],
			'password' => $dbc['password'],
			'database' => $dbc['database'],
			'prefix' => $dbc['table_prefix']
		];

		$config['secret_key'] = $app->config->get('auth_key');

		$content = "<?php //update at ".date('Y-m-d H:i:s')."\nreturn ".ArrayUtil::export($config, true).";\n";
		$file = RES_PATH.'/config.php';

		file_put_contents($file, $content);

		if (function_exists('opcache_invalidate') && opcache_is_script_cached($file)) {
			opcache_invalidate($file);
		}
	}

}