<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2018/5/6
 * Time: 15:23
 */

namespace app\services;

use app\models\Content;
use vgot\Web\Url;

class ContentService
{

	public static function getContent($cid) {
		return getApp()->db->from('content')->where(['cid'=>$cid])->fetch();
	}

	/**
	 * 根据别名获取内容
	 *
	 * @param string $alias
	 * @return array
	 */
	public function getContentByAlias($alias) {
		return getApp()->db->from('content')->where(['alias'=>$alias])->fetch();
	}

	public static function getContentByUser($cid) {
		$where = [
			'AND',
			'cid' => $cid
		];

		Content::privacyWhere($where, $_ENV['uid']);

		return Content::find()
			->select('cid,alias,type,title,created,uid,password,allowComment,commentNum')
			->where($where)->get();
	}

	/**
	 * 获取微博内容列表
	 *
	 * @param int $mid
	 * @param int $uid 要显示的人的文章
	 * @param int $myUid 自己的 UID，此内容决定是否显示自己的私密内容
	 * @param int $limit
	 * @param int $offset
	 * @return array
	 */
	public static function fetchList($mid=null, $uid=null, $myUid=0, $offset=0, $limit=5) {
		$app = getApp();
		$where = ['AND'];

		if ($mid !== null) {
			$where['r.mid'] = $mid;
			$app->db->join('relationship r', 'cid', 'inner');
		}

		if ($uid === null) {
			//显示公开及自己的私密内容
			if ($myUid > 0) {
				$where[] = ['OR', ['c.status'=>1], ['c.uid'=>$myUid, 'c.status'=>3]];
			} else {
				$where['c.status'] = 1;
			}
		} else {
			$where['c.uid'] = $uid;
			//指定显示用户为自己，则直接显示私密的内容
			if ($uid == $myUid && $myUid > 0) {
				$where['c.status'] = [1, 3];
			} else {
				$where['c.status'] = 1;
			}
		}

		$where['c.type'] = 'post';

		$result = array(
			'count' => $app->db->from('content')->alias('c')->where($where)->count(),
			'list' => array()
		);

		if ($result['count'] > 0) {
			$fields = 'c.cid,c.alias,c.title,c.created,c.uid,c.status,c.password,c.allowComment,c.commentNum,c.content';

			$cond = array('orderby'=>'c.created desc', 'limit'=>"$offset, $limit");

			//Join is not keep in query, so we need join again
			if ($mid !== null) {
				$app->db->join('relationship r', 'cid', 'inner');
			}

			//包含用户信息
			$fields .= ',u.username,u.nickname,u.group as userGroup,u.status as userStatus';
			$app->db->leftJoin('user u', 'u.uid=c.uid')->from('content')->alias('c')->select($fields)
				->where($where)->orderBy(['c.created'=>SORT_DESC])->limit($limit, $offset);

			while ($row = $app->db->fetch()) {
				$breakPoint = strpos($row['content'], '__page_break_tag__');
				if ($breakPoint) {
					$row['content'] = substr($row['content'], 0, $breakPoint);
					$row['hasMore'] = true;
				} else {
					$row['hasMore'] = false;
				}

				$result['list'][] = $row;
			}
		}

		return $result;
	}


	/**
	 * 往内容列表中注入分类信息
	 *
	 * 同时会注入每条内容的 URL 地址
	 *
	 * @param array $list
	 */
	public static function listInjectCategory(&$list) {
		$app = getApp();
		$cids = array_column($list, 'cid');

		$app->db->join('relationship r', 'mid', 'right')->from('meta')->alias('m')
			->select('r.mid,r.cid,m.name,m.alias')
			->where(['r.cid'=>$cids, 'm.type'=>MetaService::TYPE_CATEGORY]);

		$metas = [];

		while ($row = $app->db->fetch()) {
			$metas[$row['cid']] = $row;
		}

		foreach ($list as &$row) {
			if (isset($metas[$row['cid']])) {
				$meta = $metas[$row['cid']];
				$row['mid'] = $meta['mid'];
				$row['category'] = $meta['name'];
				$row['malias'] = $meta['alias'];
			} else {
				$row['mid'] = null;
				$row['category'] = null;
				$row['malias'] = null;
			}

			$row['url'] = url($row, (isset($row['type']) ? $row['type'] : 'post'));
		}
	}

	/**
	 * 获取独立页面列表
	 *
	 * @param bool $showInvisible 是否包含未发布的
	 * @param bool $withContent
	 * @param bool $withUser
	 * @return array
	 */
	public static function fetchPageList($showInvisible=false, $withContent=false, $withUser=false) {
		$app = getApp();
		$where = array();

		if (!$showInvisible) {
			$where['c.status'] = 1;
		}

		$where['c.type'] = 'page';

		$fields = 'c.cid,c.alias,c.title,c.created,c.uid,c.status,c.commentNum';

		if ($withContent) {
			$fields .= ',c.content';
		}

		//包含用户信息
		if ($withUser) {
			$fields .= ',u.username,u.nickname,u.group as userGroup,u.status as userStatus';
			$app->db->join('user u', 'u.uid=c.uid', 'left');
		}

		return $app->db->from('content c')->select($fields)->where($where)->orderBy(['c.created'=>SORT_DESC])->fetchAll();
	}

	public static function getAttachments($cid) {
		$attachs = array();
		$app = getApp();

		$app->db->from('attachment')->where(array('cid'=>$cid))->orderBy(['uploaded'=>SORT_DESC]);
		while ($row = $app->db->fetch()) {
			$row['fullPath'] = 'uploads/'.$row['path'];
			$attachs[] = $row;
		}

		return $attachs;
	}

	/**
	 * 获取指定内容的 URL
	 *
	 * @param array $post
	 * @param bool $absolute 是否返回绝对地址
	 * @return string
	 */
	public static function getPostUrl($post, $absolute=false) {
		if ($post['type'] == 'post') {
			$meta = getApp()->db->from('meta m')
				->join('relationship r', 'mid', 'inner')
				->where(['r.cid'=>$post['cid'], 'm.type'=>MetaService::TYPE_CATEGORY])
				->select('m.mid,m.alias AS malias')->get();
			$meta && $post += $meta;
		}

		return url($post, $post['type'], $absolute);
	}

}