<?php

namespace app\services;

use app\models\User;

class UserService {

	const UID = 0;
	const USERNAME = 1;
	const MAIL = 2;

	public static function getUser($userId, $fetchType=self::UID) {
		switch ($fetchType) {
			case self::UID: $key = 'uid'; break;
			case self::USERNAME: $key = 'username'; break;
			case self::MAIL: $key = 'mail'; break;
			default: return false;
		}

		return User::find()->where(array($key=>$userId))->get();
	}

	public static function updatePassword($uid, $password)
	{
		$hash = EncryptService::passwordHash($password);
		User::update(['password'=>$hash], ['uid'=>$uid]);
	}

}
