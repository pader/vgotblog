<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2018/6/17
 * Time: 18:37
 */

namespace app\services;

class UserManagerService
{

	public static function fetchList($offset=0, $limit=20) {
		$app = getApp();

		$result = array(
			'count' => $app->db->from('user')->count(),
			'list' => array()
		);

		$result['list'] = $app->db->select('uid,username,nickname,mail,group,status')
			->from('user')->limit($limit, $offset)->fetchAll();

		return $result;
	}

}