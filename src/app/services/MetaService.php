<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2018/5/6
 * Time: 15:18
 */

namespace app\services;


class MetaService
{

	const BY_MID = 0;
	const BY_ALIAS = 1;
	const TYPE_CATEGORY = 'category';
	const TYPE_TAG = 'tag';

	public static function getMeta($id, $type=self::TYPE_CATEGORY, $by=self::BY_MID) {
		$keyField = $by == self::BY_ALIAS ? 'alias' : 'mid';
		return getApp()->db->from('meta')->where(array($keyField=>$id, 'type'=>$type))->get();
	}

	public static function getCategories() {
		$app = getApp();

		$data = $app->cache->get('categories');

		if ($data === null) {
			$query = getApp()->db->from('meta')->select('mid,name,alias,description,count')
				->where(array('type'=>self::TYPE_CATEGORY));

			$data = [];

			while ($row = $query->fetch()) {
				$data[$row['mid']] = $row;
			}

			$app->cache->set('categories', $data);
		}

		return $data;
	}


	/**
	 * Add Meta Row
	 *
	 * @param string $name
	 * @param string $alias
	 * @param string $description
	 * @param string $type
	 * @return int
	 */
	public static function add($name, $alias, $description, $type) {
		$app = getApp();
		$app->db->insert('meta', array('name'=>$name, 'alias'=>$alias, 'description'=>$description, 'type'=>$type));
		return $app->db->insertId();
	}

	/**
	 * Update Meta Data
	 *
	 * @param int $mid
	 * @param string $name
	 * @param string $alias
	 * @param string $description
	 * @return bool
	 */
	public static function update($mid, $name=null, $alias=null, $description=null) {
		$update = array();
		$name && $update['name'] = $name;
		$alias && $update['alias'] = $alias;
		$description && $update['description'] = $description;

		if ($update) {
			getApp()->db->where(array('mid'=>$mid))->update('meta', $update);
			return true;
		}

		return false;
	}

	/**
	 * 更新 Meta 计数
	 *
	 * @param int|array $mids
	 * @param int $num
	 */
	public static function updateCount($mids, $num) {
		if ($num == 0) return;

		!is_array($mids) && $mids = array($mids);
		$where = array('mid'=>$mids);
		$set = array('^count'=>'count'.($num > 0 ? "+$num" : '-'.abs($num)));

		getApp()->db->where($where)->update('meta', $set);
	}

	/**
	 * 获取指定 CID 的相关分类标签信息
	 *
	 * @param int $cid
	 * @param string $type category|tag ?
	 * @return array
	 */
	public static function getCidMetas($cid, $type=null) {
		$where = array('r.cid'=>$cid);

		if ($type !== null) {
			$where['m.type'] = $type;
		}

		return getApp()->db->join('relationship r', 'mid', 'inner')->from('meta m')->where($where)
			->select('m.mid,m.name,m.alias,m.type,m.count')->indexBy('mid')->fetchAll();
	}

	/**
	 * 根据标签名获取已存在的标签
	 *
	 * @param array $tagArr
	 * @return array
	 */
	public static function getTagByAlias($tagArr) {
		return getApp()->db->from('meta')->select('mid,name,alias,count')->where(array('alias'=>$tagArr, 'type'=>'tag'))
			->indexBy('alias')->fetchAll();
	}

	/**
	 * 热门标签缓存
	 *
	 * @return array
	 */
	public static function getHotTags() {
		$app = getApp();
		$data = $app->cache->get('hot_tags');

		if ($data === null) {
			$data = $app->db->from('meta')->select('mid,name,alias,count')->where(array('type'=>'tag', 'count >'=>0))
				->orderBy(['count'=>SORT_DESC])->limit(50)->fetchAll();
			$app->cache->set('hot_tags', $data, 86400);
		}

		return $data;
	}

	/**
	 * 根据前缀搜索
	 *
	 * @param string $search 要搜索的前缀
	 * @param string $type 要搜索的 meta 类型
	 * @return array
	 */
	public static function searchByPrefix($search, $type)
	{
		return from('meta')->select('mid,name,alias')->where(['alias LIKE'=>"$search%", 'type'=>$type])->limit(20)->fetchAll();
	}

}