<?php

namespace app\services;

use app\models\Comment;
use app\models\Content;
use app\models\User;

/**
 * 评论服务
 * @package app\services
 */
class CommentService
{

	/**
	 * 向指定内容添加一个评论
	 *
	 * @param int $cid
	 * @param array $data
	 * @return int 新评论的ID
	 */
	public static function addComment($cid, $data)
	{
		$data += [
			'cid' => $cid,
			'created' => now(),
			'uid' => $_ENV['uid'],
			'ip' => getApp()->input->clientIp(),
			'status' => Comment::STATUS_PUBLIC
		];

		Comment::insert($data);
		$commentId = getApp()->db->insertId();

		if ($data['status'] == Comment::STATUS_PUBLIC) {
			Content::update(['^commentNum' => '`commentNum`+1'], ['cid' => $cid]);
		}

		return $commentId;
	}

	/**
	 * 获取指定内容中的评论数据
	 *
	 * @param int $cid
	 * @param int $offset
	 * @param int $limit
	 * @return array
	 * @throws \vgot\Exceptions\DatabaseException
	 */
	public static function fetchByCid($cid, $offset=0, $limit=20)
	{
		$where = ['cid'=>$cid, 'parent_id'=>0, 'status'=>Comment::STATUS_PUBLIC];

		$result = [
			'count' => Comment::find()->where($where)->count(),
			'list' => []
		];

		if ($result['count'] > 0) {
			$lids = $uids = $comments = [];

			$query = Comment::find()->where($where)->orderBy(['created' => SORT_DESC])->limit($limit, $offset)
				->select('cmtid,uid,created,author,mail,content,url');

			while ($row = $query->fetch()) {
				$row['uid'] > 0 && $uids[] = $row['uid'];
				$comments[$row['cmtid']] = $row;
				$lids[] = $row['cmtid'];
			}

			//获取所有子评论
			$replids = [];
			$where['parent_id'] = array_keys($comments);
			$query = Comment::find()->where($where)->orderBy(['created'=>SORT_ASC])
				->select('cmtid,parent_id,reply_id,uid,created,author,mail,content,url');
			while ($row = $query->fetch()) {
				$row['uid'] > 0 && $uids[] = $row['uid'];
				$comments[$row['cmtid']] = $row;
				$replids[$row['parent_id']][] = $row['cmtid'];
			}

			$users = [];

			if ($uids) {
				$uids = array_unique($uids);
				$users = User::find()->where(['uid'=>$uids])->select('uid,username,nickname,mail,url,group')
					->indexBy('uid')->fetchAll();
			}

			//设置用户
			foreach ($comments as &$row) {
				if ($row['uid'] > 0 && isset($users[$row['uid']])) {
					$user = $users[$row['uid']];
					$row['author'] = $user['nickname'];
					$row['mail'] = $user['mail'];
					$row['url'] = $user['url'];
					$row['group'] = $user['group'];
				} else {
					$row['group'] = 'guest';
				}
			}

			unset($row);
			$list = [];

			foreach ($lids as $id) {
				$row = $comments[$id];
				$row['replies'] = [];

				if (isset($replids[$row['cmtid']])) {
					foreach ($replids[$row['cmtid']] as $replyId) {
						$reply = $comments[$replyId];

						//回复的用户
						if (isset($comments[$reply['reply_id']])) {
							$replyComment = $comments[$reply['reply_id']];
							$reply['reply_uid'] = $replyComment['uid'];
							$reply['reply_author'] = $replyComment['author'];
							$reply['reply_mail'] = $replyComment['mail'];
							$reply['reply_url'] = $replyComment['url'];
						} else {
							$reply['reply_uid'] = 0;
							$reply['reply_author'] = 'DELETED';
							$reply['reply_mail'] = '';
							$reply['reply_url'] = '';
						}

						$row['replies'][] = $reply;
					}
				}

				$list[] = $row;
			}

			$result['list'] = $list;
		}

		return $result;
	}

	/**
	 * 保存游客评论时的基础信息
	 *
	 * @param string $author
	 * @param string $email
	 * @param string $url
	 */
	public static function saveAuthorInfo($author, $email, $url)
	{
		vsetcookie('comment_author', json_encode([$author, $email, $url?:'']));
	}

	/**
	 * 获取游客评论时的基础信息
	 *
	 * @return array
	 */
	public static function getAuthorInfo()
	{
		$author = $email = $url = '';

		$json = vgetcookie('comment_author');
		if ($json && ($arr = @json_decode($json)) && is_array($arr)) {
			list($author, $email, $url) = $arr;
		}

		return compact('author', 'email', 'url');
	}

	/**
	 * 重新计算指定文章的评论数
	 *
	 * @param array $cids
	 * @throws \vgot\Exceptions\DatabaseException
	 */
	public static function recountComment($cids) {
		if (!$cids) return;
		$app = getApp();
		$postTable = $app->db->tableName(Content::tableName());
		$commentTable = $app->db->tableName(Comment::tableName());
		$strCids = join(',', $cids);
		$status = Comment::STATUS_PUBLIC;
		$app->db->exec("UPDATE $postTable p SET p.commentNum=(SELECT COUNT(*) FROM $commentTable c WHERE c.cid=p.cid AND c.status=$status) WHERE p.cid IN($strCids)");
	}

}