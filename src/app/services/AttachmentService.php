<?php

namespace app\services;

use app\models\Option;

class AttachmentService {

	const DIR = 'uploads';

	/**
	 * 添加一条附件数据
	 *
	 * @param int $cid 所属内容ID，内容未创建时使用 0，将被标记为用户的未关联附件
	 * @param int $uid
	 * @param array $data 文件信息数组，仅需指定 filename, filesize,path,filetype 即可，uploaded 可选
	 * @return int
	 */
	public static function add($cid, $uid, $data) {
		if (!isset($data['uploaded'])) {
			$data['uploaded'] = now();
		}

		$data['cid'] = $cid ? $cid : 0;
		
		$app = getApp();

		$app->db->insert('attachment', $data);
		$aid = $app->db->insertId();

		if ($data['cid'] == 0) {
			$unattachIds = self::getUnattachIds($uid);
			$unattachIds[] = $aid;
			self::setUnattachIds($uid, $unattachIds);
		}

		return $aid;
	}

	/**
	 * 获取指定用户未关联的附件
	 *
	 * @param int $uid
	 * @return array
	 */
	public static function getUnattachIds($uid) {
		$unattach = Option::get('unattach_uid_'.$uid);
		return !empty($unattach) ? $unattach : [];
	}

	/**
	 * 存储未关联的附件ID信息到指定的用户
	 *
	 * @param int $uid
	 * @param array $ids
	 */
	public static function setUnattachIds($uid, $ids) {
		Option::set('unattach_uid_'.$uid, array_values($ids), 'system');
	}

	/**
	 * 删除指定用户未关联的附件的关联信息
	 * @param int $uid
	 */
	public static function delUnattachIds($uid) {
		Option::remove('unattach_uid_'.$uid);
	}

	/**
	 * 获取指定用户未关联的附件列表
	 *
	 * @param int $uid
	 * @return array
	 */
	public static function getUnattachments($uid) {
		$ids = self::getUnattachIds($uid);

		$attachs = array();

		if ($ids) {
			$app = getApp();
			$app->db->from('attachment')->where(array('aid'=>$ids))->orderBy(['uploaded'=>SORT_DESC]);
			while ($row = $app->db->fetch()) {
				$row['basePath'] = 'uploads/'.$row['path'];
				$attachs[] = $row;
			}
		}

		return $attachs;
	}

	/**
	 * 从附件的数据行中删除附件
	 *
	 * @param array $row
	 * @return bool
	 */
	public static function deleteFromRow($row) {
		if (!isset($row['fullPath'])) {
			$row['fullPath'] = self::DIR.'/'.$row['path'];
		}

		$file = BASE_PATH.'/'.$row['fullPath'];

		if (!is_file($file) || unlink($file)) {
			getApp()->db->where(array('aid'=>$row['aid']))->delete('attachment');
			return true;
		}

		return false;
	}

	public static function fetchList($offset=0, $limit=10)
	{
		$app = getApp();
		$count = $app->db->from('attachment')->count();
		
		$result = array(
			'count' => $count,
			'list' => array()
		);

		if ($count > 0) {
			$fields = 'a.*,c.alias,c.title,c.created,c.uid,c.type,c.status,c.password,c.allowComment,c.commentNum';
			$app->db->join('content c', 'cid', 'left');
			$result['list'] = $app->db->from('attachment a')->select($fields)->orderBy(array('a.aid'=>SORT_DESC))
				->limit($limit, $offset)->fetchAll();
		}

		return $result;
	}
	
	public static function getDetail($aid)
	{
		$fields = 'a.*,c.alias,c.title,c.created,c.uid,c.type,c.status,c.password,c.allowComment,c.commentNum';
		return getApp()->db->join('content c', 'cid', 'left')->from('attachment a')
			->select($fields)->where(array('a.aid'=>$aid))->get();
	}

}
