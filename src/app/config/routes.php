<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 17-4-23
 * Time: 下午11:29
 */

$routes = [];

if (is_file(RES_PATH.'/config.php')) {
	$core = getApp()->config->load('config', true, true);

	//所有支持的参数列表
	$params = array(
		'cid' => '(\d+)',
		'mid' => '(\d+)',
		'alias' => '([\w\-\.]+?)',
		'category' => '([\w\-]+?)',
		'year' => '(\d{4})',
		'month' => '(\d{1,2})',
		'day' => '(\d{1,2})'
	);

	$resetList = array(
		'url_category' => array('index/category', 'mid,alias'),
		'url_post' => array('post/post', 'cid,alias,category,year,month,day'),
		'url_page' => array('post/page', 'cid,alias')
	);

	$PATH = isset($_SERVER['PATH_INFO']) ? trim($_SERVER['PATH_INFO'], '/') : '';

	foreach ($resetList as $name => $row) {
		//添加到系统路由匹配规则中
		$search = $replace = array();
		foreach (explode(',', $row[1]) as $key) {
			$search[] = '\{' . $key . '\}';
			$replace[] = $params[$key];
		}

		$urx = trim($core[$name], '/');
		$pattern = str_replace($search, $replace, preg_quote($urx));
		$routes[$pattern] = $row[0];

		//找出其中存在的参数列表
		if (preg_match('#^' . $pattern . '$#', $PATH, $pathVal)) {
			preg_match_all('#\{(' . str_replace(',', '|', $row[1]) . ')\}#', $urx, $pathKey);
			$_ENV['route_params'] = array_combine($pathKey[1], array_slice($pathVal, 1));
			break;
		}
	}
}

return [
	'default_controller' => 'IndexController',
	'default_action' => 'index',
	'404_override' => false,
	'404_view' => 'errors/404',

	/**
	 * Router method
	 *
	 * Router base on uri params, this configure is setting what type of params use for router.
	 * PATH_INFO, QUERY_STRING, GET
	 */
	'route_method' => 'PATH_INFO',

	/**
	 * Set route param name when router_method is GET.
	 * Get value only allow a-z0-9\-_
	 */
	'route_param' => 'uri',
	'suffix' => '',

	/**
	 * Set case symbol in url
	 * @var string|false
	 */
	'case_symbol' => '-',

	//路由中的目录名是否首字母大写
	'ucfirst' => false,

	'route_maps' => [
		'(admin|login|user)' => '$1',
		'tag/(.+)' => 'index/tag/$1',
		'author/(\d+)' => 'index/author/$1'
	] + $routes
];

