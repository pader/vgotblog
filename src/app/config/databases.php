<?php
/**
 * Created by PhpStorm.
 * User: Pader
 * Date: 2017/4/25
 * Time: 0:52
 */

$db = getApp()->config->get('db', 'config');

return [
	'default_connection' => 'main',
	'main' => [
		'dsn' => '',
		'host' => $db['host'],
		'port' => $db['port'],
		'username' => $db['username'],
		'password' => $db['password'],
		'database' => $db['database'],
		'table_prefix' => $db['prefix'],
		'type' => 'mysql',
		'driver' => 'mysqli',
		'pconnect' => false,
		'charset' => 'utf8',
		'timeout' => 5, //connect timeout seconds
		'collate' => 'utf8_general_ci',
		'query_builder' => true,
		'debug' => true
	]
];
