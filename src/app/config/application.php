<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2017/4/23
 * Time: 02:26
 */

return [
	'id' => 'vgotblog',
	'base_url' => '',
	'entry_file' => 'index.php',

	//The providers to register
	'providers' => [
		'passwordHash' => [
			'class' => 'app\libs\PasswordHash',
			'args' => [
				'iteration_count_log2' => 8,
				'portable_hashes' => true
			]
		],
		'hook' => [
			'class' => 'app\libs\Hook'
		]
	],

	//Output
	'output_charset' => 'utf-8',
	'output_gzip' => true,
	'output_gzip_level' => 8,
	'output_gzip_minlen' => 1024, //1KB
	'output_gzip_force_soft' => false, //是否强制使用框架自带的gzip压缩，否则会检测是否可以启用PHP内置压缩

	/**
	 * 设置系统的错误捕捉事件，如果不设此选项，则使用比方内置方法
	 *
	 * @var callable
	 */
	'set_error_handler' => null,

	//应用程序自定义配置区域，非框架部分

	//加密密钥
	'auth_key' => '',

	//VgotBlog 版本信息
	'version' => '0.10.1'
];