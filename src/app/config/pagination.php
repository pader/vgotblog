<?php

//Boostrap 分页风格设置
return [
	//整个分页起始标签设置
	'fullTagOpen' => '<nav><ul class="pager">',
	'fullTagClose' => '</ul></nav>',

	//常规数字链接起始标签设置
	'numTagOpen' => '<li>',
	'numTagClose' => '</li> ',

	//当前页起始标签设置
	'curTagOpen' => '<li class="active"><span>',
	'curTagClose' => '</span></li> ',

	//下一页链接及起始标签设置
	'nextLink' => '下一页',
	'nextTagOpen' => '<li>',
	'nextTagClose' => '</li> ',

	//上一页链接及起始标签设置
	'prevLink' => '上一页',
	'prevTagOpen' => '<li>',
	'prevTagClose' => '</li> ',

	//第一页链接及起始标签设置
	'firstLink' => '首页',
	'firstTagOpen' => '<li>',
	'firstTagClose' => '</li> ',

	//页面很多时过渡链接
	'transTag' => '...',

	//最后一页链接及起始标签设置
	'lastLink' => '尾页',
	'lastTagOpen' => '<li>',
	'lastTagClose' => '</li> ',

	'showPageCount' => false
];
