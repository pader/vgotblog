<?php


namespace app\models;

use app\libs\Model;

class Option extends Model
{

	public static function tableName() {
		return 'option';
	}

	public static function get($name)
	{
		$row = self::find()->select('value,type')->where(['name'=>$name])->get();
		return $row ? self::parseValue($row['value'], $row['type']) : null;
	}

	public static function set($name, $value, $category, $createIfNotExists=true)
	{
		$existsCategory = self::find()->select('category')->where(['name'=>$name])->scalar();

		if ($existsCategory ? $existsCategory !== $category : !$createIfNotExists) {
			return false;
		}

		$type = self::getType($value);

		switch ($type) {
			case 'serialize': $value = serialize($value); break;
			case 'bool': $value = $value ? 1 : 0; break;
		}

		if ($existsCategory) {
			self::update(['value'=>$value, 'type'=>$type], ['name'=>$name]);
		} else {
			self::insert(compact('name', 'value', 'category', 'type'));
		}

		return true;
	}

	public static function remove($name)
	{
		self::delete(['name'=>$name]);
	}


	public static function fetchByCategory($category)
	{
		$query = self::find()->select('name,value,type')->where(['category'=>$category]);
		$data = [];
		while ($row = $query->fetch()) {
			$data[$row['name']] = self::parseValue($row['value'], $row['type']);
		}
		return $data;
	}

	private static function getType($value)
	{
		switch (gettype($value)) {
			case 'integer':
				return 'int';
			case 'double':
				return 'float';
			case 'boolean':
				return 'bool';
			case 'array':
			case 'object':
				return 'serialize';
			default:
				return 'string';
		}
	}

	private static function parseValue($value, $type)
	{
		switch ($type) {
			case 'int':
				return (int)$value;
			case 'bool':
				return (bool)$value;
			case 'float':
				return (float)$value;
			case 'serialize':
				return unserialize($value);
			default:
				return $value;
		}
	}

}