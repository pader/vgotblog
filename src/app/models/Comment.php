<?php

namespace app\models;

use app\libs\Model;

class Comment extends Model
{

	const STATUS_PENDING = 0;
	const STATUS_PUBLIC = 1;
	const STATUS_SPAM = 2;
	const STATUS_DELETED = 3;

	public static function tableName() {
		return 'comment';
	}

	public static function getAvailableComment($cid, $commentId)
	{
		return Comment::find()->where(['cid'=>$cid, 'cmtid'=>$commentId, 'status'=>self::STATUS_PUBLIC])
			->select('cmtid,cid,parent_id,reply_id,uid')->limit(1)->get();
	}

}