<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2019/11/6
 * Time: 13:59
 */

namespace app\models;

use app\libs\Model;

class Content extends Model
{

	const TYPE_POST = 'post';
	const TYPE_PAGE = 'page';
	const TYPE_DRAFT = 'draft';
	const TYPE_TRASH = 'trash';

	const STATUS_DRAFT = 0;
	const STATUS_PUBLIC = 1;
	const STATUS_HIDDEN = 2;
	const STATUS_PRIVATE = 3;
	const STATUS_DELETED = 4;

	public static function tableName() {
		return 'content';
	}

	public static function getStatusText($status) {
		switch ($status) {
			case self::STATUS_DRAFT: return '草稿';
			case self::STATUS_PUBLIC: return '公开';
			case self::STATUS_HIDDEN: return '隐藏';
			case self::STATUS_PRIVATE: return '私密';
		}
	}

	public static function privacyWhere(&$where, $uid=0, $alias='')
	{
		if ($alias != '') {
			$alias .= '.';
		}

		if ($uid) {
			$where[] = [
				'OR',
				[$alias.'status'=>Content::STATUS_PUBLIC],
				[$alias.'uid'=>$uid, $alias.'status'=>Content::STATUS_PRIVATE]
			];
		} else {
			$where[$alias.'status'] = 1;
		}
	}

	public static function recyleCid()
	{
		return self::find()->select('cid')->where(array('type'=>'trash'))
			->orderBy(['created'=>SORT_ASC])->limit(1)->scalar();
	}

}