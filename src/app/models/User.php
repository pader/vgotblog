<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2020/2/5
 * Time: 14:42
 */

namespace app\models;

use app\libs\Model;

class User extends Model
{

	public static function tableName() {
		return 'user';
	}

}