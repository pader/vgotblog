<?php $this->render('admin/header'); ?>

<h3>分类  <a href="<?=siteUrl('admin/meta/add')?>" class="btn btn-link btn-xs" style="margin-left:20px;" role="button">+ 新建分类</a></h3>

<?php if ($categories) { ?>
<div class="input-group" style="width:110px;margin-bottom:8px;">
	<div class="input-group-addon">
		<input type="checkbox" id="checkAll" />
	</div>
	<div class="input-group-btn">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">选中项 <span class="caret"></span></button>
		<ul class="dropdown-menu dropdown-action" role="menu">
			<li><a href="###" data-action="remove">删除</a></li>
			<li><a href="###" data-action="merge">合并至...</a></ul>
	</div>
</div>

<div class="wrap-table">
	<table class="table">
		<thead>
		<tr>
			<th>　</th>
			<th>名称</th>
			<th>别名</th>
			<th>文章数</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($categories as $row) { ?>
			<tr>
				<td><input type="checkbox" name="mids[]" value="<?=$row['mid']?>" /></td>
				<td><a href="<?=siteUrl('admin/meta/edit')?>?mid=<?=$row['mid']?>"><?=$row['name']?></a></td>
				<td><?=$row['alias']?></td>
				<td><span class="badge"><?=$row['count']?></span></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
</div>
<?php } ?>

<script type="text/javascript">
$("ul.dropdown-action > li a").click(function() {
	var action = $(this).data("action");

	if (action == "remove") {
		showDialog("删除分类同时将删除该分类下的所有文章及相关附件和数据，您可以先转移分类下的文章到其它分类中。<br>请确认接下来的操作！", [
			["转移到其它分类", function() {
				console.log("Transport");
				this.close();
			}],
			["直接删除", function() {
				console.log("Delete");
				this.close();
			}],
			["取消", null]
		], {
			verticalButtons: true
		});
	}
});
</script>
<?php $this->render('admin/footer'); ?>