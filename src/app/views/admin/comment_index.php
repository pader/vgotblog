<?php
/**
 * @var $comments array
 * @var $status mixed
 * @var $cid int
 */

use app\models\Comment;
use vgot\Web\Url;

$this->render('admin/header');

$statuses = [
	-1 => '全部',
	Comment::STATUS_PENDING => '待审核',
	Comment::STATUS_PUBLIC => '正常',
	Comment::STATUS_SPAM => '垃圾',
	Comment::STATUS_DELETED => '回收站'
];
?>
<style type="text/css">
.comment-item-check {cursor:pointer;}
</style>

<h3>评论</h3>

<div>
	<div class="pull-right filter-bar">
		<?php foreach ($statuses as $k => $statusText) { ?>
			<?php if ($status == $k) { ?>
				<span class="bg-primary"><?=$statusText?></span>
			<?php } else { ?>
				<a href="<?=Url::site(['admin/comment', 'status'=>$k, 'cid'=>$cid])?>"><?=$statusText?></a>
			<?php } ?>
			<?php if ($k !== -1) { ?> (<?=isset($statusCount[$k]) ? $statusCount[$k] : 0?>)<?php } ?>
			<?=($k != Comment::STATUS_DELETED ? '<span class="slash">|</span>' : '&nbsp;&nbsp;&nbsp;')?>
		<?php } ?>
	</div>
	<div class="input-group" style="width:110px;margin-bottom:8px;">
		<div class="input-group-addon">
			<input type="checkbox" id="checkAll" />
		</div>
		<div class="input-group-btn">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">选中项 <span class="caret"></span></button>
			<ul class="dropdown-menu dropdown-action" role="menu" id="actions">
				<?php foreach (array_diff_key($statuses, array_flip(['-1', $status])) as $k => $v) { ?>
					<li><a href="###" data-action="move" data-status="<?=$k?>">移动到<?=$v?></a></li>
				<?php } ?>
				<?php if ($status == Comment::STATUS_DELETED) { ?>
					<li><a href="###" data-action="delete">彻底删除</a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>

<div class="wrap-table">
	<?php if ($comments) { ?>
	<form action="<?=siteUrl('admin/comment/action')?>" method="post" id="listForm">
		<ul class="media-list comment-list">
			<?php foreach ($comments as $row) { ?>
			<li class="media comment-item">
				<div class="media-left comment-item-check"><input type="checkbox" name="cmtids[]" value="<?=$row['cmtid']?>" /></div>
				<div class="media-left"><img class="media-object img-rounded" src="<?=getAvatar($row)?>" /></div>
				<div class="media-body">
					<div class="media-heading">
						<small class="text-muted pull-right"><?=fmtime($row['created'])?></small>
						<?=$row['author']?>
						<?php if ($row['userSite']) { ?>
							<a href="<?=$row['userSite']?>" target="_blank" class="comment-item-outicon"><span class="glyphicon glyphicon-home"></span></a>
						<?php } ?>
						<?php if ($row['mail']) { ?>
							<a href="mailto:<?=$row['mail']?>" class="comment-item-outicon"><span class="glyphicon glyphicon-envelope"></span></a>
						<?php } ?>
						<small class="text-muted">
							发表在《<a href="<?=$row['url']?>#comment<?=$row['cmtid']?>" target="_blank"><?=mb_substr($row['title'], 0, 30, 'UTF-8')?></a>》
							[<?=$statuses[$row['status']]?>]
						</small>
					</div>
					<p><?=$row['content']?></p>
				</div>
			</li>
			<?php } ?>
		</ul>
		<input type="hidden" name="action" value="" id="sendAction" />
		<input type="hidden" name="status" value="" id="sendStatus">
	</form>
	<?=$pages?>
	<?php } else { ?>
		<p class="text-muted text-center">没有<?=$status!=-1?$statuses[$status]:''?>评论</p>
	<?php } ?>
</div>

<script type="text/javascript">
function itemChecked(checkbox) {
	const item = $(checkbox).closest(".comment-item");
	setTimeout(() => {
		if (checkbox.prop("checked")) {
			item.addClass("selected");
		} else {
			item.removeClass("selected");
		}
	}, 0);
}

function sendAction(action, status) {
	$("#sendAction").val(action);
	$("#sendStatus").val(status);
	$("#listForm").submit();
}

$("#checkAll").on("click", function() {
	$(".comment-item-check input:checkbox").prop("checked", this.checked);
	if (this.checked) {
		$(".comment-item").addClass("selected");
	} else {
		$(".comment-item").removeClass("selected");
	}
});

$(".comment-item-check").click(function(e) {
	const checkbox = $(this).find(":checkbox");
	if (checkbox.get(0) != e.target) {
		checkbox.prop("checked", !checkbox.prop("checked"));
	}
	itemChecked(checkbox);
});

$("#actions a[data-action]").click(function() {
	const action = $(this).data("action");
	switch (action) {
		case "move":
			const status = $(this).data("status");
			showDialog("确定移动评论吗？", [
				["确定", function() {
					sendAction(action, status);
					this.close();
				}],
				["取消", null]
			]);
			break;
		case "delete":
			showDialog("彻底删除评论后数据将无法恢复，确定吗？", [
				["确定", function() {
					sendAction(action, 0);
					this.close();
				}],
				["取消", null]
			]);
			break;
	}
});
</script>

<?php $this->render('admin/footer'); ?>