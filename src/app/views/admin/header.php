<?php
use vgot\Web\Url;
?>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title><?php if (isset($TITLE)) { echo $TITLE.' | '; } ?>Power by VgotBlog</title>
<meta name="robots" content="noindex, nofollow" />
<link type="text/css" href="<?=STATIC_URL?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<!--<link type="text/css" href="<?=STATIC_URL?>lib/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />-->
<link type="text/css" href="<?=STATIC_URL?>admin/base.css" rel="stylesheet" />
<script type="text/javascript" src="<?=STATIC_URL?>lib/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
var ROOT_URL = "<?=Url::base()?>";
var BASE_URL = "<?=rtrim(Url::site('admin'), '/').'/'?>";
var LOCATE = "<?=$LOCATE?>";
</script>
<script type="text/javascript" src="<?=STATIC_URL?>js/admin.js"></script>
<!--[if lt IE 9]>
<script src="http://apps.bdimg.com/libs/html5shiv/3.7/html5shiv.min.js"></script>
<script src="http://apps.bdimg.com/libs/respond.js/1.4.2/respond.js"></script>
<![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?=Url::site('admin')?>">VGOT BLOG</a>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li data-locate="index"><a href="<?=Url::site('admin')?>">控制台</a></li>
				<li class="dropdown" data-locate="content-write">
					<a href="<?=Url::site('admin/content/write')?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">撰写</a>
					<ul class="dropdown-menu" role="menu">
						<li data-locate="content-write"><a href="<?=Url::site('admin/content/write')?>">文章</a></li>
						<li data-locate="content-write/page"><a href="<?=Url::site('admin/content/write/page')?>">页面</a></li>
					</ul>
				</li>
				<li class="dropdown" data-locate="content!content-write attachment comment media user">
					<a href="<?=Url::site('admin/content')?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">内容</a>
					<ul class="dropdown-menu" role="menu">
						<li data-locate="content-index"><a href="<?=Url::site('admin/content')?>">文章</a></li>
						<li data-locate="content-page"><a href="<?=Url::site('admin/content/page')?>">页面</a></li>
						<li data-locate="attachment"><a href="<?=Url::site('admin/attachment')?>">附件</a></li>
						<li class="divider"></li>
						<li data-locate="comment"><a href="<?=Url::site('admin/comment')?>">评论</a></li>
						<li class="divider"></li>
						<li data-locate="meta-category"><a href="<?=Url::site('admin/meta/category')?>">分类</a></li>
						<li data-locate="meta-tag"><a href="<?=Url::site('admin/meta/tag')?>">标签</a></li>
						<li class="divider"></li>
						<li data-locate="user-index"><a href="<?=Url::site('admin/user')?>">用户</a></li>
					</ul>
				</li>
				<li data-locate="plugin"><a href="<?=Url::site('admin/plugin')?>">插件</a></li>
				<li class="dropdown" data-locate="option">
					<a href="<?=Url::site('admin/option/general')?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">设置</a>
					<ul class="dropdown-menu" role="menu">
						<li data-locate="option-general"><a href="<?=Url::site('admin/option/general')?>">基本</a></li>
						<li data-locate="option-url"><a href="<?=Url::site('admin/option/url')?>">链接地址</a></li>
						<li data-locate="option-nav"><a href="<?=Url::site('admin/option/nav')?>">导航</a></li>
						<li data-locate="option-security"><a href="<?=Url::site('admin/option/security')?>">安全</a></li>
						<li data-locate="option-performance"><a href="<?=Url::site('admin/option/performance')?>">性能</a></li>
					</ul>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="<?=Url::site('admin/user/myaccount')?>"><?=$_ENV['user']['username']?></a></li>
				<li><a href="<?=Url::base()?>" target="_blank">网站首页</a></li>
				<li><a href="<?=Url::site('login/logout')?>">退出</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container -->
</nav>

<div class="container container-body">