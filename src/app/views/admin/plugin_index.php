<?php
/**
 * @var array $plugins
 */
$this->render('admin/header');
?>

<h3>插件管理</h3>

<div class="wrap-table">
	<table class="table">
		<thead>
		<tr>
			<th>名称</th>
			<th>版本</th>
			<th>描述</th>
			<th>状态</th>
			<th>操作</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($plugins as $row) { ?>
			<tr>
				<td><?=$row['name']?></td>
				<td><?=$row['version']?></td>
				<td><?=$row['description']?></td>
				<td><?=$row['enabled'] ? '已启用' : '未启用'?></td>
				<td>
					<?php if ($row['enabled']) { ?>
						<a href="<?=siteUrl(['admin/plugin/enable', 'id'=>$row['id'], 'do'=>'disable'])?>">停用</a>
					<?php } else { ?>
						<a href="<?=siteUrl(['admin/plugin/enable', 'id'=>$row['id'], 'do'=>'enable'])?>">启用</a>
					<?php } ?>
					<?php if (!empty($row['with_options'])) { ?>
						&nbsp;&nbsp;<a href="<?=siteUrl(['admin/plugin/config', 'id'=>$row['id']])?>">配置</a>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
</div>

<?php $this->render('admin/footer'); ?>
