<?php $this->render('admin/header'); ?>

<h3>链接地址</h3>
<form role="form" class="form-option" action="<?=siteUrl('admin/option/save')?>" method="post">
	<div class="form-group">
		<label>地址重写功能</label>
		<div class="radio">
			<label><input type="radio" name="core[url_rewrite]" id="inputRewriteDisable" value="0" <?php if ($core['url_rewrite'] === '0') { ?>checked="checked" <?php } ?>/>不启用</label>&nbsp;&nbsp;
			<label><input type="radio" name="core[url_rewrite]" id="inputRewriteEnable" value="1" <?php if ($core['url_rewrite'] == '1') { ?>checked="checked" <?php } ?>/>启用</label>
		</div>
		<div class="help-block">启用地址重写后，网站的地址将不显示 php 文件的访问细节，使地址看上去完全是静态的。<br />
			启用重写必须在服务器中针对主机进行了 Rewrite 设置，请先检查是否支持并进行了正确的配置。</div>
	</div>
	<div class="form-group">
		<label for="inputBaseUrl">文章路径</label>
		<div class="radio">
			<?php
			$inList = false;
			foreach ($preset as $name => $val) { ?>
				<div class="radio-list"><label><input type="radio" name="core[url_post]" value="<?=$val?>" <?php if ($core['url_post'] == $val) { $inList = true; ?>checked <?php } ?>/><?=$name?> <code><?=$val?></code></label></div>
			<?php } ?>
			<div class="radio-list clearfix url-custom">
				<label class="radio-inline"><input type="radio" name="core[url_post]" id="radioUrlPostCustom" <?php if (!$inList) { ?>value="<?=$core['url_post']?>" checked <?php } ?>/>自定义</label>
				<input type="text" class="form-control" name="core[url_post_custom]" id="inputUrlPostCustom" value="<?=($inList ? '' : $core['url_post'])?>" />
			</div>
		</div>
		<div class="help-block">可用参数: {cid} 文章ID, {alias} 文章别名, {category} 分类, {year} 年, {month} 月, {day} 日。<br />
			自定义地址中，必须有 {cid} 或者 {alias} 至少一项。<br />一旦选择了某种路径风格建议不再修改它（除非明确知道修改会带来的结果）。</div>
	</div>
	<div class="form-group">
		<label for="inputDescription">独立页面路径</label>
		<input type="text" class="form-control" name="core[url_page]" id="inputUrlPage" value="<?=$core['url_page']?>" />
		<div class="help-block">可用参数: {cid} 页面ID, {alias} 页面别名，至少包含其中一项参数。</div>
	</div>
	<div class="form-group">
		<label for="inputKeywords">分类路径</label>
		<input type="text" class="form-control" name="core[url_category]" id="inputUrlCategory" value="<?=$core['url_category']?>" />
		<div class="help-block">可用参数: {mid} 分类ID, {alias} 分类别名，至少包含其中一项参数。</div>
	</div>
	<button type="submit" class="btn btn-primary">保存设置</button>
</form>

<script type="text/javascript">
var customRadio = $("#radioUrlPostCustom");

$("#inputUrlPostCustom").click(function() {
	customRadio.prop("checked", true);
}).blur(function() {
	customRadio.val($(this).val());
});
</script>

<?php $this->render('admin/footer'); ?>