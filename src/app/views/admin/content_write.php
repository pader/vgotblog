<?php

use app\models\Content;

$this->render('admin/header');
?>
<link type="text/css" rel="stylesheet" href="<?=STATIC_URL?>admin/tokenize2.min.css" />
<h3><?=$TITLE?><a href="<?=siteUrl('admin/content'.($type == 'page' ? '/'.$type : ''))?>" class="btn btn-link btn-xs navlink" role="button">返回<?=($type == 'post' ? '文章' : '页面')?>列表</a></h3>
<form role="form" action="<?=siteUrl('admin/content/save')?>" method="post" class="form-write">
	<?php if ($isEdit) { ?>
		<input type="hidden" name="cid" value="<?=$content['cid']?>" />
	<?php } else { ?>
		<input type="hidden" name="type" value="<?=$type?>" />
	<?php } ?>
	<div class="row">
		<div class="col-sm-9">
			<div class="form-group">
				<?php if ($draft) { ?>
					<p class="text-warning bg-warning" style="padding-left:7px;">
						<span class="glyphicon glyphicon-pencil"></span> 正在编辑的是 <?=fmtime($draft['modified'])?> 保存的草稿，您可以 <a href="###" onclick="sendAction('remove', <?=$draft['cid']?>);">删除草稿</a>。</p>
				<?php } ?>
				<input type="text" class="form-control" name="title" id="inputTitle" value="<?php if ($isEdit) echo $content['title']; ?>" placeholder="标题" />
				<div class="help-block"><?=$demoUrl?></div>
			</div>
			<div class="form-group">
				<textarea name="content" id="content" rows="10"><?php if ($isEdit) echo htmlspecialchars($content['content']); ?></textarea>
			</div>
			<div class="form-group clearfix">
				<div class="pull-right">
					<button type="submit" class="btn btn-default" name="action" value="save" style="margin-right:5px;">保存草稿</button>
					<button type="submit" class="btn btn-primary" name="action" value="publish"><?=($isEdit ? '更新' : '发布')?><?=($type == 'post' ? '文章' : '页面')?></button>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<ul class="nav nav-pills nav-justified">
				<li role="presentation" class="active"><a href="#tpOption" aria-controls="tpOption" role="tab" data-toggle="tab">选项参数</a></li>
				<li role="presentation"><a href="#tpAttachment" aria-controls="tpAttachment" role="tab" data-toggle="tab">附件 <span class="badge"><?=count($attachments)?></span></a></li>
			</ul>

			<div class="tab-content">

				<div class="tab-pane active" id="tpOption">
					<?php if ($type == Content::TYPE_POST) { ?>
					<div class="form-group">
						<label for="inputCategory">分类</label>
						<div class="panel panel-default">
							<?php foreach ($categories as $row) { ?>
								<div class="radio">
									<label><input name="mid[]" type="radio" value="<?=$row['mid']?>" <?php if ($isEdit && $cateId == $row['mid']) echo 'checked '; ?>/> <?=$row['name']?></label>
								</div>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label for="inputTags">标签</label>
						<select class="form-control" id="inputTags" multiple></select>
						<input type="hidden" name="tags" id="inputTagsValue" value="<?php if ($isEdit) echo $tags; ?>" />
					</div>
					<?php } ?>
					<div class="form-group">
						<label for="inputCreated">发布日期</label>
						<input type="text" class="form-control" name="created" id="inputCreated" value="<?php if ($isEdit) echo vdate('Y-n-j H:i:s', $content['created']); ?>" placeholder="留空使用当前时间" />
					</div>
					<div class="form-group">
						<label>选项</label>
						<div class="checkbox">
							<label><input type="checkbox" name="allowComment" value="1" <?php if (!$isEdit || $content['allowComment'] == 1) echo 'checked '; ?>/> 允许评论</label>
						</div>
					</div>
					<div class="form-group form-inline">
						<label for="inputPublic">　公开度　</label>
						<select class="form-control" id="inputPublic" name="status">
						<?php foreach ($statusText as $statusVal => $statusName) { ?>
							<option value="<?=$statusVal?>"<?php if ($status == $statusVal) { echo ' selected'; } ?>><?=$statusName?></option>
						<?php } ?>
						</select>
					</div>
					<?php if ($type == Content::TYPE_POST) { ?>
					<div class="form-group form-inline">
						<label for="inputPassword">访问密码　</label>
						<input type="password" class="form-control" name="password" id="inputPassword" value="<?php if ($isEdit) echo $content['password']; ?>" placeholder="留空不需要密码" autocomplete="off" />
					</div>
					<?php } ?>
				</div>

				<div class="tab-pane" id="tpAttachment">
					<div class="panel panel-default attachup-panel" id="attachDragPut">
						<div class="text-center text-muted attachup-box">
							<p class="attachup-box-p">将文件拖放到此处</p>
							<p class="attachup-box-p">或者 <a href="##" class="attachup-upload" id="attachUpload" role="button">选择文件</a> 上传</p>
							<p class="attachup-box-pdrop">松开鼠标开始上传</p>
						</div>
					</div>
					<div class="panel panel-default attachup-list" id="attachList">
					<?php if ($attachments) { foreach ($attachments as $row) { ?>
						<div class="attachup-list-element" data-aid="<?=$row['aid']?>">
							<a href="###" title="点击插入到文章" class="attachup-insert">
								<span class="glyphicon glyphicon-paperclip" title="<?=$row['filename']?>"></span>
								<span class="attachup-name"><script>document.write(filename("<?=$row['filename']?>"));</script></span>
							</a>
							<span class="attachup-ft"><script>document.write(filesize(<?=$row['filesize']?>));</script>
								<a href="###" title="改名" class="attachup-rename"><span class="glyphicon glyphicon-edit"></span></a>
								<a href="###" title="删除" class="attachup-remove"><span class="glyphicon glyphicon-remove"></span></a>
							</span>
						</div>
					<?php } } else {?>
						<div class="text-muted text-center attachup-none">没有附件</div>
					<?php } ?>
					</div>
				</div>

			</div>
		</div>
	</div>
</form>

<form action="<?=siteUrl('admin/content/action')?>" method="post" id="actionForm">
	<input type="hidden" name="action" value="" id="actionAction" />
	<input type="hidden" name="cids[]" value="<?php if ($isEdit) echo $content['cid']; ?>" id="actionCids" />
</form>

<script type="text/javascript" src="<?=STATIC_URL?>lib/ueditor/config.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>lib/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript">
var ue = UE.getEditor('content');
/*
ue.addInputRule(function(root){
	root.traversal(function(node){
		if (node.type == 'text' && /\[attachment\=\d+\]/.test(node.data)) {
			//var hr = UE.uNode.createElement('<hr class="pagebreak" noshade="noshade" size="5" style="-webkit-user-select: none;">');
			//node.parentNode.insertBefore(hr,node);
			//node.parentNode.removeChild(node)
			node.data = "[This is attachment]";
		}
	})
});

//ue.addOutputRule();
*/

var alias = $("#alias");
var aliasCopy = alias.next(".form-alias-copy");

aliasCopy.height(alias.height());

alias.bind("input propertychange", function() {
	var val = alias.val();
	aliasCopy.text(val.length > 0 ? val : '');
});
</script>
<script type="text/javascript" src="<?=STATIC_URL?>lib/plupload/moxie.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>lib/plupload/plupload.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>admin/tokenize2.min.js"></script>
<script type="text/javascript">
var attachList = $(".attachup-list");
var attachDrop = $("#attachDragPut");

attachDrop.bind("dragenter", function (e) {
	$(this).addClass("attachup-drop");
}).bind("drop dragleave mouseleave", function(e) {
	if (e.type == "dragleave" && e.target != attachDrop.get(0)) {
		return;
	}
	$(this).removeClass("attachup-drop");
});

var uploader = new plupload.Uploader({
	browse_button: "attachUpload",
	runtimes: "html5,flash,html4",
	url: "<?=siteUrl('admin/content/upload')?><?php if ($isEdit) echo '?cid='.$content['cid']; ?>",
	drop_element: "attachDragPut",
	flash_swf_url: "<?=STATIC_URL?>lib/plupload/Moxie.swf",
	init: {
		FilesAdded: function(up, files) {
			attachList.find(".attachup-none").remove();

			var doc = "";
			$.each(files, function() {
				doc += '<div class="attachup-list-element" id="' + this.id + '">'
					+ '<a href="###" title="点击插入到文章" class="attachup-insert">'
					+ '<span class="glyphicon glyphicon-paperclip" title="' + this.name + '"></span>'
					+ '<span class="attachup-name">' + filename(this.name) + '</span></a>'
					+ '<span class="attachup-ft">'
					+ '<span class="pull-right attachup-list-element-status">待上传</span>' + filesize(this.origSize)
					+ '<a href="###" title="改名" class="attachup-rename hidden"><span class="glyphicon glyphicon-edit"></span></a>'
					+ '<a href="###" title="取消上传" class="attachup-remove"><span class="glyphicon glyphicon-remove"></span></a>'
					+ '</span></div>';
			});

			attachList.prepend(doc);
			uploader.start();
		},
		UploadProgress: function(up, file) {
			$("#" + file.id + " .attachup-list-element-status").html("上传中 - " + file.percent + "%");
		},
		FileUploaded: function(up, file, responseObject) {
			var elem = $("#" + file.id);
			var rt = $.parseJSON(responseObject.response);

			if (rt.status) {
				elem.attr("data-aid", rt.data.aid).find(".attachup-rename").removeClass("hidden");
				elem.find(".attachup-remove").attr("title", "删除");
				elem.find(".attachup-list-element-status").html("完成");
				//elem.append('<input type="hidden" name="attachment[]" value="' + rt.data.aid + '" />');
				attachData[rt.data.aid] = {aid:rt.data.aid, filename:file.name, url:rt.data.url, type:rt.data.type};
			} else {
				elem.find(".attachup-list-element-status").html("失败");
				alert("文件 " + file.name + " 上传失败：" + rt.message);
			}
		},
		Error: function(up, err) {
			alert("文件 " + err.file.name + " 上传失败：" + err.message + " [STATUS " + err.status + "]");
		}
	}
});

function sendAction(action, cids) {
	if (cids) {
		$("#actionCids").val(cids);
	}
	$("#actionAction").val(action);
	$("#actionForm").submit();
}

uploader.init();

var attachData = <?=$attachData?>;
var attachList = $("#attachList");

attachList.on("click", ".attachup-list-element[data-aid] > .attachup-insert", function() {
	var aid = $(this).closest(".attachup-list-element").attr("data-aid");
	var attach = attachData[aid];

	if ($.inArray(attach.type.toLowerCase(), ["jpg", "jpeg", "gif", "png", "bmp"]) != -1) {
		ue.execCommand("insertimage", {src:attach.url, title:attach.filename});
	} else {
		console.log({url:attach.url, title:attach.filename, target:"_blank"});
		ue.execCommand("link", {href:attach.url, title:attach.filename, textValue:attach.filename, target:"_blank"});
	}
}).on("click", "a.attachup-rename", function() {
	if (!checkAttachReady(this)) {
		alert("该附件未成功上传，不可改名！");
		return;
	}

	alert("Rename");
}).on("click", "a.attachup-remove", function() {
	var p = $(this).closest(".attachup-list-element");
	if (!checkAttachReady(this)) {
		p.remove();
		return;
	}

	var name = p.find(".attachup-name").prev().attr("title");

	if (!confirm("删除附件 " + name + "\n\n这将导致文章中相应的附件图片或下载链接等无法使用，并且不可恢复。确定要删除吗？")) {
		return;
	}

	var aid = p.attr("data-aid");

	$.post(BASE_URL + "attachment/remove", {aid:aid}, function(d) {
		if (d.status) {
			p.remove();
			if (attachList.find("> .attachup-list-element").length == 0) {
				var noneHtml = '<div class="text-muted text-center attachup-none">没有附件</div>';
				attachList.html(noneHtml);
			}
		} else {
			alert(d.message);
		}
	}, "json");
});

function checkAttachReady(obj) {
	return $(obj).closest(".attachup-list-element").is("[data-aid]");
}

// Tags
(function() {
	var tagsInput = $("#inputTags");
	var tagsReal = $("#inputTagsValue");
	var tagsToken = tagsInput.tokenize2({
		placeholder: "输入逗号分隔多个标签",
		tokensAllowCustom: true,
		dropdownMaxItems: 20,
		dataSource: BASE_URL + "meta/tokenize",
		debounce: 200,
		delimiter: [',', '，']
	});

	var tagsValue = tagsReal.val().split(", ");
	if (tagsValue.length > 0) {
		tagsValue.forEach(t => {
			tagsToken.trigger('tokenize:tokens:add', t);
		});
	}

	tagsInput.on('tokenize:tokens:added tokenize:tokens:remove', function(e, value) {
		var tags = tagsInput.val();
		tagsReal.val(tags ? tags.join(",") : "");
	});
})();

$("#inputPassword").focus(function() {
	$(this).attr("type", "text");
}).blur(function() {
	$(this).attr("type", "password");
});
</script>

<?php $this->render('admin/footer'); ?>