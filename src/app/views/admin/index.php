<?php $this->render('admin/header'); ?>

<h3>博客概览</h3>
<h3>运行环境</h3>
<div class="row">
	<div class="col-sm-2">操作系统</div>
	<div class="col-sm-4"><?=PHP_OS?></div>
	<div class="col-sm-2">服务器软件</div>
	<div class="col-sm-4"><?=$software?></div>
	<div class="col-sm-2">PHP 版本</div>
	<div class="col-sm-4">PHP/<?=PHP_VERSION?></div>
	<div class="col-sm-2">OpCache 状态</div>
	<div class="col-sm-4"><?php if ($opcache) { echo $opcache; } else { ?>未启用<?php } ?></div>
	<div class="col-sm-2">MySQL 版本</div>
	<div class="col-sm-4">MySQL/<?=$mysqlVersion?></div>
	<div class="col-sm-2">MySQL 已运行</div>
	<div class="col-sm-4"><?=$mysqlUptime?></div>
</div>

<?php $this->render('admin/footer'); ?>