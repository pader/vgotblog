<?php
/**
 * @var $TITLE string
 * @var $isEdit bool
 * @var $meta array
 */
$this->render('admin/header');
?>

<h3><?=$TITLE?></h3>
<form role="form" class="form-option" action="?_=<?=now()?>" method="post">
	<div class="form-group">
		<label for="inputName">分类名称</label>
		<input type="text" class="form-control" name="name" id="inputName" value="<?php if ($isEdit) echo $meta['name']; ?>" />
	</div>
	<div class="form-group">
		<label for="inputAlias">分类别名</label>
		<input type="text" class="form-control" name="alias" id="inputAlias" value="<?php if ($isEdit) echo $meta['alias']; ?>" />
	</div>
	<div class="form-group">
		<label for="inputDesc">分类描述</label>
		<textarea class="form-control" name="description" id="inputDesc" rows="2"><?php if ($isEdit) echo htmlspecialchars($meta['description']); ?></textarea>
	</div>
	<?php if ($isEdit) { ?><input type="hidden" name="mid" value="<?=$meta['mid']?>" /><?php } ?>
	<button type="submit" class="btn btn-primary"><?php if ($isEdit) { ?>修改<?php } else { ?>新建<?php } ?>分类</button>
</form>

<?php $this->render('admin/footer'); ?>