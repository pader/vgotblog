<?php $this->render('admin/header'); ?>

<h3>文章  <a href="<?=siteUrl('admin/content/write')?>" class="btn btn-link btn-xs" style="margin-left:20px;" role="button">+ 新文章</a></h3>

<?php if ($list) { ?>
<div class="input-group" style="width:110px;margin-bottom:8px;">
	<div class="input-group-addon">
		<input type="checkbox" id="checkAll" />
	</div>
	<div class="input-group-btn">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">选中项 <span class="caret"></span></button>
		<ul class="dropdown-menu dropdown-action" role="menu">
			<li><a href="###" data-action="remove">删除</a></li>
		</ul>
	</div>
</div>

<form action="<?=siteUrl('admin/content/action')?>" method="post" id="listForm">
	<input type="hidden" name="action" value="" id="sendAction" />
	<div class="wrap-table">
		<table class="table content-list">
			<thead>
			<tr>
				<th>　</th>
				<th>标题</th>
				<th>浏览</th>
				<th style="white-space: nowrap;">评论</th>
				<th>作者</th>
				<th>分类</th>
				<th>时间</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($list as $row) { ?>
				<tr>
					<td><input type="checkbox" name="cids[]" value="<?=$row['cid']?>" /></td>
					<td>
						<a href="<?=siteUrl('admin/content/write')?>?cid=<?=$row['cid']?>"><?=$row['title']?></a>
						<?php if (isset($statusText[$row['status']])) { ?>
							<small class="text-muted"><?=$statusText[$row['status']]?></small>
						<?php } elseif ($row['password']) { ?>
							<small class="text-muted">密码保护</small>
						<?php } ?>
						<a href="<?=$row['url']?>" class="content-list-oplink" title="打开此文章" target="_blank"><span class="glyphicon glyphicon-new-window"></span></a>
					</td>
					<td style="white-space: nowrap;"><?=$row['viewNum']?></td>
					<td style="white-space: nowrap;"><a href="<?=siteUrl(['admin/comment', 'cid'=>$row['cid']])?>" target="_blank"><?=$row['commentNum']?></a></td>
					<td><a href="?uid=<?=$row['uid']?>" title="<?=$row['username']?>"><?=$row['nickname']?></a></td>
					<td style="white-space: nowrap;"><?php if ($row['mid']) { ?><a href="?mid=<?=$row['mid']?>"><?=$row['category']?></a><?php } else { ?><i>无</i><?php } ?></td>
					<td style="white-space: nowrap;"><?=vdate('Y-m-d H:i:s', $row['created'])?></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		<?=$pages?>
	</div>
</form>
<?php } ?>

<script type="text/javascript">
function sendAction(action) {
	$("#sendAction").val(action);
	$("#listForm").submit();
}

$("#checkAll").on("click", function() {
	$("table.content-list td:first-child input:checkbox").prop("checked", this.checked);
});

$("ul.dropdown-action a").on("click", function() {
	var action = $(this).attr("data-action");

	if (action == "remove") {
		showDialog("删除文章将同时删除该文章下的所有附件、评论和相关数据，确定吗？", [
			["确定", function() {
				sendAction(action);
				this.close();
			}],
			["取消", null]
		]);
	} else {
		sendAction(action);
	}
});
</script>
<?php $this->render('admin/footer'); ?>