<?php
/**
 * @var $data array
 */
$this->render('admin/header');
?>

<h3>性能设置</h3>

<form role="form" class="form-option" action="<?=siteUrl('admin/option/save')?>" method="post">
	<div class="form-group">
		<label for="cacheAdpater">缓存适配器</label>
		<select id="cacheAdpater" name="core[cache_adapter]" class="form-control">
			<option value="file"<?php if ($data['cache_adapter'] == 'file') { ?> selected<?php } ?>>文件缓存</option>
			<option value="db"<?php if ($data['cache_adapter'] == 'db') { ?> selected<?php } ?>>数据表缓存</option>
			<option value="memcache"<?php if ($data['cache_adapter'] == 'memcache') { ?> selected<?php } ?>>Memcache</option>
			<option value="redis"<?php if ($data['cache_adapter'] == 'redis') { ?> selected<?php } ?>>Redis</option>
		</select>
	</div>

	<div class="panel panel-default cache-options hide" id="adpater_file">
		<div class="panel-heading">文件缓存配置</div>
		<div class="panel-body form-horizontal">
			<div class="form-group">
				<label for="inputFileCacheDirLevel" class="col-sm-3 control-label">目录层级</label>
				<div class="col-sm-9">
					<input type="number" class="form-control" name="core[cache_file][dirLevel]" value="1" id="inputFileCacheDirLevel">
				</div>
			</div>
			<div class="form-group">
				<label for="inputFileCacheGc" class="col-sm-3 control-label">回收概率</label>
				<div class="col-sm-9">
					<div class="input-group">
						<input type="number" class="form-control" name="core[cache_file][gcProbability]" id="inputFileCacheGc" value="<?=$data['cache_file']['gcProbability']?>" placeholder="写入时机删除过期缓存文件的概率">
						<div class="input-group-addon">/1000000</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="inputFileCacheMemory" class="col-sm-3 control-label">运行时缓存</label>
				<div class="col-sm-9">
					<div class="checkbox">
						<label><input id="inputFileCacheMemory" type="checkbox" name="core[cache_file][cacheInMemory]"<?php if (!empty($data['cache_file']['cacheInMemory'])) { ?> checked<?php } ?> value="1"></label>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default cache-options hide" id="adpater_db">
		<div class="panel-heading">数据库缓存配置</div>
		<div class="panel-body form-horizontal">
			<div class="form-group">
				<label for="inputDbCacheGc" class="col-sm-3 control-label">回收概率</label>
				<div class="col-sm-9">
					<div class="input-group">
						<input type="number" class="form-control" name="core[cache_db][gcProbability]" id="inputDbCacheGc" value="<?=$data['cache_db']['gcProbability']?>" placeholder="写入时机删除过期缓存记录数据的概率">
						<div class="input-group-addon">/1000000</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default cache-options hide" id="adpater_memcache">
		<div class="panel-heading">Memcache 配置</div>
		<div class="panel-body form-horizontal">
			<div class="form-group">
				<label for="inputMemcacheHost" class="col-sm-3 control-label">服务主机地址</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="core[cache_memcache][host]" id="inputMemcacheHost" value="<?=$data['cache_memcache']['host']?>" placeholder="Memcache主机地址">
				</div>
			</div>
			<div class="form-group">
				<label for="inputMemcachePort" class="col-sm-3 control-label">服务端口</label>
				<div class="col-sm-9">
					<input type="number" class="form-control" name="core[cache_memcache][port]" id="inputMemcachePort" value="<?=$data['cache_memcache']['port']?>" placeholder="Memcache服务端口，默认11211">
				</div>
			</div>
			<div class="form-group">
				<label for="inputMemcachePconnect" class="col-sm-3 control-label">持久连接</label>
				<div class="col-sm-9">
					<div class="checkbox">
						<label><input id="inputMemcachePconnect" name="core[cache_memcache][pconnect]" type="checkbox" name="cache_memcache[pconnect]"<?php if (!empty($data['cache_memcache']['pconnect'])) { ?> checked<?php } ?> value="1"></label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="inputMemcachePrefix" class="col-sm-3 control-label">键名前缀</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="core[cache_memcache][keyPrefix]" id="inputMemcachePrefix" value="<?=$data['cache_memcache']['keyPrefix']?>">
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default cache-options hide" id="adpater_redis">
		<div class="panel-heading">Redis 配置</div>
		<div class="panel-body form-horizontal">
			<div class="form-group">
				<label for="inputRedisHost" class="col-sm-3 control-label">服务主机地址</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="core[cache_redis][host]" id="inputRedisHost" value="<?=$data['cache_redis']['host']?>" placeholder="Redis主机地址">
				</div>
			</div>
			<div class="form-group">
				<label for="inputRedisPort" class="col-sm-3 control-label">服务端口</label>
				<div class="col-sm-9">
					<input type="number" class="form-control" name="core[cache_redis][port]" id="inputRedisPort" value="<?=$data['cache_redis']['port']?>" placeholder="Redis服务端口，默认6379">
				</div>
			</div>
			<div class="form-group">
				<label for="inputRedisPass" class="col-sm-3 control-label">连接密码</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="core[cache_redis][password]" id="inputRedisPass" value="<?=$data['cache_redis']['password']?>" placeholder="Redis连接密码">
				</div>
			</div>
			<div class="form-group">
				<label for="inputRedisPconnect" class="col-sm-3 control-label">持久连接</label>
				<div class="col-sm-9">
					<div class="checkbox">
						<label><input id="inputRedisPconnect" name="core[cache_redis][pconnect]" type="checkbox" name="cache_redis[pconnect]"<?php if (!empty($data['cache_redis']['pconnect'])) { ?> checked<?php } ?> value="1"></label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="inputRedisDatabase" class="col-sm-3 control-label">数据库编号</label>
				<div class="col-sm-9">
					<input type="number" class="form-control" name="core[cache_redis][database]" id="inputRedisDatabase" value="<?=$data['cache_redis']['database']?>">
				</div>
			</div>
			<div class="form-group">
				<label for="inputRedisPrefix" class="col-sm-3 control-label">键名前缀</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="core[cache_redis][keyPrefix]" id="inputRedisPrefix" value="<?=$data['cache_redis']['keyPrefix']?>">
				</div>
			</div>
		</div>
	</div>

	<button type="submit" class="btn btn-primary">保存设置</button>
</form>

<script type="text/javascript">
$('#cacheAdpater').change(function() {
	var adpater = $(this).val();
	var panels = $("div.cache-options");
	var id = '#adpater_' + adpater;
	panels.not(id).addClass('hide');
	$(id).removeClass('hide');
}).trigger('change');
</script>

<?php $this->render('admin/footer'); ?>