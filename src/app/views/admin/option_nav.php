<?php $this->render('admin/header'); ?>

<h3>导航设置</h3>

<div class="wrap-table">
	<form role="form" action="<?=siteUrl('admin/option/navdo')?>" method="post" id="listForm">
		<input type="hidden" name="action" value="sort" id="listFormAction" />
		<table class="table" id="navs">
			<thead>
			<tr>
				<th>　</th>
				<th>导航名称</th>
				<th>类型</th>
				<th>排序</th>
				<th>可见</th>
				<th>地址</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($navs as $row) { ?>
				<tr>
					<td><input type="checkbox" name="ids[]" value="<?=$row['id']?>" <?php if ($row['type'] == 'system') { ?>disabled <?php }?>/></td>
					<td><a href="<?=siteUrl('admin/option/editNav/'.$row['id'])?>"><?=$row['name']?></a></td>
					<td><?php if ($row['type'] == 'system') { echo '系统'; } elseif ($row['type'] == 'page') { echo '页面'; } else { echo '自定义'; } ?></td>
					<td><input type="number" name="sort[<?=$row['id']?>]" value="<?=$row['sort']?>" class="form-control input-sm" style="width:80px;text-align:center;" /></td>
					<td><a href="<?=siteUrl('admin/option/navdo')?>?action=visible&id=<?=$row['id']?>&visible=<?=$row['visible'] ? '0' : '1'?>" title="点击<?=$row['visible'] ? '隐藏' : '显示'?>该导航" disabled><?=$row['visible'] ? '显示' : '隐藏'?></a></td>
					<td><?=$row['url']?><a href="<?=($row['url'] ? $row['url'] : siteUrl(''))?>" class="content-list-oplink" title="在新窗口中打开" target="_blank"><span class="glyphicon glyphicon-new-window"></span></a></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>

		<div class="clearfix">
			<div class="input-group pull-left" style="width:110px;margin-right:20px;">
				<div class="input-group-addon">
					<input type="checkbox" id="checkAll" />
				</div>
				<div class="input-group-btn">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">选中项 <span class="caret"></span></button>
					<ul class="dropdown-menu dropdownAction" role="menu">
						<li><a href="###" data-action="remove">删除</a></li>
					</ul>
				</div>
			</div>

			<div class="input-group pull-left">
				<button type="button" class="btn btn-primary" onclick="sendAction('sort');">应用设置</button>
			</div>
		</div>
	</form>
</div>

<div class="wrap-table navadd">
	<h4 class="text-center">添加导航</h4>

	<div class="row navadd-row">
		<div class="col-sm-4 navadd-custom">
			<h5>自定义</h5>
			<form class="form-horizontal" action="<?=siteUrl('admin/option/navdo')?>" method="post">
				<input type="hidden" name="action" value="add" />
				<input type="hidden" name="type" value="custom" />
				<div class="form-group form-group-sm">
					<span class="col-sm-2 control-label">名称</span>
					<div class="col-sm-6"><input type="text" class="form-control" name="name" /></div>
				</div>
				<div class="form-group form-group-sm">
					<span class="col-sm-2 control-label">地址</span>
					<div class="col-sm-10"><input type="text" class="form-control" name="url" placeholder="完整的绝对地址" /></div>
				</div>
				<div class="form-group form-group-sm">
					<span class="col-sm-2 control-label">排序</span>
					<div class="col-sm-3"><input type="text" class="form-control" name="sort" value="0" /></div>
				</div>
				<div class="form-group form-group-sm">
					<div class="col-sm-offset-2 col-sm-10">
						<div class="checkbox">
							<label><input type="checkbox" name="newwindow" value="1" /> 新窗口打开</label>
						</div>
					</div>
				</div>
				<div class="form-group form-group-sm">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default btn-sm">添加</button>
					</div>
				</div>
			</form>
		</div>

		<div class="col-sm-4">
			<h5>分类</h5>
			<form action="<?=siteUrl('admin/option/navdo')?>" method="post">
				<input type="hidden" name="action" value="add" />
				<input type="hidden" name="type" value="category" />
				<ul class="list-unstyled ">
					<?php foreach (\app\services\MetaService::getCategories() as $row) { ?>
						<li class="checkbox"><label><input type="checkbox" name="mids[]" value="<?=$row['mid']?>" /> <?=$row['name']?></label></li>
					<?php } ?>
				</ul>
				<button class="btn btn-default btn-sm" type="submit">添加</button>
			</form>
		</div>

		<div class="col-sm-4">
			<h5>页面</h5>
			<form action="<?=siteUrl('admin/option/navdo')?>" method="post">
				<input type="hidden" name="action" value="add" />
				<input type="hidden" name="type" value="page" />
				<ul class="list-unstyled">
					<?php foreach ($pages as $row) { ?>
					<li class="checkbox"><label><input type="checkbox" name="cids[]" value="<?=$row['cid']?>" /> <?=$row['title']?></label></li>
					<?php } ?>
				</ul>
				<button class="btn btn-default btn-sm" type="submit">添加</button>
			</form>
		</div>

	</div> <!--end .navadd-row-->
</div> <!--end .navadd-->

<script type="text/javascript">
function sendAction(action) {
	$("#listFormAction").val(action);
	$("#listForm").submit();
}

$("#checkAll").on("click", function() {
	$("#navs td:first-child input:checkbox:not(:disabled)").prop("checked", this.checked);
});

$("ul.dropdownAction a").on("click", function() {
	var action = $(this).attr("data-action");

	if (action == "remove") {
		showDialog("确定要删除指定的导航吗？", [
			["确定", function() {
				sendAction(action);
				this.close();
			}],
			["取消", null]
		]);
	} else {
		sendAction(action);
	}
});
</script>


<?php $this->render('admin/footer'); ?>