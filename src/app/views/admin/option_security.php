<?php $this->render('admin/header'); ?>

<h3>安全设置</h3>

<form role="form" class="form-option" action="<?=siteUrl('admin/option/save')?>" method="post">
	<div class="form-group">
		<label for="inputUploadAllowExts">允许上传的文件类型</label>
		<input type="text" class="form-control" name="setting[upload_allow_exts]" id="inputUploadAllowExts" value="<?=$option['upload_allow_exts']?>" />
		<p class="help-block">输入允许的文件后缀，多个用 “|” 分隔。</p>
	</div>
	<div class="form-group">
		<label for="inputUploadAllowMaxsize">允许上传的单个文件大小上限</label>
		<input type="text" class="form-control" name="setting[upload_allow_maxsize]" id="inputUploadAllowMaxsize" value="<?=$option['upload_allow_maxsize']?>" />
		<p class="help-block">以 KB 为单位。</p>
	</div>
	<div class="form-group">
		<label>附件保护</label>
		<div class="checkbox">
			<label><input type="checkbox" name="setting[protect_attachment_url]" value="1"<?php if ($option['protect_attachment_url']) { ?> checked<?php } ?> /> 启用附件地址保护</label>
		</div>
		<p class="help-block">暂不支持此功能</p>
	</div>
	<div class="form-group">
		<label>匿名评论</label>
		<div class="checkbox">
			<label><input type="checkbox" name="setting[comment_email_require]" value="1"<?php if ($option['comment_email_require']) { ?> checked<?php } ?> /> 必须输入邮箱</label>
		</div>
	</div>
	<button type="submit" class="btn btn-primary">保存设置</button>
</form>

<?php $this->render('admin/footer'); ?>