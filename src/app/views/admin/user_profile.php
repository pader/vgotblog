<?php $this->render('admin/header'); ?>

<h3><?php if ($isEdit) { ?>修改用户信息<?php } else { ?>新增用户<?php } ?><a href="<?=siteUrl('admin/user').'?p='.$page?>" class="btn btn-link btn-xs navlink" role="button">返回用户列表</a></h3>

<form role="form" class="form-option" action="?_=<?=now()?>" method="post" name="userProfile">
	<div class="form-group">
		<label for="inputName">用户名</label>
		<input type="text" class="form-control" name="name" id="inputName" value="<?php if ($isEdit) { echo $user['username']; } ?>"<?php if ($isEdit) { ?> disabled<?php } ?> />
	</div>
	<div class="form-group">
		<label for="inputAlias">电子邮箱</label>
		<input type="text" class="form-control" name="mail" id="inputAlias" value="<?php if ($isEdit) { echo $user['mail']; } ?>" />
	</div>
	<div class="form-group">
		<label for="inputAlias">密码</label>
		<input type="password" class="form-control" name="password" id="inputPassword" value=""<?php if ($isEdit) { ?> placeholder="不修改密码请留空"<?php } ?> />
	</div>
	<div class="form-group">
		<label for="inputAlias">确认密码</label>
		<input type="password" class="form-control" name="passwordConfirm" id="inputPwdcnf" value="" placeholder="请输入相同的密码" />
	</div>
	<div class="form-group">
		<label for="inputGroup">组</label>
		<div class="row">
			<div class="col-xs-3">
				<select name="group" class="form-control">
					<option value="visitor"<?php if (!$isEdit || $user['group'] == 'visitor') { ?> selected="selected"<?php } ?>>访客</option>
					<option value="writer"<?php if ($isEdit && $user['group'] == 'writer') { ?> selected="selected"<?php } ?>>作者/编辑</option>
					<option value="admin"<?php if ($isEdit && $user['group'] == 'admin') { ?> selected="selected"<?php } ?>>管理员</option>
				</select>
			</div>
		</div>
		<!--
		<div class="radio">
			<label><input type="radio" name="group" id="inputGroupVisitor" value="visitor"<?php if (!$isEdit || $user['group'] == 'visitor') { ?> checked="checked"<?php } ?> />访客</label>&nbsp;&nbsp;
			<label><input type="radio" name="group" id="inputGroupWrite" value="writer"<?php if ($isEdit && $user['group'] == 'writer') { ?> checked="checked"<?php } ?> />作者/编辑</label>&nbsp;&nbsp;
			<label><input type="radio" name="group" id="inputGroupAdmin" value="admin"<?php if ($isEdit && $user['group'] == 'admin') { ?> checked="checked"<?php } ?> />管理员</label>
		</div>
		-->
		<div class="help-block">作者/编辑组可以新建和修改自己的文章等数据。</div>
	</div>
	<div class="form-group">
		<label for="inputStatus">状态</label>
		<div class="row">
			<div class="col-xs-3">
				<select name="status" class="form-control">
					<option value="normal"<?php if (!$isEdit || $user['status'] == 'normal') { ?> selected="selected"<?php } ?>>正常</option>
					<option value="ban"<?php if ($isEdit && $user['status'] == 'ban') { ?> selected="selected"<?php } ?>>禁言</option>
					<option value="disabled"<?php if ($isEdit && $user['status'] == 'disabled') { ?> selected="selected"<?php } ?>>禁用</option>
				</select>
			</div>
		</div>
		<!--
		<div class="radio">
			<label><input type="radio" name="status" id="inputGroupVisitor" value="normal"<?php if (!$isEdit || $user['status'] == 'normal') { ?> checked="checked"<?php } ?> />正常</label>&nbsp;&nbsp;
			<label><input type="radio" name="status" id="inputGroupWrite" value="ban"<?php if ($isEdit && $user['status'] == 'ban') { ?> checked="checked"<?php } ?> />禁言</label>&nbsp;&nbsp;
			<label><input type="radio" name="status" id="inputGroupAdmin" value="disabled"<?php if ($isEdit && $user['status'] == 'disabled') { ?> checked="checked"<?php } ?> />禁用</label>
		</div>
		-->
		<span class="help-block">禁言代表该用户无法进行评论和发表文章等，禁用则代表该用户无法使用任何功能。</span>
	</div>
	<div class="form-group">
		<label class="control-label" for="inputNick">昵称</label>
		<input type="text" class="form-control" name="nickname" id="inputNick" value="<?php if ($isEdit) { echo htmlspecialchars($user['nickname']); } ?>" />
	</div>
	<div class="form-group">
		<label for="inputUrl">链接</label>
		<input type="text" class="form-control" name="url" id="inputUrl" value="<?php if ($isEdit) { echo htmlspecialchars($user['url']); } ?>" />
	</div>
	<div class="form-group">
		<label for="inputDesc">个人简介</label>
		<textarea class="form-control" name="description" id="inputDesc" rows="2"><?php if ($isEdit) { echo htmlspecialchars($user['description']); } ?></textarea>
	</div>
	<button type="submit" class="btn btn-primary">修改用户</button>
</form>

<script type="text/javascript" src="<?=STATIC_URL?>lib/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>lib/jquery-validation/localization/messages_zh.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>js/jquery.validator.defaults.js"></script>
<script type="text/javascript">
var form = document.forms["userProfile"];

var validate = $(form).validate({
	rules: {
		mail: {
			required: true,
			email: true
		},
		passwordConfirm: {
			equalTo: form.password
		},
		nickname: {
			required: true,
			minlength: 2
		}
	},
	messages: {
		passwordConfirm: {
			equalTo: "请输入相同的密码"
		}
	},
	submitHandler: function() {
		console.log(arguments);
		return false;
	}
});
</script>
<?php $this->render('admin/footer'); ?>