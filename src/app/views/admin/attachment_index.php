<?php $this->render('admin/header'); ?>

<h3>附件</h3>

<div class="input-group" style="width:110px;margin-bottom:8px;">
	<div class="input-group-addon">
		<input type="checkbox" id="checkAll" />
	</div>
	<div class="input-group-btn">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">选中项 <span class="caret"></span></button>
		<ul class="dropdown-menu dropdown-action" role="menu">
			<li><a href="###" data-action="remove">删除</a></li>
		</ul>
	</div>
</div>

<div class="wrap-table">
	<table class="table">
		<thead>
		<tr>
			<th>　</th>
			<th>名称</th>
			<th>大小</th>
			<th>所在文章</th>
			<th>上传时间</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($files as $row) { ?>
			<tr>
				<td><input type="checkbox" name="mids[]" value="<?=$row['mid']?>" /></td>
				<td><a href="<?=siteUrl('admin/attachment/detail')?>?aid=<?=$row['aid']?>&p=<?=$page?>"><?=$row['filename']?></a></td>
				<td><?=\vgot\Utils\FileUtil::formatSize($row['filesize'])?></td>
				<td>
					<a href="<?=siteUrl('admin/content/write')?>?cid=<?=$row['cid']?>" target="_blank"><?=$row['title']?></a>
					<a href="<?=$row['url']?>" class="content-list-oplink" title="打开此文章" target="_blank"><span class="glyphicon glyphicon-new-window"></span></a>
				</td>
				<td><?=vdate('Y-m-d H:i:s', $row['uploaded'])?></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<?=$pages?>
</div>

<?php $this->render('admin/footer'); ?>