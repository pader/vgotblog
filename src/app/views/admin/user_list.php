<?php $this->render('admin/header', array('TITLE'=>'管理用户')); ?>

<h3>用户  <a href="<?=siteUrl('admin/user/profile')?>" class="btn btn-link btn-xs" style="margin-left:20px;" role="button">+ 新用户</a></h3>

<?php if ($list) { ?>
	<div class="input-group" style="width:110px;margin-bottom:8px;">
		<div class="input-group-addon">
			<input type="checkbox" id="checkAll" />
		</div>
		<div class="input-group-btn">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">选中项 <span class="caret"></span></button>
			<ul class="dropdown-menu dropdown-action" role="menu">
				<li><a href="###" data-action="remove">删除</a></li>
			</ul>
		</div>
	</div>

	<form action="<?=siteUrl('admin/user/batch')?>" method="post" id="listForm">
		<input type="hidden" name="action" value="" id="sendAction" />
		<div class="wrap-table">
			<table class="table content-list">
				<thead>
				<tr>
					<th>　</th>
					<th>用户名</th>
					<th>昵称</th>
					<th>邮箱</th>
					<th>组</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($list as $row) { ?>
					<tr>
						<td><input type="checkbox" name="uids[]" value="<?=$row['uid']?>" /></td>
						<td><a href="<?=siteUrl('admin/user/profile')?>?uid=<?=$row['uid']?>&p=<?=$page?>"><?=$row['username']?></a>
							<?php if ($row['status'] == 'ban') { ?>
								<small class="text-muted">禁言</small>
							<?php } elseif ($row['status'] == 'disabled') { ?>
								<small class="text-muted">禁用</small>
							<?php } ?>
						</td>
						<td><?=$row['nickname']?></td>
						<td><a href="mailto:<?=$row['mail']?>"><?=$row['mail']?></a></td>
						<td><?php if ($row['group'] == 'visitor') { ?>普通用户<?php } elseif ($row['group'] == 'admin') { ?>管理员<?php } elseif ($row['group'] == 'writer') { ?>作者<?php } ?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
			<?=$pages?>
		</div>
	</form>
<?php } ?>

<?php $this->render('admin/footer'); ?>