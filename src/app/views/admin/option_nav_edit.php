<?php $this->render('admin/header'); ?>

<h3><?=$TITLE?><a href="<?=siteUrl('admin/option/nav')?>" class="btn btn-link btn-xs navlink" role="button">返回导航列表</a></h3>
<form role="form" class="form-option" action="?_=<?=now()?>" method="post">
	<div class="form-group">
		<label for="inputName">导航名称</label>
		<input type="text" class="form-control" name="name" id="inputName" value="<?=$nav['name']?>" />
	</div>
	<div class="form-group">
		<label for="inputUrl">导航地址</label>
		<input type="text" class="form-control" name="url" id="inputUrl" value="<?=$nav['url']?>" <?php if ($disabled) { echo 'disabled '; } ?>/>
		<?php if ($disabled) { ?>
			<p class="help-block">该导航由<?=$sourceType?><?php if ($sourceName != '') { ?> “<a href="<?=$nav['url']?>" target="_blank"><?=$sourceName?></a>” <?php } ?>生成，无法直接修改地址。</p>
		<?php } ?>
	</div>
	<div class="form-group">
		<label for="inputNewPage">打开窗口</label>
		<div class="checkbox">
			<label><input type="checkbox" name="newpage" id="inputNewPage" value="1" <?php if ($nav['newpage']) {?>checked <?php } ?>/> 在新窗口打开</label>
		</div>
	</div>
	<button type="submit" class="btn btn-primary">保存修改</button>
</form>


<?php $this->render('admin/footer'); ?>