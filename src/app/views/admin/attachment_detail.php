<?php
/**
 * @var $this \Controller\Attachment
 * @var $page int
 * @var $attachment array
 * @var $isImage boolean
 * @var $fileUrl string
 */
$this->load->view('admin/header');
?>

<h3><?=$attachment['filename']?><a href="<?=siteUrl('admin/attachment').($page > 1 ? '?p='.$page : '')?>" class="btn btn-link btn-xs navlink" role="button">返回附件列表</a></h3>

<form role="form" action="<?=siteUrl('admin/attachment/save')?>" method="post" class="form-write">
	<input type="hidden" name="aid" value="<?=$attachment['aid']?>" />

	<div class="row">
		<div class="col-sm-9">
			<?php if ($isImage) { ?>
			<img src="<?=$fileUrl?>">
			<?php } ?>
		</div>
		<div class="col-sm-3">
		</div>
	</div>
</form>

<?php $this->load->view('admin/footer'); ?>