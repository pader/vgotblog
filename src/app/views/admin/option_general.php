<?php $this->render('admin/header'); ?>

<h3>基本设置</h3>
<form role="form" class="form-option" action="<?=siteUrl('admin/option/save')?>" method="post">
	<div class="form-group">
		<label for="inputTitle">站点名称</label>
		<input type="text" class="form-control" name="setting[title]" id="inputTitle" value="<?=$option['title']?>" />
	</div>
	<div class="form-group">
		<label for="inputBaseUrl">站点地址</label>
		<input type="text" class="form-control" name="core[base_url]" id="inputBaseUrl" value="<?=$core['base_url']?>" />
		<p class="help-block">输入网站的绝对地址（以斜线“/”结尾），留空则由系统自动生成。</p>
	</div>
	<div class="form-group">
		<label for="inputDescription">站点描述</label>
		<input type="text" class="form-control" name="setting[description]" id="inputDescription" value="<?=$option['description']?>" />
	</div>
	<div class="form-group">
		<label for="inputKeywords">关键词</label>
		<input type="text" class="form-control" name="setting[keywords]" id="inputKeywords" value="<?=$option['keywords']?>" />
	</div>
	<button type="submit" class="btn btn-primary">保存设置</button>
</form>

<?php $this->render('admin/footer'); ?>