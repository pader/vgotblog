<?php
/**
 * @var array $plugin
 * @var \app\libs\Plugin $entry
 * @var array $options
 */

use app\helpers\FormUtil;

$this->render('admin/header');

$makeOption = function($key, $option) use ($plugin, $entry) {
	$value = $entry->get($key, isset($option['default']) ? $option['default'] : null);
	$attrs = ['class' => 'form-control'];
	$key = 'config['.$key.']';
	switch ($option['type']) {
		case 'text':
			return FormUtil::input('text', $key, $value, $attrs);
		case 'select':
			return FormUtil::select($key, $option['options'], $value, $attrs);
		case 'radio':
			return FormUtil::radio($key, $option['options'], $value);
			break;
		case 'textarea':
			return FormUtil::textarea($key, $value, $attrs);
			break;
		default:
			return '';
	}
}
?>

<h3>
	插件配置
	<a href="<?=siteUrl('admin/plugin')?>" class="btn btn-link btn-xs navlink" role="button">返回插件列表</a>
</h3>

<div class="wrap-table plugin-config">
	<h4>
		<?php if (isset($plugin['author'])) { ?>
		<small class="pull-right text-muted" title="作者">
			<span class="glyphicon <?=isset($plugin['url']) ? 'glyphicon-home' : 'glyphicon-user'?>"></span>
			<a href="<?=isset($plugin['url']) ? $plugin['url'] : '#'?>"><?=$plugin['author']?></a>
		</small>
		<?php } ?>
		<?=$plugin['name']?>
		<small><?=$plugin['version']?></small>
	</h4>
	<p><?=$plugin['description']?></p>
	<hr/>
	<form class="form-horizontal plugin-config-form" method="post">
		<h4>配置项</h4>
		<?php foreach ($options as $key => $option) { ?>
			<div class="form-group">
				<label for="input<?=$key?>" class="col-sm-2 control-label"><?=$option['name']?></label>
				<div class="col-sm-8<?php if ($option['type'] == 'radio' || $option['type'] == 'checkbox') { echo " {$option['type']}"; } ?>">
					<?=$makeOption($key, $option)?>
					<?php if (isset($option['desc']))  { ?>
						<span class="help-block"><?=$option['desc']?></span>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-8">
				<button type="submit" class="btn btn-primary">保存配置</button>
			</div>
		</div>
	</form>
</div>

<?php $this->render('admin/footer'); ?>
