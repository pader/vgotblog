<?php
use vgot\Web\Url;
?>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Vgot Blog</title>
<link type="text/css" href="<?=STATIC_URL?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<!--<link type="text/css" href="--><?//=STATIC_URL?><!--lib/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />-->
<link type="text/css" href="<?=STATIC_URL?>admin/base.css" rel="stylesheet" />
<script type="text/javascript" src="<?=STATIC_URL?>lib/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
var ROOT_URL = "<?=Url::base('')?>";
var BASE_URL = "<?=rtrim(Url::base(''), '/').'/'?>";
</script>
<script type="text/javascript" src="<?=STATIC_URL?>js/global.js"></script>
</head>
<body>
<div class="container">
<?php
$panel = 'panel-default';
$icon = '';

switch ($status) {
	case 'success':
		$panel = 'panel-success';
		$icon = 'glyphicon-ok';
		break;
	case 'info':
		$panel = 'panel-info';
		$icon = 'glyphicon-info-sign';
		break;
	case 'error':
		$panel = 'panel-danger';
		$icon = 'glyphicon-remove-sign';
		break;
	case 'alert':
		$panel = 'panel-warning';
		$icon = 'glyphicon-exclamation-sign';
		break;
}
?>
	<div class="panel <?=$panel?> msg msg-<?=$status?>">
		<div class="panel-heading">提示消息</div>
		<div class="panel-body">
			<?php if ($icon) { ?>
				<span class="glyphicon <?=$icon?>"></span>
			<?php } ?>
			<?=$message?>

			<address>
				<?php
				if($goto != ':close') {
					if($seconds > 0) {
						echo '<span id="navlink"><a href="'.$goto.'">&raquo; 页面在 '.$seconds.' 秒后跳转，您也可以点击这里立即跳转..</a> [<a href="#" id="cancel">取消跳转</a>]</span>';
					}
					echo '<button id="begin" onclick="location.href=\''.$goto.'\';" class="btn btn-default"';
					if($seconds > 0) echo ' style="display:none;"';
					echo '>点击继续 &raquo;</button>';
				} else { ?>
					<button onclick="window.opener=null;window.open('','_self');window.close();" class="btn btn-default">&nbsp;关 闭&nbsp;</button>
				<?php } ?>
			</address>
		</div>
	</div>

</div>

<?php if($seconds > 0 and $goto != ':close'): ?>
<script type="text/javascript">
var timeout = setTimeout(function() {
	location.href = "<?=$goto?>";
}, <?=($seconds * 1000)?>);

document.getElementById("cancel").onclick = function(){
	clearTimeout(timeout);
	document.getElementById("navlink").style.display = "none";
	document.getElementById("begin").style.display = "";
};
</script>
<?php endif; ?>
</body>
<script src="<?=STATIC_URL?>lib/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>