<?php
use vgot\Web\Url;
?>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Vgot Blog</title>
<link type="text/css" href="<?=STATIC_URL?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link type="text/css" href="<?=STATIC_URL?>lib/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
<link type="text/css" href="<?=STATIC_URL?>theme/default/base.css" rel="stylesheet" />
<script type="text/javascript" src="<?=STATIC_URL?>lib/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
var ROOT_URL = "<?=Url::base('')?>";
var BASE_URL = "<?=rtrim(Url::base(''), '/').'/'?>";
</script>
<script type="text/javascript" src="<?=STATIC_URL?>js/global.js"></script>
</head>


	<div class="w p10">
		<div class="mod page w2">
			<h3>提示消息</h3>
			<div class="pbody">
				<div class="msgbox<?=$status?> clearfix">
					<div class="ico_tips"></div>
					<div class="fl">
						<?=$message?>
						<address>
							<?php
							if($goto != ':close') {
								if($seconds > 0) {
									echo '<span id="navlink"><a href="'.$goto.'">&raquo; 页面在 '.$seconds.' 秒后跳转，您也可以点击这里立即跳转..</a> [<a href="#" id="cancel">取消跳转</a>]</span>';
								}
								echo '<button id="begin" onclick="location.href=\''.$goto.'\';" class="button"';
								if($seconds > 0) echo ' style="display:none;"';
								echo '>点击继续 &raquo;</button>';
							} else { ?>
								<button onclick="window.opener=null;window.open('','_self');window.close();" class="button">&nbsp;关 闭&nbsp;</button>
							<?php } ?>
						</address>
					</div>
				</div>
			</div>
		</div>

	</div>

<?php if($seconds > 0 and $goto != ':close'): ?>
	<script type="text/javascript">
	var timeout = setTimeout(function() {
		location.href = "<?=$goto?>";
	}, <?=($seconds * 1000)?>);

	document.getElementById("cancel").onclick = function(){
		clearTimeout(timeout);
		document.getElementById("navlink").style.display = "none";
		document.getElementById("begin").style.display = "block";
	};
	</script>
<?php endif; ?>

</div>
<script src="<?=STATIC_URL?>lib/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>