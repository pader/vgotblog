<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title><?php if (isset($TITLE)) { echo $TITLE.' | '; } echo $this->config->get('option', 'title'); ?></title>
<?php if ($OPTION['description'] != '') { ?>
<meta name="description" content="<?=$OPTION['description']?>" />
<?php } if ($OPTION['keywords'] != '') { ?>
<meta name="keywords" content="<?=$OPTION['keywords']?>" />
<?php } ?>
<link type="text/css" href="<?=STATIC_URL?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link type="text/css" href="<?=STATIC_URL?>lib/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
<link type="text/css" href="<?=STATIC_URL?>theme/default/base.css" rel="stylesheet" />
<script type="text/javascript" src="<?=STATIC_URL?>lib/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
var ROOT_URL = "<?=baseUrl('')?>";
var BASE_URL = "<?=rtrim(siteUrl(''), '/').'/'?>";
</script>
<script type="text/javascript" src="<?=STATIC_URL?>js/global.js"></script>
<!--[if lt IE 9]>
<script src="//cdn.bootcss.com/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
<script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!--
<link type="text/css" href="<?=STATIC_URL?>lib/ueditor/third-party/SyntaxHighlighter/shCoreDefault.css" rel="stylesheet" />
<script type="text/javascript" src="<?=STATIC_URL?>lib/ueditor/third-party/SyntaxHighlighter/shCore.js"></script>
-->
</head>
<body>
<div class="head-master">
	<div class="container">
		<!--<div class="head-logo"></div>-->
		<nav class="nav pull-left">
		<?php foreach ($OPTION['nav'] as $nav) { ?>
			<a href="<?=$nav['url']?>"<?php if (isset($FLAG) && $FLAG == $nav['flag']) { echo ' class="active"'; } ?><?php if ($nav['newpage']) { echo ' target="_blank"'; } ?>><?=$nav['name']?></a>
		<?php } ?>
		</nav>
		<div class="nav pull-right">
			<?php if ($_ENV['uid'] > 0) { ?>
				<a href="#"><?=$_ENV['user']['username']?></a>
				<?php if ($_ENV['manage_permission']) { ?>
					<a href="<?=siteUrl('admin')?>" target="_blank">管理后台</a>
				<?php } ?>
				<a href="<?=siteUrl('login/logout')?>">退出</a>
			<?php } else { ?>
				<a href="<?=siteUrl('login')?>">登录</a>
			<?php } ?>
		</div>
	</div>
</div>

<div class="container container-body">