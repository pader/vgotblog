<?php view('header'); ?>

<div class="row">
	<div class="col-md-9">
		<div class="blog-post">
			<h2 class="blog-post-title"><a href="<?=url($post)?>"><?=$post['title']?></a></h2>
			<div class="blog-post-meta">
				<a href="<?=siteUrl('author/'.$post['uid'])?>" title="查看所有由 <?=$post['nickname']?> 撰写的文章"><?=$post['nickname']?></a> /
				<?php if ($category) { ?><a href="<?=url($category, 'category')?>"><?=$category['name']?></a> / <?php } ?>
				<?=vdate('Y.m.d', $post['created'])?>
			</div>
			<?php if ($passwordChecked) { ?>
				<div class="blog-post-content"><?=$post['content']?></div>
			<?php } else { ?>
				<div class="blog-post-pwdconfirm">
					<p>需要密码才能查看该内容</p>
					<form action="<?=siteUrl('posts/verify')?>" class="form-inline" method="post">
						<input type="hidden" name="cid" value="<?=$post['cid']?>" />
						<input type="password" class="form-control" name="password" value="" placeholder="文章查看密码" />
						<input type="submit" class="btn btn-primary" value="查看" />
					</form>
				</div>
			<?php } ?>
		</div>
		<div class="ds-thread" data-thread-key="post-<?=$post['cid']?>"></div>
	</div>
	<?php view('sidebar'); ?>
</div>

<script type="text/javascript" src="<?=STATIC_URL?>js/vb-content.js"></script>

<?php if ($passwordChecked && $post['allowComment']) { ?>
<script type="text/javascript">
var duoshuoQuery = {short_name:"vgotblog"};
(function() {
	var ds = document.createElement('script');
	ds.type = 'text/javascript';ds.async = true;
	ds.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//static.duoshuo.com/embed.js';
	ds.charset = 'UTF-8';
	(document.getElementsByTagName('head')[0]
	|| document.getElementsByTagName('body')[0]).appendChild(ds);
})();
</script>
<?php } ?>
<?php view('footer'); ?>