<?php view('header'); ?>

<div class="row">
	<div class="col-md-9">
		<div class="blog-post">
			<h2 class="blog-post-title"><a href="<?=url($post, 'page')?>"><?=$post['title']?></a></h2>
			<div class="blog-post-content"><?=$post['content']?></div>
			<div class="ds-thread" data-thread-key="post-<?=$post['cid']?>"></div>
		</div>
	</div>
	<?php view('sidebar'); ?>
</div>

<script type="text/javascript" src="<?=STATIC_URL?>js/vb-content.js"></script>

<?php if ($post['allowComment']) { ?>
<script type="text/javascript">
var duoshuoQuery = {short_name:"vgotblog"};
(function() {
	var ds = document.createElement('script');
	ds.type = 'text/javascript';ds.async = true;
	ds.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//static.duoshuo.com/embed.js';
	ds.charset = 'UTF-8';
	(document.getElementsByTagName('head')[0]
	|| document.getElementsByTagName('body')[0]).appendChild(ds);
})();
</script>
<?php } ?>
<?php view('footer'); ?>