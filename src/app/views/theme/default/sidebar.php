<div class="col-md-3">
	<?php
	$categoreis = $this->cache->get('category');
	if ($categoreis) {
	?>
	<div class="sidebar-module">
		<h4>分类</h4>
		<ul class="list-unstyled">
			<?php foreach ($categoreis as $row) { ?>
				<li><a href="<?=url($row, 'category')?>"><?=$row['name']?></a></li>
			<?php } ?>
		</ul>
	</div>
	<?php } ?>

	<?php
	$hotTags = $this->cache->get('hotTags');
	if ($hotTags) {
	?>
	<div class="sidebar-module">
		<h4>热门标签</h4>
		<ul class="list-unstyled clearfix hottags">
			<?php foreach ($hotTags as $row) { ?>
				<li><a href="<?=siteUrl('tag/'.$row['alias'])?>"><?=$row['name']?></a></li>
			<?php } ?>
		</ul>
	</div>
	<?php } ?>

	<div class="sidebar-module">
		<h4>归档</h4>
		<ul class="list-unstyled">
		</ul>
	</div>
</div>