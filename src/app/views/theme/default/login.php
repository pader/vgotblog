<?php view('header'); ?>

<div class="well login-panel">
	<form role="form" onsubmit="return postLogin(this);">
		<div class="form-group">
			<label for="exampleInputEmail1">用户名</label>
			<input type="username" name="username" class="form-control" id="exampleInputEmail1" placeholder="输入用户名" />
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">密码</label>
			<input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="输入密码">
		</div>
		<div class="checkbox">
			<label>
				<input type="checkbox" name="keeplogin" value="1" /> 保持登录
			</label>
		</div>
		<div class="alert alert-warning" role="alert" id="loginMessage" style="display:none;"></div>
		<button type="submit" class="btn btn-default" id="loginBtn">登录</button>
	</form>
</div>

<script type="text/javascript">
function postLogin(form) {
	var username = $.trim(form.username.value);
	var password = $.trim(form.password.value);

	var keepLogin = form.keeplogin.checked ? 1 : 0;

	$("#loginBtn").prop("disabled", true);

	$.post(BASE_URL + "login/postlogin", {username:username, password:password, keeplogin:keepLogin}, function(t) {
		if (t.status) {
			location.href = t.data.redirectTo;
		} else {
			$("#loginBtn").prop("disabled", false);
			$("#loginMessage").html(t.message).show();
		}
	}, "json");

	return false;
}
</script>

<?php view('footer'); ?>