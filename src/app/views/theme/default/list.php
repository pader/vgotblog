<?php view('header'); ?>

<div class="row">
	<div class="col-md-9 blog-list">
		<?php if (isset($listTitle)) { ?><h3 class="blog-list-title"><span class="glyphicon glyphicon-tag"></span><?=$listTitle?></h3><?php } ?>
		<?php foreach ($list as $post) { ?>
			<div class="blog-post">
				<h2 class="blog-post-title"><a href="<?=$post['url']?>" name="post<?=$post['cid']?>"><?=$post['title']?></a></h2>
				<div class="blog-post-meta">
					<a href="<?=siteUrl('author/'.$post['uid'])?>" title="查看所有由 <?=$post['nickname']?> 撰写的文章"><?=$post['nickname']?></a> /
					<?php if ($post['category']) { ?><a href="<?=url($post, 'category')?>"><?=$post['category']?></a> / <?php } ?>
					<?=vdate('Y.m.d', $post['created'])?>
				</div>
			<?php if (blogCheckPassword($post)) { ?>
				<div class="blog-post-content">
					<?=$post['content']?>
					<?php if ($post['hasMore']) { ?>
						<p class="blog-post-more"><span class="glyphicon glyphicon-eye-open" title="阅读剩余部分"></span> <a href="<?=$post['url']?>#more">阅读全部内容 &gt;&gt;</a></p>
					<?php } ?>
				</div>
			<?php } else { ?>
				<div class="blog-post-pwdconfirm">
					<p>需要密码才能查看该内容</p>
					<form action="<?=siteUrl('posts/verify')?>" class="form-inline" method="post">
						<input type="hidden" name="cid" value="<?=$post['cid']?>" />
						<input type="password" class="form-control" name="password" value="" placeholder="文章查看密码" />
						<input type="submit" class="btn btn-default" value="查看" />
					</form>
				</div>
			<?php } ?>
			</div>
		<?php } ?>
		<?=$pages?>
	</div>
	<?php view('sidebar'); ?>
</div>

<script type="text/javascript" src="<?=STATIC_URL?>js/vb-content.js"></script>

<?php view('footer'); ?>