<?php

use app\services\MetaService;
use vgot\Web\Url;

$app = getApp();
?>
<div class="col-sm-2 sidebar hidden-xs">
<?php
$categoreis = MetaService::getCategories();
$hotTags = MetaService::getHotTags();

if ($categoreis) {
?>
	<div class="sidebar-name">分类</div>
	<ul class="sidebar-content list-unstyled sidebar-category">
	<?php foreach ($categoreis as $row) { ?>
		<li><a href="<?=url($row, 'category')?>"><?=$row['name']?></a></li>
	<?php } ?>
	</ul>
<?php } ?>

	<div class="sidebar-name">热门标签</div>
	<div class="sidebar-content sidebar-tags tags">
	<?php foreach ($hotTags as $row) { ?>
		<a href="<?=Url::site('tag/'.$row['alias'])?>"><?=$row['name']?></a>
	<?php } ?>
	</div>
</div>