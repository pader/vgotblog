<?php view('header'); ?>

<div class="row">
	<?php view('sidebar'); ?>
	<div class="col-md-9 col-sm-10 content-wrap">
		<h1 class="content-title"><?=$post['title']?></h1>
		<div class="content-body blog-post-content"><?=$post['content']?></div>
		<?php if ($post['allowComment']) { ?>
			<?php view('sub-comment'); ?>
		<?php } ?>
	</div>
</div>

<?php view('footer'); ?>