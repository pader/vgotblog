<?php
/**
 * @var $post array
 * @var $category array
 * @var $passwordChecked bool
 */
use vgot\Web\Url;

view('header');
?>

<div class="row">
	<?php view('sidebar'); ?>
	<div class="col-md-9 col-sm-10 content-wrap">
		<h1 class="content-title"><?=$post['title']?></h1>
		<span class="meta-section posted">
			<a href="<?=Url::site('author/'.$post['uid'])?>" class="meta-user" title="查看所有由 <?=$post['nickname']?> 撰写的文章"><?=$post['nickname']?></a>
			在 <span class="meta-date"><?=vdate('Y年n月j日', $post['created'])?></span>
			发表于 <?php if ($category) { ?><a href="<?=url($category, 'category')?>" class="meta-category"><?=$category['name']?></a><?php } ?>
		</span>
		<?php if ($tags) { ?>
		<span class="meta-section tags">
			<?php foreach ($tags as $tag) { ?>
			<a class="tag" href="<?=siteUrl(['tag/'.urlencode($tag['alias'])])?>"><?=$tag['name']?></a>
			<?php } ?>
		</span>
		<?php } ?>
		<?php if (!$passwordChecked) { ?>
			<div class="blog-post-pwdconfirm">
				<p>需要密码才能查看该内容</p>
				<form action="<?=Url::site('posts/verify')?>" class="form-inline" method="post">
					<input type="hidden" name="cid" value="<?=$post['cid']?>" />
					<input type="password" class="form-control" name="password" value="" placeholder="文章查看密码" />
					<input type="submit" class="btn btn-primary" value="查看" />
				</form>
			</div>
		<?php } else { ?>
			<div class="content-body blog-post-content"><?=$post['content']?></div>
			<?php if ($post['allowComment']) { ?>
				<?php view('sub-comment'); ?>
			<?php } ?>
		<?php } ?>
	</div>
</div>

<?php view('footer'); ?>