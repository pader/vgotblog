</div>

<footer class="footer">
	<div class="container">
		<?=getApp()->hook->pre('page_foot')?>
		<p class="text-center"><span class="symbol glyphicon glyphicon-fire"></span>由 VGOT BLOG 强力驱动</p>
		<?=getApp()->hook->post('page_foot')?>
	</div>
</footer>

<script type="text/javascript" src="<?=STATIC_URL?>js/vb-content.js"></script>
<script src="<?=STATIC_URL?>lib/bootstrap/js/bootstrap.min.js"></script>
<?=getApp()->hook->post('page_body')?>
</body>
</html>