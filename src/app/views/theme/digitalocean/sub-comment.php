<?php
use vgot\Web\Url;

/**
 * @var array $post
 * @var array $comment
 * @var string $pages
 * @var array $commentAuthor
 */

$commentUser = function($row) use ($post) {
?>
<span class="comment-item-author">
	<?php if ($row['url']) { ?>
		<a href="<?=$row['url']?>" target="_blank"><?=$row['author']?></a>
	<?php } else { ?>
		<?=$row['author']?>
	<?php } ?>
	<?php if ($post['uid'] == $row['uid']) { ?><span class="label label-info">博主</span><?php } ?>
</span>
<?php
}
?>

<div class="comment" id="comment">
	<a name="comment"></a>
	<h3><span class="glyphicon glyphicon-comment text-muted"></span> 评论 <small>共有 <?=$post['commentNum']?> 条评论</small></h3>
	<form method="post" action="<?=Url::site('comment/post')?>" name="comment">
		<input type="hidden" name="_csrfToken" value="<?=csrfToken('cid_'.$post['cid'])?>">
		<input type="hidden" name="cid" value="<?=$post['cid']?>">

		<div class="media comment-form">
			<div class="media-left"><img class="media-object img-rounded" src="<?=getAvatar($_ENV['user'])?>" /></div>
			<div class="media-body">
				<div class="media-heading">
					<?php if ($_ENV['uid']) { ?>
						<span class="text-muted">已登录为</span>
						<?php if ($_ENV['user']['url']) { ?>
							<a href="<?=$_ENV['user']['url']?>" target="_blank"><?=$_ENV['user']['username']?></a>
						<?php } else { ?>
							<?=$_ENV['user']['username']?>
						<?php } ?>
					<?php } else { ?>
						<div class="row">
							<div class="col-sm-3 col-xs-12">
								<input name="author" type="text" class="form-control" id="inputNickname" value="<?=$commentAuthor['author']?>" autocomplete placeholder="称呼（必填）" required />
							</div>
							<div class="col-sm-3 col-xs-12">
								<input name="email" type="email" class="form-control" id="inputEmail" value="<?=$commentAuthor['email']?>" autocomplete placeholder="邮箱，不会被公开（<?=opt('comment_email_require')?'必填':'选填'?>）" <?=opt('comment_email_require')?' required':''?>/>
							</div>
							<div class="col-sm-4 col-xs-12">
								<input name="url" type="url" class="form-control" id="inputUrl" value="<?=$commentAuthor['url']?>" autocomplete placeholder="您的网站（选填）" />
							</div>
						</div>
					<?php } ?>
				</div>
				<div class="row">
					<div class="col-sm-10 col-xs-12">
						<textarea name="content" class="form-control" autocomplete="off" placeholder="评论内容.." required></textarea>
						<div class="comment-btns pull-right">
							<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-send"></span>&nbsp;&nbsp;评论</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>

	<?php if ($comment['count'] > 0) { ?>
		<ul class="media-list comment-list">
		<?php foreach ($comment['list'] as $row) { ?>
			<li class="media comment-item" data-comment-id="<?=$row['cmtid']?>">
				<a name="comment<?=$row['cmtid']?>"></a>
				<div class="media-left"><img class="media-object img-rounded" src="<?=getAvatar($row)?>" /></div>
				<div class="media-body">
					<div class="media-heading">
						<?php $commentUser($row); ?>
						<small class="text-muted comment-item-time"><?=fmtime($row['created'])?></small>
					</div>
					<p><?=$row['content']?></p>
					<div class="comment-acts"><a href="javascript:;" class="comment-reply">回复</a></div>

					<!--回复列表-->
					<?php if ($row['replies']) { ?>
						<ul class="media-list comment-replies">
							<?php foreach ($row['replies'] as $reply) { ?>
								<li class="media comment-item" data-comment-id="<?=$reply['cmtid']?>">
									<a name="comment<?=$reply['cmtid']?>"></a>
									<div class="media-left"><img class="media-object img-rounded" src="<?=getAvatar($reply)?>" /></div>
									<div class="media-body">
										<div class="media-heading">
											<?php $commentUser($reply); ?>
											<span class="text-muted comment-item-rpf">
												<em class=" glyphicon glyphicon-share-alt"></em>
												<a href="#comment<?=$reply['reply_id']?>"><?=$reply['reply_author']?></a>
											</span>
											<small class="text-muted comment-item-time"><?=fmtime($reply['created'])?></small>
										</div>
										<p><?=$reply['content']?></p>
										<div class="comment-acts"><a href="javascript:;" class="comment-reply">回复</a></div>
									</div>
								</li>
							<?php } ?>
						</ul>
					<?php } ?>

				</div>
			</li>
		<?php } ?>
		</ul>
		<?=$pages?>
	<?php } else { ?>
		<p class="text-muted text-center">暂无评论，快发表你的评论吧。</p>
	<?php } ?>
</div>
<script type="text/javascript" src="<?=STATIC_URL?>lib/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>lib/jquery-validation/localization/messages_zh.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>lib/msg.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>theme/digitalocean/form.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>js/comment.js"></script>