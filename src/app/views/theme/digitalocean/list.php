<?php
use vgot\Web\Url;

view('header');
/**
 * @var array $list
 */
?>

<div class="row">
	<?php view('sidebar'); ?>
	<div class="col-sm-9 list-wrap">
		<?php if (isset($listTitle)) { ?>
			<div class="category-title text-muted">
				<span class="glyphicon glyphicon-tag"></span> <?=$listTitle?>
			</div>
		<?php } ?>
		<div class="list-outer">
		<?php foreach ($list as $post) { ?>
			<div class="list-row">
				<h3 class="list-title"><a href="<?=$post['url']?>" name="post<?=$post['cid']?>"><?=$post['title']?></a></h3>
				<?php if (blogCheckPassword($post)) { ?>
					<div class="list-intro blog-post-content content-body"><?=$post['content']?></div>
				<?php } else { ?>
					<div class="blog-post-pwdconfirm">
						<p>需要密码才能查看该内容</p>
						<form action="<?=siteUrl('posts/verify')?>" class="form-inline" method="post">
							<input type="hidden" name="cid" value="<?=$post['cid']?>" />
							<input type="password" class="form-control" name="password" value="" placeholder="文章查看密码" />
							<input type="submit" class="btn btn-default" value="查看" />
						</form>
					</div>
				<?php } ?>
				<div class="list-meta">
					<?php if ($post['hasMore']) { ?>
						<p class="blog-post-more pull-right"><a href="<?=$post['url']?>#more" title="阅读剩余部分"><span class="glyphicon glyphicon-eye-open"></span> 阅读全部内容</a></p>
					<?php } ?>
					<a href="<?=Url::site('author/'.$post['uid'])?>" title="查看所有由 <?=$post['nickname']?> 撰写的文章" class="meta-user"><span class="glyphicon glyphicon-user"></span><?=$post['nickname']?></a>
					<span class="meta-date"><span class="glyphicon glyphicon-calendar"></span><?=fmtime($post['created'])?></span>
					<a href="<?=$post['url']?>#comment" class="meta-comment"><span class="glyphicon glyphicon-comment"></span><?=$post['commentNum']?></a>
				</div>
			</div>
		<?php } ?>
		</div>
		<?=$pages?>
	</div>
</div>

<?php view('footer'); ?>