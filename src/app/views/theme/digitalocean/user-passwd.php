<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2020/2/1
 * Time: 14:43
 */

use vgot\Web\Url;

view('header');
?>

<div class="row">
	<?php view('user-sidebar'); ?>
	<div class="col-sm-9 content-wrap">
		<h1 class="content-title">修改密码</h1>

		<form class="form-horizontal" name="userProfile" method="post" action="<?=Url::site('user/passwd/post')?>">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">用户名</label>
				<div class="col-sm-10">
					<?=$_ENV['user']['username']?>
				</div>
			</div>
			<div class="form-group">
				<label for="oldPassword" class="col-sm-2 control-label">原密码</label>
				<div class="col-sm-10">
					<input type="password" name="password" class="form-control" id="oldPassword">
				</div>
			</div>
			<div class="form-group">
				<label for="newPassword" class="col-sm-2 control-label">新密码</label>
				<div class="col-sm-10">
					<input type="password" name="newPassword" class="form-control" id="newPassword">
				</div>
			</div>
			<div class="form-group">
				<label for="passwordConfirm" class="col-sm-2 control-label">重复新密码</label>
				<div class="col-sm-10">
					<input type="password" name="passwordConfirm" class="form-control" id="passwordConfirm">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">提交修改</button>
				</div>
			</div>
		</form>
	</div>

</div>

<script type="text/javascript" src="<?=STATIC_URL?>lib/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>lib/jquery-validation/localization/messages_zh.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>lib/msg.js"></script>
<script type="text/javascript" src="<?=STATIC_URL?>theme/digitalocean/form.js"></script>
<script type="text/javascript">
var form = document.forms["userProfile"];

var validate = $(form).validate({
	rules: {
		password: {
			required: true
		},
		newPassword: {
			required: true,
			minlength: 6
		},
		passwordConfirm: {
			equalTo: form.newPassword
		}
	},
	messages: {
		passwordConfirm: {
			equalTo: "请输入相同的密码"
		}
	},
	submitHandler: function() {
		$(form).handleForm({
			afterSuccess: function() {
				location.reload();
			},
			afterError: function(flag) {
				switch (flag) {
					case 'old_password_error':
						form.password.value = '';
						form.password.focus();
						break;
					case 'len_error':
						form.passwordConfirm.value = '';
						form.newPassword.focus();
						break;
				}
			}
		});
		return false;
	}
});
</script>
<?php view('footer'); ?>