<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2020/2/1
 * Time: 14:43
 *
 * @var $user array
 */

view('header');
?>

<div class="row">
	<?php view('user-sidebar'); ?>
	<div class="col-sm-9 content-wrap">
		<h1 class="content-title">个人信息</h1>
		<table class="table">
			<tr>
				<td>用户名</td>
				<td><?=$user['username']?></td>
			</tr>
			<tr>
				<td>邮箱</td>
				<td><?=$user['mail']?></td>
			</tr>
			<tr>
				<td>昵称</td>
				<td><?=$user['nickname']?></td>
			</tr>
			<tr>
				<td>注册时间</td>
				<td><?=date('Y-m-d H:i', $user['regtime'])?></td>
			</tr>
			<tr>
				<td>用户组</td>
				<td><?=$user['group']?></td>
			</tr>
			<tr>
				<td>状态</td>
				<td><?=$user['status']?></td>
			</tr>
			<tr>
				<td>个人简介</td>
				<td><?=$user['description']?></td>
			</tr>
		</table>
	</div>
</div>

<?php view('footer'); ?>