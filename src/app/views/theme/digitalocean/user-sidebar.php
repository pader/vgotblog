<?php

use vgot\Web\Url;

$app = getApp();
?>
<div class="col-sm-2 sidebar hidden-xs">
	<div class="sidebar-name">个人中心</div>
	<ul class="sidebar-content list-unstyled sidebar-category">
		<li><a href="<?=Url::site('user')?>">个人信息</a></li>
		<li><a href="<?=Url::site('user/comments')?>">我的评论</a></li>
		<li><a href="<?=Url::site('user/passwd')?>">修改密码</a></li>
	</ul>
</div>
