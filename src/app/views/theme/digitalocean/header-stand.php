<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title><?php if (isset($TITLE)) { echo $TITLE.' | '; } echo opt('title'); ?></title>
<link type="text/css" href="<?=STATIC_URL?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link type="text/css" href="<?=STATIC_URL?>theme/digitalocean/style.css" rel="stylesheet" />
<meta name="description" content="<?=opt('description')?>" />
<meta name="keywords" content="<?=opt('keywords')?>" />
<link rel="shortcut icon" href="<?=STATIC_URL?>vgot.svg" type="image/svg+xml" />
<script type="text/javascript" src="<?=STATIC_URL?>lib/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
var ROOT_URL = "<?=siteUrl('')?>";
var BASE_URL = "<?=rtrim(siteUrl(''), '/').'/'?>";
</script>
<!--[if lt IE 9]>
<script src="//cdn.bootcss.com/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
<script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="<?=STATIC_URL?>js/global.js"></script>
</head>
<body>