<?php view('header'); ?>

<div class="container">
	<div class="row">
		<?php view('sidebar'); ?>
		<div class="col-sm-10 list-wrap">
			<?php if (isset($listTitle)) { ?>
				<div class="panel panel-default panel-floating panel-floating-inline">
					<div class="panel-body">
						<div class="panel-content">
							<h5><span class="glyphicon glyphicon-tag"></span> <strong><?=$listTitle?></strong></h5>
							<p class="text-muted"><small>内容列表已经筛选</small></p>
						</div>
					</div>
				</div>
			<?php } ?>
			<div class="row">
			<?php foreach ($list as $post) { ?>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="list-block">
						<div class="block-hd">
							<h3 class="block-title"><a href="<?=$post['url']?>" name="post<?=$post['cid']?>"><?=$post['title']?></a></h3>
							<?php if (blogCheckPassword($post)) { ?>
								<div class="block-desc"><?=strip_tags($post['content'], '<p><br><embed><img><video>')?></div>
							<?php } else { ?>
								<div class="blog-post-pwdconfirm">
									<p>需要密码才能查看该内容</p>
									<form action="<?=siteUrl('posts/verify')?>" class="form-inline" method="post">
										<input type="hidden" name="cid" value="<?=$post['cid']?>" />
										<input type="password" class="form-control" name="password" value="" placeholder="文章查看密码" />
										<input type="submit" class="btn btn-default" value="查看" />
									</form>
								</div>
							<?php } ?>
						</div>
						<div class="block-meta">
							<a href="<?=siteUrl('author/'.$post['uid'])?>" title="查看所有由 <?=$post['nickname']?> 撰写的文章" class="meta-user"><span class="glyphicon glyphicon-user"></span><?=$post['nickname']?></a>
							<span class="meta-date"><span class="glyphicon glyphicon-calendar"></span><?=vdate('Y-m-d', $post['created'])?></span>
							<a href="#" class="meta-comment"><span class="glyphicon glyphicon-comment"></span>10</a>
						</div>
					</div>
				</div>
			<?php } ?>
			</div>
			<?=$pages?>
		</div>
	</div>
</div>

<?php view('footer'); ?>