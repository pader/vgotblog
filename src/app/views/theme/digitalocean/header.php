<?php
use vgot\Web\Url;
view('header-stand');
?>
<?=getApp()->hook->pre('page_body')?>
<header>
	<div class="container">
		<div class="brand-logo">
			<a class="logo-icon" href="#">VGOT BLOG</a>
			<span class="logo-separator"></span>
		</div>
		<nav>
			<ul>
			<?php foreach (opt('nav') as $nav) { ?>
				<li><a href="<?=$nav['url']?>"<?php if (isset($FLAG) && $FLAG == $nav['flag']) { echo ' class="active"'; } if ($nav['newpage']) { echo ' target="_blank"'; } ?>><?=$nav['name']?></a></li>
			<?php } ?>
			</ul>
		</nav>
		<div class="account-buttons">
			<?php if ($_ENV['uid'] > 0) { ?>
				<div class="dropdown">
					<a href="javascript:;" class="dropdown-toggle" type="button" id="userDropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<img class="img-circle" src="<?=getAvatar($_ENV['user'])?>" />
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdownMenu">
						<li class="dropdown-header"><?=$_ENV['user']['username']?></li>
						<li role="separator" class="divider"></li>
						<li><a href="<?=Url::site('user')?>">个人信息</a></li>
						<?php if ($_ENV['manage_permission']) { ?>
						<li><a href="<?=Url::site('admin')?>" target="_blank">管理后台</a></li>
						<?php } ?>
						<li><a href="<?=Url::site('login/logout')?>">退出</a></li>
					</ul>
				</div>
			<?php } else { ?>

				<a href="<?=Url::site('login')?>" class="account-login">
					<span class="account-icon glyphicon glyphicon-user"></span>
					登录
				</a>
<!--				<a href="#" class="account-signup">注册</a>-->
			<?php } ?>
		</div>
	</div>
</header>

<div class="container">