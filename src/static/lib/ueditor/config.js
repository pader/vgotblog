(function () {
    var URL = window.UEDITOR_HOME_URL || getUEBasePath();

    window.UEDITOR_CONFIG = {
        UEDITOR_HOME_URL: URL,
		serverUrl: "#",
		toolbars: [["bold","italic","underline","strikethrough","forecolor","backcolor","fontsize","|",
			"justifyleft","justifycenter", "justifyright", "|", "insertunorderedlist","insertorderedlist",
			"inserttable","link","anchor","emotion", "insertvideo","horizontal","blockquote","insertcode",
			"|","drafts","pasteplain","pagebreak","fullscreen","|","source"],[]],
		elementPathEnabled: false,
		wordCount: false,
		initialFrameHeight: 440,
		autoHeightEnabled: false,
		initialStyle: '', //IFrame CSS Style
		saveInterval: 30000,
		pageBreakTag: '__page_break_tag__'
	};

    function getUEBasePath(docUrl, confUrl) {
        return getBasePath(docUrl || self.document.URL || self.location.href, confUrl || getConfigFilePath());
    }

    function getConfigFilePath() {
        var configPath = document.getElementsByTagName('script');
        return configPath[ configPath.length - 1 ].src;
    }

    function getBasePath(docUrl, confUrl) {
        var basePath = confUrl;

        if (/^(\/|\\\\)/.test(confUrl)) {
            basePath = /^.+?\w(\/|\\\\)/.exec(docUrl)[0] + confUrl.replace(/^(\/|\\\\)/, '');
        } else if (!/^[a-z]+:/i.test(confUrl)) {
            docUrl = docUrl.split("#")[0].split("?")[0].replace(/[^\\\/]+$/, '');
            basePath = docUrl + "" + confUrl;
        }

        return optimizationPath(basePath);
    }

    function optimizationPath(path) {
        var protocol = /^[a-z]+:\/\//.exec(path)[ 0 ],
            tmp = null,
            res = [];

        path = path.replace(protocol, "").split("?")[0].split("#")[0];
        path = path.replace(/\\/g, '/').split(/\//);
        path[ path.length - 1 ] = "";

        while (path.length) {
            if (( tmp = path.shift() ) === "..") {
                res.pop();
            } else if (tmp !== ".") {
                res.push(tmp);
            }
        }

        return protocol + res.join("/");
    }

    window.UE = {
        getUEBasePath: getUEBasePath
    };

})();