$(function() {
	//SyntaxHighlighter.all();
});

$.fn.handleForm = function(options) {
	var btn = this.find(":submit");
	btn.prop('disable', true);

	var sets = $.extend({
		afterSuccess: $.noop,
		afterError: $.noop,
		loadingText: '处理中'
	}, options);

	msg.loading(sets.loadingText);

	$.ajax(this.attr('action'), {
		type: 'post',
		data: this.serialize(),
		success: function(ret) {
			if (ret.status) {
				msg.success(ret.message, function() {
					sets.afterSuccess(ret.data);
				});
			} else {
				msg.failure(ret.message, function() {
					sets.afterError(ret.data);
				});
			}
		},
		error: function(xhr) {
			msg.failure(xhr.status + ' 错误', sets.afterError);
		},
		complete: function() {
			btn.prop('disable', false);
		}
	});
}
