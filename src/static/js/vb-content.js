var codeBlock = false;

$("div.blog-post-content").each(function() {
	//Set the code block
	$(this).find("pre").each(function() {
		var cls = $(this).attr("class");
		if (cls) {
			var m = cls.match(/brush:(\w+);/);
			if (m) {
				$(this).wrapInner('<code class="language-' + m[1] + '"></code>').attr("class", "code-block");
				codeBlock = true;
			}
		}
	});
});

(function() {
	var thisScript = $("script:last");
	var src = thisScript.attr("src");
	var staticUrl = thisScript.attr("src").substring(0, src.length-16);

	//Code Highlight
	if (codeBlock) {
		var libPrism = '<link rel="stylesheet" href="' + staticUrl + 'lib/prism/prism.css">'
			+ '<script src="' + staticUrl + 'lib/prism/prism.js"></script>';
		//thisScript.after(libPrism);
		document.write(libPrism);
	}
})();