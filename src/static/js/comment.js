const commentForm = document.forms["comment"];
const comment = $("#comment");
const replyFormBasic = $(commentForm).clone().addClass('comment-reply-form');

replyFormBasic.find('input[type="hidden"]:last')
	.after('<input type="hidden" name="reply_id" value="" />')

$(commentForm).validate({
	submitHandler() {
		$(commentForm).handleForm({
			loadingText: '发表中',
			afterSuccess() {
				location.reload();
			}
		});
		return false;
	}
});

comment.on('click', '.comment-reply', function() {
	if ($(this).hasClass('comment-reply-open')) {
		$(this).closest('.media-body').find('.comment-reply-form').remove();
		$(this).removeClass('comment-reply-open');
	} else {
		$('.comment-list').find('.comment-reply-open').each(function() {
			$(this).closest('.media-body').find('.comment-reply-form').remove();
			$(this).removeClass('comment-reply-open');
		});

		const commentId = $(this).closest('.comment-item').data('comment-id');
		const replyForm = replyFormBasic.clone();
		replyForm.find('input[name="reply_id"]').val(commentId);

		$(this).closest('.comment-acts').after(replyForm);
		$(this).addClass('comment-reply-open');

		replyForm.find('textarea').focus();

		$(replyForm).validate({
			submitHandler() {
				$(replyForm).handleForm({
					loadingText: '回复中',
					afterSuccess() {
						location.reload();
					}
				});
				return false;
			}
		});
	}
});