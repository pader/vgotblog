function showMessage(type, message, delay) {
	if (typeof delay == "undefined") {
		delay = 3000;
	}

	$("#message").remove();

	$("body > .container-body").prepend('<div class="alert alert-' + type + '" role="alert" id="message">' + message + '</div>');

	if (delay > 0) {
		setTimeout(function() {
			$("#message").slideUp(function() {
				$(this).remove();
			});
		}, delay)
	}
}

//The Simple Tabs Plugin
$.fn.simpleTabs = function(options) {
	var settings = {
		contents: "",
		currentClass: "current",
		defaultShow: 0,
		event: "click",
		method: {},
		onBeforeSwitch: $.noop,
		onSwitch: $.noop
	};
	if (options) {
		$.extend(settings,options);
	}

	var tabs = $(this);
	var contents = $(settings.contents);
	tabs.eq(settings.defaultShow).addClass(settings.currentClass);
	contents.hide().eq(settings.defaultShow).show();

	settings.method.switchTab = function(index) {
		var t = tabs.eq(index), c = contents.eq(index);

		settings.onBeforeSwitch.call(c,index,tabs,contents);

		tabs.not(t).removeClass(settings.currentClass);
		t.addClass(settings.currentClass);
		contents.not(c).hide();
		c.show();

		settings.onSwitch.call(c,index,tabs,contents);
	};

	tabs.bind(settings.event,function(){
		var index = tabs.index(this);
		settings.method.switchTab(index);
	});

	return this;
};

function filesize(bytes) {
	var filesizeUnits = ['Bytes','KB','MB','GB','TB','PB','EB','ZB','YB'];
	var base = 1024;
	if (bytes) {
		var i = 0;
		if (bytes >= base) {
			var a = bytes;
			while(1) { a /= base; i++; if(a < base) break; }
		}
		return parseFloat((bytes/Math.pow(base,i)).toFixed(2)) + ' ' + filesizeUnits[i];
	} else return '0 Bytes';
}

function filename(name, length) {
	length = length || 25;
	if (name.length > length) {
		return name.substr(0, 10) + "..." + name.substr(-10);
	} else {
		return name;
	}
}

function showDialog(message, buttons, opt) {
	if (!buttons) {
		buttons = [
			['OK', null]
		];
	}

	var setting = {
		verticalButtons: false, //按钮是否平行垂直排列
		shade: true, //是否显示遮罩背景
		disableScroll: false //是否停用页面滚动
	};

	if (opt) {
		$.extend(setting, opt);
	}

	var colw = setting.verticalButtons ? 12 : 12 / buttons.length;
	var colHtml = '';

	for (var i=0,btn; btn=buttons[i]; i++) {
		colHtml += '<div class="col-xs-' + colw + '"><button type="button">' + btn[0] + '</button></div>';
	}

	var autoId = window._CONFIRM_AUTO_ID || 0;
	var id = "confirmDialog" + id;
	++autoId;
	window._CONFIRM_AUTO_ID = autoId;

	var html = '<div class="confirm" id="' + id +  '">\
		<div class="confirm-content">' + message + '</div>\
	<div class="confirm-foot">\
		<div class="row buttons">' + colHtml + '</div>\
	</div>\
	</div>';
	
	if (setting.shade) {
		html += '<div class="confirm-shade" id="' + id + 'shade"></div>';
	}

	var body = $("body");
	body.append(html);

	if (setting.disableScroll) {
		body.css("overflow", "hidden");
	}

	var outer = $("#" + id);
	var w = outer.outerWidth();
	var h = outer.outerHeight();

	outer.css({
		"left": "50%",
		"margin-left": -(w/2)+"px",
		"top": ((document.documentElement.clientHeight-h)/2)+"px"
	});

	var instance = {
		close: function() {
			outer.remove();

			if (setting.disableScroll) {
				body.css("overflow", "");
			}

			if (setting.shade) {
				$("#"+id+"shade").remove();
			}
		}
	};

	outer.find(".buttons button").each(function(i) {
		var btn = buttons[i];
		$(this).click(function() {
			if (btn[1]) {
				btn[1].call(instance);
			} else {
				instance.close();
			}
		});
	});
}