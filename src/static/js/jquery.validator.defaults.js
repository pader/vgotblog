$.validator.setDefaults({
    errorElement: "span",
    errorClass: "validate-error",
    errorPlacement: function (error, element) {
        var container = element.next();

        if (container.is(".validate-container")) {
            container.find(".validate-error").replaceWith(error);
        } else {
            var outer = $('<div class="validate-container"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></div>');
            element.after(outer);
            outer.append(error);
        }

        element.closest(".form-group").addClass("has-warning");
    },
    success: function(label) {
        label.closest(".form-group").removeClass("has-warning");
        label.closest(".validate-container").remove();
    }
});