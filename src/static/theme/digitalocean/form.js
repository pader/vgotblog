$.validator.setDefaults({
	errorElement: "span",
	errorClass: "validate-error",
	errorPlacement: function (error, element) {
		element.parent().addClass("has-error");
		element.tooltip('destroy').tooltip({
			animation: false,
			title: error.text(),
			trigger: 'manual',
		}).tooltip('show');
	},
	success: function(error, element) {
		var container = $(element).closest('.has-error');
		if (container.length > 0) {
			container.removeClass("has-error");
			$(element).tooltip('destroy');
		}
	}
});

