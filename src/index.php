<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2017/4/23
 * Time: 01:42
 */
use vgot\Boot;

ini_set('display_errors', 'On');
ini_set('error_reporting', E_ALL);

define('BASE_PATH', __DIR__); //constant only for app
define('RES_PATH', BASE_PATH.'/res');
define('VIEW_PATH', BASE_PATH.'/app/views');

require BASE_PATH.'/../../vgot_framework/src/framework/Boot.php';

//if (is_file(BASE_PATH.'/vendor/autoload.php')) {
//	require BASE_PATH.'/vendor/autoload.php';
//}

Boot::addNamespaces([
	'app' => BASE_PATH.'/app',
	'plugin' => RES_PATH.'/plugin'
]);

Boot::systemConfig([
	'controller_namespace' => '\app\controllers',
	'config_path' => BASE_PATH.'/app/config',
	'views_path' => VIEW_PATH,
	'common_config_path' => RES_PATH,
	'common_views_path' => BASE_PATH.'/../../vgot_framework/src/views'
]);

/**
 * 应用框架环境配置
 *
 * 应用程序可根据此配置读取不同的配置等
 *
 * development 本地开发环境
 * testing 测试平台
 * production 线上正式环境（生产环境）
 */
define('ENVIRONMENT', 'development');

switch (ENVIRONMENT) {
	case 'production':
		error_reporting(0);
		break;

	case 'testing':
	case 'development':
		error_reporting(E_ALL);
		break;
}

Boot::run();

