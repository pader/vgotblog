<?php
/**
 * Created by PhpStorm.
 * User: pader
 * Date: 2018/5/6
 * Time: 14:27
 */

namespace {

	/**
	 * @return \vgot\Core\IdeApplication
	 */
	function getApp() {}

}

namespace vgot\Core {

	/**
	 * Application
	 *
	 * @property \app\libs\PasswordHash $passwordHash
	 * @property \app\libs\Hook $hook
	 */
	class IdeApplication extends \vgot\Core\Application {}

	class Base extends IdeApplication {}

	/**
	 * Class Controller
	 *
	 * @method render($name, $vars=null, $return=false)
	 */
	class Controller extends Base {}

}
